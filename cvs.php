<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php include_once 'menu.php'; ?>
		<div class="row" id="content">
			<div class="col-md-8">
				<div id="tituloPrincipalFundo" style="margin:0 0 20px 0px;">
					<span>CANDIDATOS</span>
				</div>
				<?php
				$query = "SELECT * FROM cvs cv 
				INNER JOIN (SELECT email, cpf as cpfCnpj, cidade, estado, id_usuario as idUsuario, cargo_interesse1 as cargoInteresse, imagem as imagemUsuario, nome FROM usuarios) u ON cv.id_usuario = u.idUsuario
				WHERE cv.ativo = 1
				ORDER BY u.nome ASC LIMIT 10";
				$query = mysql_query($query);
				$numRows = mysql_num_rows($query);
				if ($numRows > 0) {
					while ($result = mysql_fetch_array($query)) {
						$id_curriculum = $result['id_curriculum'];
						$nome = $result['nome'];
						$email = $result['email'];
						$cargoInteresse = $result['cargoInteresse'];
						$imagemUsuario = $result['imagemUsuario'];
						$escolaridade = $result['escolaridade'];
						if($escolaridade == 'doutoradoCompleto' ) $escolaridade = 'Doutorado Completo';
						if($escolaridade == 'doutoradoIncompleto' ) $escolaridade = 'Doutorado Incompleto';
						if($escolaridade == 'mestradoCompleto' ) $escolaridade = 'Mestrado Completo';
						if($escolaridade == 'mestradoIncompleto' ) $escolaridade = 'Mestrado Incompleto';
						if($escolaridade == 'posGraduacaoCompleta' ) $escolaridade = 'Pós-graduação Completa';
						if($escolaridade == 'posGraduacaoIncompleta' ) $escolaridade = 'Pós-graduação Incompleta';
						if($escolaridade == 'superiorCompleto' ) $escolaridade = 'Superior Completo';
						if($escolaridade == 'superiorIncompleto' ) $escolaridade = 'Superior Incompleto';
						if($escolaridade == 'ensinoMedioCompleto' ) $escolaridade = 'Ensino Médio Completo';
						if($escolaridade == 'ensinoMedioIncompleto' ) $escolaridade = 'Ensino Médio Incompleto';	
						if($escolaridade == 'ensinoFundamentalCompleto' ) $escolaridade = 'Ensino Fundamental Completo';
						if($escolaridade == 'ensinoFundamentalIncompleto' ) $escolaridade = 'Ensino Fundamental Incompleto';
						if($escolaridade == 'naoAlfabetizado' ) $escolaridade = 'Não Alfabetizado';
						$cidade = $result['cidade'];
						$estado = $result['estado'];
						$objetivo = $result['objetivo'];
						echo
						'
						<div class="col-md-4" id="candidato">
						<div class="col-md-9" id="infoCandidato">
						<div class="col-md-12 semPadding">
						<div class="col-md-12 semPadding imagemUsuario" style="background:url(admin/'.$imagemUsuario.') 50% 50% no-repeat;">
						</div>
						</div>
						<span><b>Nome: </b>'.$nome.'</span>
						<span><font id="spanEmprego">Cargo de Interesse: </font>'.$cargoInteresse.'</span>
						<span><font id="spanEmprego">Escolaridade: </font>'.$escolaridade.'</span>
						<span><font id="spanEmprego">Cidade: </font>'.$cidade.' - '.$estado.'</span>
						<br/>
						<a href="verCv.php?idCv='.$id_curriculum.'" class="btn btn-primary">Ver Curriculum</a>
						</div>
						</div>
						';						
					}
				}
				?>
			</div>	
			<?php include_once 'buscaRapida.php'; ?>
		</div>
		<?php include_once 'footer.php'; ?>
	</div>
</body>
</html>