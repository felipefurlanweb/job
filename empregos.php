<?php 

	include_once 'head.php';
	include_once 'menu.php';
	$candidato = 0;
	$empresa = 0;
	if (isset($_POST['btnPesquisaRapidaCandidato'])) {

		$columns = array();
		if($_POST['emprego'] != "") {
		    $columns[] = "v.nome = '".$_POST['emprego']."'";
		}
		if($_POST['estado'] != "") {
		    $columns[] = "v.estado = '".$_POST['estado']."'";
		}
		if($_POST['cidade'] != "") {
		    $columns[] = "v.cidade = '".$_POST['cidade']."'";
		}
		if($_POST['valorMinimo'] != "") {
		    $columns[] = "v.salario >= '".$_POST['valorMinimo']."'";
		}
		if($_POST['valorMaximo'] != "") {
		    $columns[] = "v.salario <= '".$_POST['valorMaximo']."'";
		}
		$query = 'SELECT * FROM vagas v LEFT JOIN (SELECT nome_fantasia as nomeEmpresa, id as idEmpresa, ativo, imagem FROM empresas ) em ON v.idEmpresa = em.idEmpresa';
		if( count( $columns ) > 0 ) {
		    $query .= sprintf( ' WHERE %s', implode( ' AND ', $columns ) );
		}
		$query = $query." ORDER BY v.nome ASC";
		$mensagemErro = '<div class="alert alert-danger">Nenhuma vaga encontrada.</div>'; 
		$candidato = 1;
	}elseif (isset($_POST['btnPesquisaRapidaEmpresa'])) {
		$columns = array();
		if($_POST['estado'] != "") {
		    $columns[] = "u.estado = '".$_POST['estado']."'";
		}
		if($_POST['cidade'] != "") {
		    $columns[] = "u.cidade = '".$_POST['cidade']."'";
		}
		if($_POST['valorMinimo'] != "") {
		    $columns[] = "e.salario >= '".$_POST['valorMinimo']."'";
		}
		if($_POST['valorMaximo'] != "") {
		    $columns[] = "e.salario <= '".$_POST['valorMaximo']."'";
		}
		$query = "SELECT * FROM cvs cv INNER JOIN (SELECT nome, email, imagem, cargo_interesse1, cargo_interesse2, cargo_interesse3, estado, cidade, id_usuario as idUsuario FROM usuarios) u ON cv.id_usuario = u.idUsuario ";
		if( count( $columns ) > 0 ) {
		    $query .= sprintf( ' WHERE %s', implode( ' AND ', $columns ) );
		}
		$query = $query.@$opcaoDesejado;
		$query = $query." ORDER BY u.nome ASC";
		$mensagemErro = '<div class="alert alert-danger">Nenhum candidato encontrado.</div>'; 
		$empresa = 1;
	}else{
		$query = "SELECT * FROM vagas v LEFT JOIN 
		(SELECT nome_fantasia as nomeEmpresa, id as idEmpresa, ativo, imagem FROM empresas )
		 em ON v.idEmpresa = em.idEmpresa ORDER BY v.nome ASC";	
		$mensagemErro = '<div class="alert alert-danger">Nenhuma vaga encontrada.</div>'; 
	}
?>

		<div class="row">
			<div class="container">
				<?php 
					include_once 'buscaRapida.php'; 
				?>
				<div class="col-xs-12 col-md-8">
					<?php 
						//echo $query;
						$query = mysql_query($query);
						$numRows = mysql_num_rows($query);
						if ($numRows == 0) {
							echo $mensagemErro;
						}else{
							if ($candidato) {
								echo
								'
									<div class="panel panel-primary">
										<div class="panel panel-heading">
											<span>VAGAS</span>
										</div>
										<div class="panel panel-body">
								';
														while ($result = mysql_fetch_array($query)) {

															$id = $result["id"];
															$nome = $result["nome"];
															$nomeEmpresa = $result["nomeEmpresa"];
															$tipo_vaga = $result["tipo_vaga"];
															$horario = $result["horario"];
															$data = $result["data"];

															if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
															if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
															if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
															if($horario == "comercial") { $horarioString = "Comercial"; } 
															if($horario == "2turno") { $horarioString = "2ª turno"; } 
															$data = FormataData_Brasil($data,"tsp");
															echo
															'
																<div class="col-xs-12 col-md-3 vaga">
																	<div>
																		<span>
																			<b>'.$nome.'</b>
																		</span>
																		<span>
																			<small>
																				'.$data.'
																			</small>
																		</span>
																	</div>
																	<div class="form-group">
																		<span>'.$nomeEmpresa.'</span>
																	</div>
																	<div>
																		<a class="btn btn-primary" href="vaga.php?id='.$id.'">
																			Mais informações
																		</a>
																	</div>



																</div>
															';							

														}
									echo '</div>';
								echo '</div>';
							}

							if($empresa){

								echo

								'

									<div class="panel panel-primary">
										<div class="panel panel-heading">
												<span>CANDIDATOS EM DESTAQUE</span>
										</div>
										<div class="panel panel-body">
								';

											while ($result = mysql_fetch_array($query)) {

												$id_curriculum = $result['id_curriculum'];

												$nome = $result['nome'];
												$email = $result['email'];
												$cargoInteresse = $result['cargo_interesse1'];
												$imagemUsuario = $result['imagem'];
												$escolaridade = $result['escolaridade'];
												if($escolaridade == 'doutoradoCompleto' ) $escolaridade = 'Doutorado Completo';
												if($escolaridade == 'doutoradoIncompleto' ) $escolaridade = 'Doutorado Incompleto';
												if($escolaridade == 'mestradoCompleto' ) $escolaridade = 'Mestrado Completo';
												if($escolaridade == 'mestradoIncompleto' ) $escolaridade = 'Mestrado Incompleto';
												if($escolaridade == 'posGraduacaoCompleta' ) $escolaridade = 'Pós-graduação Completa';
												if($escolaridade == 'posGraduacaoIncompleta' ) $escolaridade = 'Pós-graduação Incompleta';
												if($escolaridade == 'superiorCompleto' ) $escolaridade = 'Superior Completo';
												if($escolaridade == 'superiorIncompleto' ) $escolaridade = 'Superior Incompleto';
												if($escolaridade == 'ensinoMedioCompleto' ) $escolaridade = 'Ensino Médio Completo';
												if($escolaridade == 'ensinoMedioIncompleto' ) $escolaridade = 'Ensino Médio Incompleto';	
												if($escolaridade == 'ensinoFundamentalCompleto' ) $escolaridade = 'Ensino Fundamental Completo';
												if($escolaridade == 'ensinoFundamentalIncompleto' ) $escolaridade = 'Ensino Fundamental Incompleto';
												if($escolaridade == 'naoAlfabetizado' ) $escolaridade = 'Não Alfabetizado';
												$cidade = $result['cidade'];
												$estado = $result['estado'];
												$objetivo = $result['objetivo'];
												echo
												'
													<div class="col-xs-12 col-md-2">
														<div>
															<p><b>'.$nome.'</b></p>
															<hr>
															<p><b>Cargo de Interesse</b></p>
															<p>'.$cargoInteresse.'</p>
															<p><b>Escolaridade</b></p>
															<p>'.$escolaridade.'</p>
															<p><b>Cidade</b></p>
															<p>'.$cidade.' - '.$estado.'</p>
															<a href="verCv.php?idCv='.$id_curriculum.'" class="btn btn-primary">Ver Curriculum</a>
														</div>
													</div>
												';		
											}
									echo '</div>';
								echo '</div>';

							}			

							if(!$candidato && !$empresa){

								?>

										
											<div class="panel panel-primary">
												<div class="panel panel-heading">
													<span>VAGAS</span>
												</div>
												<div class="panel panel-body">
												<?php

													$query = "SELECT * FROM vagas v LEFT JOIN (SELECT nome_fantasia as nomeEmpresa, id as idEmpresa, ativo, imagem FROM empresas ) em ON v.idEmpresa = em.idEmpresa ORDER BY v.nome ASC";
													$query = mysql_query($query);
													$numRows = "";
													$numRows = mysql_num_rows($query);
													if ($numRows > 0) {
														while ($result = mysql_fetch_array($query)) {

															$id = $result["id"];
															$nome = $result["nome"];
															$nomeEmpresa = $result["nomeEmpresa"];
															$tipo_vaga = $result["tipo_vaga"];
															$horario = $result["horario"];
															$data = $result["data"];

															if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
															if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
															if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
															if($horario == "comercial") { $horarioString = "Comercial"; } 
															if($horario == "2turno") { $horarioString = "2ª turno"; } 
															$data = FormataData_Brasil($data,"tsp");
															echo
															'
																<div class="col-xs-12 col-md-3 vaga">
																	<div>
																		<span>
																			<b>'.$nome.'</b>
																		</span>
																		<span>
																			<small>
																				'.$data.'
																			</small>
																		</span>
																	</div>
																	<div class="form-group">
																		<span>'.$nomeEmpresa.'</span>
																	</div>
																	<div>
																		<a class="btn btn-primary" href="vaga.php?id='.$id.'">
																			Mais informações
																		</a>
																	</div>



																</div>
															';							

														}
													}
												?>
												</div>
											</div>
								<?php
							}

						}

					?>

				</div>



			</div>
		</div>

		<?php 
			include_once 'footer.php'; 
		?>

	</div>

</body>

</html>