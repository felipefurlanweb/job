<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php 
            include_once 'menu.php'; 
            echo mostraMensagem();
        ?>
        <div class="row">
                <div class="row" id="boxCadastrar">
                    <form action="admin/engine/formularios.php" method="post">
                      <input type="hidden" name="id" value="esqueceuSenha">
                      <table>
                        <tr>
                          <td id="right-form" ><label>E-mail cadastrado:</label></td>
                          <td id="left-form" ><input class="form-control" type="text" name='email' required></td>
                        </tr>
                        <tr>
                          <td id="right-form" ></td>
                          <td id="left-form" >
                            <button class="btn btn-success" type="submit">Enviar</button></td>
                        </tr>
                      </table>
                    </form>              
                </div>
        </div>
		<?php include_once 'footer.php'; ?>
	</div>
</body>
</html>