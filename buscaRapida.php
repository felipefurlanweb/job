			<div class="col-xs-12 col-md-4" id="buscaRapida">
				<div class="panel panel-primary">
					<div class="panel panel-heading">
						<span>Busca Rápida</span>
					</div>
					<div class="panel panel-body">
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#tabCandidato" aria-controls="home" role="tab" data-toggle="tab">Vagas</a></li>
					    <li role="presentation"><a href="#tabEmpresa" aria-controls="profile" role="tab" data-toggle="tab">Candidatos</a></li>
					  </ul>
					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="tabCandidato">
							<form action="empregos.php" method="post">
								<div class="form-group marginTop">
									<select name="emprego" class="form-control">
										<option value="">Selecione uma vaga ...</option>
										<?php 
											$queryBuscaRapida = "SELECT * FROM nome_empregos ORDER BY emprego ASC";
											$queryBuscaRapida = mysql_query($queryBuscaRapida);
											while ($result = mysql_fetch_array($queryBuscaRapida)) {
												$emprego = $result['emprego'];
												$emprego = utf8_encode($emprego);
												echo '<option value="'.$emprego.'">'.$emprego.'</option>';
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<select name="estado" class="form-control" id="estado1"></select>
								</div>
								<div class="form-group">
									<select name="cidade" class="form-control" id="cidade1">
										<option value="">Selecione uma cidade ...</option>
									</select>
								</div>
								<script language="JavaScript" type="text/javascript" charset="utf-8">
								  new dgCidadesEstados({
								    estado: document.getElementById('estado1'),
								    cidade: document.getElementById('cidade1')
								  })
								</script>
								<div class="form-group">
									<select name="valorMinimo" class="form-control">
										<option value="">Valor Mínimo ...</option>
										<option value="200,00">R$ 200,00</option>
										<option value="400,00">R$ 400,00</option>
										<option value="600,00">R$ 600,00</option>
										<option value="800,00">R$ 800,00</option>
										<option value="1.000,00">R$ 1.000,00</option>
										<option value="1.500,00">R$ 1.500,00</option>
										<option value="2.000,00">R$ 2.000,00</option>
										<option value="2.500,00">R$ 2.500,00</option>
										<option value="3.000,00">R$ 3.000,00</option>
										<option value="3.500,00">R$ 3.500,00</option>
										<option value="4.000,00">R$ 4.000,00</option>
										<option value="4.500,00">R$ 4.500,00</option>
										<option value="5.000,00">R$ 5.000,00</option>
										<option value="maisMinimo">Acima de R$ 5.000,00</option>
									</select>
								</div>
								<div class="form-group">
									<select name="valorMaximo" class="form-control">
										<option value="">Valor Maximo ...</option>
										<option value="200,00">R$ 200,00</option>
										<option value="400,00">R$ 400,00</option>
										<option value="600,00">R$ 600,00</option>
										<option value="800,00">R$ 800,00</option>
										<option value="1.000,00">R$ 1.000,00</option>
										<option value="1.500,00">R$ 1.500,00</option>
										<option value="2.000,00">R$ 2.000,00</option>
										<option value="2.500,00">R$ 2.500,00</option>
										<option value="3.000,00">R$ 3.000,00</option>
										<option value="3.500,00">R$ 3.500,00</option>
										<option value="4.000,00">R$ 4.000,00</option>
										<option value="4.500,00">R$ 4.500,00</option>
										<option value="5.000,00">R$ 5.000,00</option>
										<option value="maisMaximo">Acima de R$ 5.000,00</option>
									</select>
								</div>
								<input class="btn btn-block btn-primary" type="submit" name="btnPesquisaRapidaCandidato" value="PROCURAR">
							</form>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="tabEmpresa">
							<form action="empregos.php" method="post">
								<div class="form-group marginTop">
									<select name="emprego" class="form-control">
										<option value="">Selecione uma vaga ...</option>
										<?php 
											$queryBuscaRapida = "SELECT * FROM nome_empregos ORDER BY emprego ASC";
											$queryBuscaRapida = mysql_query($queryBuscaRapida);
											while ($result = mysql_fetch_array($queryBuscaRapida)) {
												$emprego = $result['emprego'];
												$emprego = utf8_encode($emprego);
												echo '<option value="'.$emprego.'">'.$emprego.'</option>';
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<select name="estado" class="form-control" id="estado2"></select>
								</div>
								<div class="form-group">
									<select name="cidade" class="form-control" id="cidade2">
										<option value="">Selecione uma cidade ...</option>
									</select>
								</div>
								<script language="JavaScript" type="text/javascript" charset="utf-8">
								  new dgCidadesEstados({
								    estado: document.getElementById('estado2'),
								    cidade: document.getElementById('cidade2')
								  })
								</script>
								<div class="form-group">
									<select name="valorMinimo" class="form-control">
										<option value="">Salário - Valor Mínimo ...</option>
										<option value="200.00">R$ 200,00</option>
										<option value="400.00">R$ 400,00</option>
										<option value="600.00">R$ 600,00</option>
										<option value="800.00">R$ 800,00</option>
										<option value="1000.00">R$ 1.000,00</option>
										<option value="1500.00">R$ 1.500,00</option>
										<option value="2000.00">R$ 2.000,00</option>
										<option value="2500.00">R$ 2.500,00</option>
										<option value="3000.00">R$ 3.000,00</option>
										<option value="3500.00">R$ 3.500,00</option>
										<option value="4000.00">R$ 4.000,00</option>
										<option value="4500.00">R$ 4.500,00</option>
										<option value="5000.00">R$ 5.000,00</option>
										<option value="maisMinimo">Acima de R$ 5.000,00</option>
									</select>
								</div>
								<div class="form-group">
									<select name="valorMaximo" class="form-control">
										<option value="">Salário - Valor Maximo ...</option>
										<option value="400.00">R$ 400,00</option>
										<option value="600.00">R$ 600,00</option>
										<option value="800.00">R$ 800,00</option>
										<option value="1000.00">R$ 1.000,00</option>
										<option value="1500.00">R$ 1.500,00</option>
										<option value="2000.00">R$ 2.000,00</option>
										<option value="2500.00">R$ 2.500,00</option>
										<option value="3000.00">R$ 3.000,00</option>
										<option value="3500.00">R$ 3.500,00</option>
										<option value="4000.00">R$ 4.000,00</option>
										<option value="4500.00">R$ 4.500,00</option>
										<option value="5000.00">R$ 5.000,00</option>
										<option value="maisMaximo">Acima de R$ 5.000,00</option>
									</select>
								</div>
								<input class="btn btn-block btn-primary" type="submit" name="btnPesquisaRapidaEmpresa" value="PROCURAR">
							</form>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#myTabs a').click(function (e) {
					  e.preventDefault()
					  $(this).tab('show')
					});
				});	
			</script>