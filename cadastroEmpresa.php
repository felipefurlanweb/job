<html>
<head>
	<?php include_once 'head.php'; ?>
    <script src="admin/js/jasny.js"></script>
    <link rel="stylesheet" type="text/css" href="admin/css/jasny.css">
</head>
<body>
	<div class="container-fluid">
		<?php include_once 'menu.php'; ?>
		<div class="row">
				<div class="row" id="boxCadastrar">
                      <div class="tipoLogin">
                        <img src="img/profilePicture.jpg">
                        <label>SOU CANDIDATO</label>
                        <input type="radio" name="tipoCadastro" value="candidato">
                        <img src="img/empresaIcon.png">
                        <label>SOU EMPRESA</label>
                        <input type="radio" name="tipoCadastro" value="empresa">
                      </div>
                    <div class="formEmpresa">
                      <form action="admin/engine/formularios.php" method="post" id="formCadastroEmpresa" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="empresaAdd">
                        <table>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                <div>
                                  <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Selecione uma imagem</span>
                                    <span class="fileinput-exists">Alterar</span>
                                    <input type="file" name="imagem">
                                  </span>
                                  <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                </div>
                              </div>
                          <tr>
                            <td id="right-form" ><label>Razão Social: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="razaosoc" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Nome Fantasia: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="nomefantasia" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>CPF/CNPJ: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" maxlength="18" name='cpfcnpj' onkeypress='mascaraMutuario(this,cpfCnpj)' onblur='clearTimeout()' required></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>E-mail: </label></td>
                            <td id="left-form" ><input class="form-control" type="email" name="email" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Senha: </label></td>
                            <td id="left-form" ><input class="form-control" type="password" name="senha" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Telefone: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="telefone" placeholder="(xx) xxxx-xxxx" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Endereço: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="endereco"/></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Estado: </label></td>
                            <td id="left-form" ><select class="form-control" id="estado" name="estado"></select></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Cidade: </label></td>
                            <td id="left-form" ><select class="form-control" id="cidade" name="cidade">
                              <option>Selecione um estado ...</option>
                            </select></td>
                          </tr>
                          <table>
                          <tr>
                            <td>
                              <div class="boxPlano">
                                <div class="nomePlano">
                                  <span>PLANO 1</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 1 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>10</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoEmpresa" value="1">
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="boxPlano boxPlanoBorda">
                                <div class="nomePlano">
                                  <span>PLANO 2</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 2 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>20</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoEmpresa" value="2">
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="boxPlano boxPlanoBorda">
                                <div class="nomePlano">
                                  <span>PLANO 3</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 3 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>30</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoEmpresa" value="3">
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Conte nos um pouco sobre sua empresa: </label></td>
                            <td id="left-form" ><textarea class="form-control" name="sobre"></textarea></td>
                          </tr>
                          </table>
                          <table>
                          <tr>
                            <td id="right-form" ></td>
                            <td id="left-form" >
                              <button class="btn btn-success" type="submit">Continuar</button></td>
                          </tr>
                        </table>
                      </form> 
                    </div>  
                    <div class="formCandidato">
                      <form action="admin/engine/formularios.php" method="post">
                        <input type="hidden" name="id" value="cadastrarUser">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                <div>
                                  <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Selecione uma imagem</span>
                                    <span class="fileinput-exists">Alterar</span>
                                    <input type="file" name="imagem">
                                  </span>
                                  <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                </div>
                              </div>
                        <table>
                          <tr>
                            <td id="right-form" ><label>Nome: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="nome" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>CPF/CNPJ: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" maxlength="18" name='cpfcnpj' onkeypress='mascaraMutuario(this,cpfCnpj)' onblur='clearTimeout()' required></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>E-mail: </label></td>
                            <td id="left-form" ><input class="form-control" type="email" name="email" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Senha: </label></td>
                            <td id="left-form" ><input class="form-control" type="password" name="senha" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Telefone: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="telefone" placeholder="(xx) xxxx-xxxx" /></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Endereço: </label></td>
                            <td id="left-form" ><input class="form-control" type="text" name="endereco"/></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Estado: </label></td>
                            <td id="left-form" ><select class="form-control" id="estado" name="estado"></select></td>
                          </tr>
                          <tr>
                            <td id="right-form" ><label>Cidade: </label></td>
                            <td id="left-form" ><select class="form-control" id="cidade" name="cidade">
                              <option>Selecione um estado ...</option>
                            </select></td>
                          </tr>
                          <table>
                          <tr>
                            <td>
                              <div class="boxPlano">
                                <div class="nomePlano">
                                  <span>PLANO 1</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 1 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>10</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoCandidato" value="1">
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="boxPlano boxPlanoBorda">
                                <div class="nomePlano">
                                  <span>PLANO 2</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 2 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>20</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoCandidato" value="2">
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="boxPlano boxPlanoBorda">
                                <div class="nomePlano">
                                  <span>PLANO 3</span>
                                </div>
                                <div class="infoPlano">
                                  <span>Seu curriculum publicado durante 3 mês</span>
                                  <div class="precoPlano">
                                    <h2>R$<h1>30</h1>,00 p/ Mês</h2>
                                  </div>
                                  <input type="radio" name="planoCandidato" value="3">
                                </div>
                              </div>
                            </td>
                          </tr>
                          </table>
                          <table>
                          <tr>
                            <td id="right-form" ></td>
                            <td id="left-form" >
                              <button class="btn btn-success" type="submit">Continuar</button></td>
                          </tr>
                        </table>
                      </form>  
                    </div>           
                </div>
		</div>
		<?php include_once 'footer.php'; ?>
	</div>
  <script type="text/javascript">
    $(document).ready(function() {
      $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
      $(".formEmpresa").hide();
      $(".formCandidato").hide();
      $('input[type=radio][name=tipoCadastro]').change(function() {
          if (this.value == 'candidato') {
              $(".formEmpresa").hide('slow/400/fast');
              $(".formCandidato").show('slow/400/fast');
          }
          else if (this.value == 'empresa') {
              $(".formCandidato").hide('slow/400/fast');
              $(".formEmpresa").show('slow/400/fast');
          }
      });
    }); 
  </script>
</body>
</html>