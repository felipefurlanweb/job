<?php include_once 'head.php'; ?>

  <script type="text/javascript" src="js/jquery.maskedinput.js"></script>

  <script type="text/javascript">

  $(document).ready(function() {

    $("#datanasc").mask("99/99/9999");

  });

  </script>

  <script type="text/javascript">

  /* MÃ¡scaras ER */

  function mascaraTelefone(o,f){

    v_obj=o

    v_fun=f

    setTimeout("execmascaraTelefone()",1)

  }

  function execmascaraTelefone(){

    v_obj.value=v_fun(v_obj.value)

  }

  function mtel(v){

    v=v.replace(/\D/g,"");             //Remove tudo o que nÃ£o Ã© dÃ­gito

    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parÃªnteses em volta dos dois primeiros dÃ­gitos

    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hÃ­fen entre o quarto e o quinto dÃ­gitos

    return v;

  }

  function id( el ){

    return document.getElementById( el );

  }

  window.onload = function(){

    id('telefone').onkeypress = function(){

      mascaraTelefone( this, mtel );

    };

    new dgCidadesEstados({

      estado: document.getElementById('estado'),

      cidade: document.getElementById('cidade')

    });

  }

  </script>

    <section>
    <div class="container">

      <div class="row">

        <form action="admin/engine/formularios.php" method="post" enctype="multipart/form-data">

          <input type="hidden" name="id" value="cadastrarCv">

            <div class="col-xs-12 col-md-12 marginTop">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  Informe seus dados principais nos campos abaixo.
                </div>
                <div class="panel-body">

                  <div class="row">

                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <label>Envie seu currículo como arquivo ou preencha nosso formulario</label>
                        <input type="file" name="cvAnexo" style="display: initial;">
                      </div>
                      <div class="col-xs-12 col-md-3 ">
                        <label>Está empregado atualmente?</label>
                        <select class="form-control" name="empregado" required>
                          <option value="">Selecione uma opção ...</option>
                          <option value="1">Sim</option>
                          <option value="0">Não</option>
                        </select>
                      </div>
                      <div class="col-xs-12 col-md-3">
                        <label>Escolaridade</label>
                        <select class="form-control" name="escolaridade">
                          <option value="">Selecione uma opção ...</option>
                          <option value="doutoradoCompleto">Doutorado Completo</option>
                          <option value="doutoradoIncompleto">Doutorado Incompleto</option>
                          <option value="mestradoCompleto">Mestrado Completo</option>
                          <option value="mestradoIncompleto">Mestrado Incompleto</option>
                          <option value="posGraduacaoCompleta">Pós-graduação Completa</option>
                          <option value="posGraduacaoIncompleta">Pós-graduação Incompleta</option>
                          <option value="superiorCompleto">Superior Completo</option>
                          <option value="superiorIncompleto">Superior Incompleto</option>
                          <option value="ensinoMedioCompleto">Ensino Médio Completo</option>
                          <option value="ensinoMedioIncompleto">Ensino Médio Incompleto</option>
                          <option value="ensinoFundamentalCompleto">Ensino Fundamental Completo</option>
                          <option value="ensinoFundamentalIncompleto">Ensino Fundamental Incompleto</option>
                          <option value="naoAlfabetizado">Não Alfabetizado</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 marginTop">
                      <label>Objetivo Profissional</label>
                      <textarea class="form-control" name="objetivoProfissional" required></textarea>
                    </div>
                    <div class="col-xs-12 col-md-6 marginTop">
                      <label>Experiência Profissional</label>
                      <textarea class="form-control" name="experienciaProfissional" required></textarea>
                    </div>
                    <div class="col-xs-12 col-md-4 marginTop">
                      <label>Último emprego</label>
                      <input type="text" class="form-control" name="ultimoEmprego" required>
                    </div>
                    <div class="col-xs-12 col-md-4 marginTop">
                      <label>Pretensão Salárial</label>
                      <input type="text" class="form-control valorReal" name="salario">
                    </div>
                    <div class="col-xs-12 col-md-12 marginTop">
                      <button class="btn btn-success" type="submit">Cadastrar</button></td>
                    </div>

                  </div>

                </div>
              </div>

            </div>
      </div>

    </section>


      <script type="text/javascript">

      $(document).ready(function() {

        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

      }); 

      </script>

      <?php include_once 'footer.php'; ?>

    </div>

  </body>

  </html>