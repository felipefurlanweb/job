<?php include_once 'head.php';

	$idEmprego = $_GET['id'];


	$query = "SELECT * FROM vagas v
	INNER JOIN empresas e ON v.idEmpresa = e.id
	WHERE v.id = $idEmprego";

	$query = mysql_query($query);

	while ($result = mysql_fetch_array($query)) {

		$nomeEmpresa = $result['nome_fantasia'];
		$idEmpresa = $result['idEmpresa'];

	}

	$query = "SELECT * FROM vagas WHERE id = '$idEmprego'";

	$query = mysql_query($query);

	while ($result = mysql_fetch_array($query)) {

		$nome = $result['nome'];

		$empresa = $result['idEmpresa'];

		$principaisAtividades = $result['principais_atividades'];

		$estado = $result['estado'];

		$cidade = $result['cidade'];

		$requisitos = $result['requisitos'];

		$salario = $result['salario'];


		$tipo_vaga = $result['tipo_vaga'];
		$horario = $result['horario'];
		if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
		if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
		if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
		if($horario == "comercial") { $horarioString = "Comercial"; } 
		if($horario == "2turno") { $horarioString = "2ª turno"; } 

	}

?>

<div class="container">
	<div class="row">


		<div class="col-xs-12 col-md-12">


			<?php

			if (@$_SESSION['job']['usuario']['id'] != "") {

				$idUsuario = $_SESSION['job']['usuario']['id'];

				$query = "SELECT * FROM candidatos WHERE id_vaga = '$idEmprego' AND id_usuario = '$idUsuario'";

				$query = mysql_query($query);

				$numRows = mysql_num_rows($query);

				if ($numRows > 0) {

					echo

					'

					<div>
						<div class="alert alert-info">VOCÊ JÁ SE CANDIDATOU PARA ESTE EMPREGO</div>
					</div>
					';

				}else{
					echo

					'

					<div>

						<form action="admin/engine/formularios.php" method="post">

							<input type="hidden" name="id" value="candidatarEmprego">

							<input type="hidden" name="idEmprego" value="'.$idEmprego.'">

							<input type="hidden" name="idUsuario" value="'.$idUsuario.'">

							<input type="submit" class="form-control btn btn-success" value="CANDIDATAR-SE">

						</form>

					</div>

					';
				}
				$sql = "SELECT * FROM usuarios_planos WHERE idUsuario = $idUsuario AND status = 'pago'";
				$sql = mysql_query($sql);
				$numRows = mysql_num_rows($sql);
				if ($numRows > 0) {

				}

			}


			?>

		</div>
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-primary">

				<div class="panel-heading">

					<span>INFORMAÇÕES</span>

				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Vaga</label>
						<input class="form-control" readonly type="text" value="<?php echo $nome; ?>">
					</div>
					<!-- verificar se empresa é anonima, se for esconder dados -->
					<div class="form-group">
						<label>Empresa</label>
						<div class="marginBottom">
							<input class="form-control" readonly type="text" value="<?php echo $nomeEmpresa; ?>">
						</div>
						<a href="empresa.php?id=<?php echo $idEmpresa; ?>" class="btn btn-primary">Conheça a Empresa</a>
					</div>
					<div class="form-group">
						<label>Requisitos</label>
						<textarea class="form-control" readonly><?php echo $requisitos; ?></textarea>
					</div>
					<div class="form-group">
						<label>Principais Atividades</label>
						<textarea class="form-control" readonly><?php echo $principaisAtividades; ?></textarea>
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Cidade</label>
						<input class="form-control" readonly type="text" value="<?php echo $cidade; ?> - <?php echo $estado ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Tipo de vaga</label>
						<input class="form-control" readonly type="text" value="<?php echo $tipo_vagaString; ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Horário</label>
						<input class="form-control" readonly type="text" value="<?php echo $horarioString; ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Salário</label>
						<input class="form-control" readonly type="text" value="R$ <?php echo $salario; ?>">
					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<?php include_once 'footer.php'; ?>



</body>

</html>