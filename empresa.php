<?php include_once 'head.php';
	
		$idEmpresa = $_GET['id'];
		$query = "SELECT * FROM empresas WHERE id = '$idEmpresa'";
		$query = mysql_query($query);
		while ($result = mysql_fetch_array($query)) {
		    $imagem = $result['imagem'];
		    $razaosoc = $result['razaosoc'];
		    $nome_fantasia = $result['nome_fantasia'];
		    $cpf_cnpj = $result['cnpj'];
		    $email = $result['email'];
		    $telefone = $result['telefone'];
		    $endereco = $result['endereco'];
		    $estado = $result['estado'];
		    $cidade = $result['cidade'];
		    $sobre = $result['sobre'];
		}
	
	include_once 'menu.php'; 
	
?>
		<div class="row">
			<div class="container">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-primary">
						<div class="panel panel-heading">
							<span><?php echo $nome_fantasia; ?></span>
						</div>
						<div class="panel panel-body">
							<div id="imgEmpresa" style="background: url('admin/<?php echo $imagem; ?>') 50% 50% no-repeat;"></div>
							<div class="form-group">
								<p><b>Razão Social / Nome</b></p>
								<span class="spanInfoEmprego"><?php echo $razaosoc; ?></span>
							</div>
							<div class="form-group">
								<p><b>Nome Fantasia</b></p>
								<span class="spanInfoEmprego"><?php echo $nome_fantasia; ?></span>
							</div>
							<?php  
								if ($cidade != "") {
								?>
								<div class="form-group">
									<p><b>Local</b></p>
									<span class="spanInfoEmprego"><?php echo $cidade; ?> - <?php echo $estado ?></span>
								</div>
								<?php
								}
							?>				
							<?php  
								if ($sobre != "") {
								?>
								<div class="form-group">
									<p><b>Sobre</b></p>
									<span class="spanInfoEmprego"><?php echo $sobre; ?></span>
								</div>
								<?php
								}
							?>		
						</div>
					</div>
				</div>
			</div>
		</div>

<?php include_once 'footer.php'; ?>
