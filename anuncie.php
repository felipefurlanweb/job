<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php include_once 'menu.php'; ?>
		<?php 

			if (isset($_POST['submit'])) {
				$_SESSION['login']['email'] = $_POST['email'];
				$_SESSION['login']['senha'] = $_POST['senha'];
			}

		?>
		<div class="row">
			<div class="col-md-12" id="boxAnuncie">
				<?php 

					if ($_SESSION['login']['email'] == "") {
						echo
						'
							<form id="formLoginAnuncie" action="anuncie.php" method="post">
								<table>
								<caption>Faça login para anunciar, ou <a href="">cadastre-se aqui</a></caption>
									<tr>
										<td id="tdDireito"><label>EMAIL</label></td>
										<td><input type="email" class="form-control" name="email"></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>SENHA</label></td>
										<td><input type="password" class="form-control" name="senha"></td>
									</tr>	
									<tr>
										<td></td>
										<td><input type="submit" name="submit" class="form-control" value="ENTRAR"></td>
									</tr>			
								</table>
							</form>
						';
					}else{
						echo
						'
							<form>
								<table>
									<tr>
										<td id="tdDireito"><label>NOME DA VAGA</label></td>
										<td><input type="text" class="form-control" name="nomeVaga"></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>REQUISITOS</label></td>
										<td><textarea class="form-control" name="requisitos"></textarea></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>PRINCIPAIS ATIVIDADES</label></td>
										<td><textarea class="form-control" name="principaisAtividades"></textarea></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>ESTADO</label></td>
										<td><select id="estado" class="form-control" name="estado"></select></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>CIDADE</label></td>
										<td><select id="cidade" class="form-control" name="estado">
											<option>Selecione um estado ...</option>
										</select></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>EMPRESA</label></td>
										<td><select class="form-control" name="estado">
											<option>Selecione uma empresa ...</option>
											<option value="1">Empresa 1</option>
											<option value="2">Empresa 2</option>
											<option value="3">Empresa 3</option>
											<option value="4">Empresa 4</option>
											<option value="5">Empresa 5</option>
										</select></td>
									</tr>
									<tr>
										<td id="tdDireito"><label>SALÁRIO</label></td>
										<td><input type="text" class="form-control valorReal" name="salario"></td>
									</tr>	
									<tr>
										<td></td>
										<td><input type="submit" class="form-control" value="ENVIAR"></td>
									</tr>			
								</table>
							</form>
						';
					}

				?>

			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
			});	
		</script>
		<?php include_once 'footer.php'; ?>
	</div>
</body>
</html>