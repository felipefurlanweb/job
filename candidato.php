<?php include_once 'head.php'; 

$idUsuario = $_GET['idUsuario'];
$query = "SELECT * FROM usuarios WHERE id = '$idUsuario'";
$query = mysql_query($query);
while ($result = mysql_fetch_array($query)) {
  $nome = $result['nome'];
  $cpf_cnpj = $result['cpf'];
  $email = $result['email'];
  $telefone = $result['telefone'];
  $endereco = $result['endereco'];
  $estado = $result['estado'];
  $cidade = $result['cidade'];
  $rede_social = $result['rede_social'];
  $datanasc = $result['datanasc'];
  $imagem = $result['imagem'];
}
if ($imagem == 'images/userDefault.jpg') {
  $imagem = 'img/userDefault.jpg';
}else{
  $imagem = 'admin/'.$imagem;
}
?>
<script type="text/javascript">
$(document).ready(function() {
  new dgCidadesEstados({
    estado: document.getElementById('estado'),
    cidade: document.getElementById('cidade'),
    estadoVal: '<?php echo $estado; ?>',
    cidadeVal: '<?php echo $cidade; ?>'
  });
});
</script>
<script src="admin/js/jasny.js"></script>
<script src="js/mascara.telefone.js"></script>
<link rel="stylesheet" type="text/css" href="admin/css/jasny.css">

  <div class="row">
    <div class="container">
      <div class="col-xs-12 col-md-12">
        <?php
          /*$query = "SELECT * FROM usuarios_planos WHERE idUsuario = $idUsuario ORDER BY data DESC LIMIT 1";
          $query = mysql_query($query);
          $numRows = mysql_num_rows($query);
          if ($numRows > 0) {
            ?>
            <div class="alert alert-success">
              <b>TEM PLANO</b>
            </div>
            <?php
          }else{
            ?>
            <div class="alert alert-danger">
              <b>NÃO TEM PLANO</b>
            </div>
            <?php
          }*/
        ?>
      </div>
    </div>        
  </div>           

  <div class="row">
    <div class="container">
      
        <form action="admin/engine/formularios.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id" value="editUser">
          <input type="hidden" name="idUsuario" value="<?php echo $idUsuario; ?>">
          <input type="hidden" name="imagemAntiga" value="<?php echo $imagem; ?>">
          
            <div class="col-xs-12 col-md-12 text-left">
              <div class="col-xs-12 col-md-2 semPadding">
                <div class="fileinput form-group fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo $imagem; ?>" alt="">
                  </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                  <div>
                    <span class="btn btn-default btn-file">
                      <span class="fileinput-new">Selecione uma imagem</span>
                      <span class="fileinput-exists">Alterar</span>
                      <input type="file" name="imagem">
                    </span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                  </div>
                </div>
                <div>
                  <a href="alterarSenha.php?idUsuario=<?php echo $idUsuario; ?>" class="btn btn-warning">Alterar Senha</a>
                </div>
              </div>
              <div class="col-xs-12 col-md-9">
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Nome: </label>
                  <input readonly class="form-control" type="text" name="nome" value="<?php echo $nome; ?>" />
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>CPF/CNPJ: </label>
                  <input readonly class="form-control" type="text" name="cpf_cnpj" value="<?php echo $cpf_cnpj; ?>" />
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>E-mail: </label>
                  <input class="form-control" type="email" name="email" value="<?php echo $email; ?>" />
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Telefone: </label>
                  <input class="form-control" type="text" name="telefone" value="<?php echo $telefone; ?>" id="telefone">
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Endereço: </label>
                  <input class="form-control" type="text" name="endereco" value="<?php echo $endereco; ?>"/>
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Data Nasc.: </label>
                  <input readonly class="form-control" type="text" name="datanasc" value="<?php echo $datanasc; ?>" />
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Cargos de interesse: </label>
                  <div class="marginTop">

                      <?php 

                        $sql = "SELECT * FROM cargos_interesses WHERE idUsuario = $idUsuario";
                        $sql = mysql_query($sql);
                        $h = 0;
                        while ($res = mysql_fetch_array($sql)) {
                          $h++;
                          $nome = $res["nome"];
                          ?>
                          <div class="marginTop cargo<?php echo $h; ?>">
                            <span onclick="deletaCargo(<?php echo $h; ?>)" class="btn btn-danger">
                              <span class="glyphicon glyphicon-trash"></span>
                            </span>
                            <input READONLY class="marginTop 
                            form-control" type="text" 
                            name="cargos[]" value="<?php echo $nome; ?>">
                            <hr/>
                          </div>
                          <?php
                        }


                      ?>

                    </div>  
                  <div class="marginTop">
                    <span class="btn btn-primary btnAddCargo">Adicionar cargos</a>
                  </div>
                  <div class="marginTop cargos">
                    <div class="getSelect">
                        <label>Cargo de interesse: </label>
                        <select class="form-control selectCargo" onchange="setaCargo()">
                          <option value="">Selecione uma opção ...</option>
                          <?php 
                          $query = "SELECT * FROM nome_empregos ORDER BY emprego ASC";
                          $query = mysql_query($query);
                          while ($result = mysql_fetch_array($query)) {
                            $nome = $result['emprego'];
                            $nome = strtoupper($nome);
                            $nome = utf8_encode($nome);
                            echo '<option value="'.$nome.'">'.$nome.'</option>';
                          }
                          ?>
                        </select>
                    </div>
                  </div>
                  <script type="text/javascript">
                    var y = 0;

                    function deletaCargo(id){
                      $(".cargo"+id).remove();
                    }

                    function setaCargo(){
                      y++;
                      var opcao = $(".selectCargo").val();
                      $(".cargos").append('<input type="hidden" name="cargos[]" value="'+opcao+'">');
                      $(".cargos").append('<div class="marginTop cargo'+y+'"><span onclick="deletaCargo('+y+')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></span><input READONLY class="marginTop form-control" type="text" value="'+opcao+'"><hr/></div>');
                      $(".selectCargo").val('');
                      $(".getSelect").hide();
                    }

                    $(document).ready(function() {
                      $(".getSelect").hide();
                      var x = 0;
                      $(".btnAddCargo").click(function(event) {
                        $(".getSelect").show();
                      });
                    });
                  </script>
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Rede social: </label>
                  <input class="form-control" type="text" name="rede_social" value="<?php echo $rede_social; ?>" />
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Estado: </label>
                  <select class="form-control" id="estado" name="estado"></select>
                </div>
                <div class="col-xs-12 col-md-4 form-group">
                  <label>Cidade: </label>
                  <select class="form-control" id="cidade" name="cidade"></select>
                </div>         
                <div class="col-xs-12 col-md-12 text-right">
                  <button class="btn btn-success" type="submit">Salvar</button></td>
                </div>       
              </div>
            </div>
          
        </form>  
      
    </div>   
  </div>
<?php include_once 'footer.php'; ?>
