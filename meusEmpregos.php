<?php include_once 'head.php'; ?>
 
  <?php 
    $idUsuario = $_GET['idUsuario']; 
  ?>
</head>
<body>
  <div class="container-fluid">
    <?php include_once 'menu.php'; ?>
      <div class="container">
        <div class="row marginCimaBaixo">
            <div class="col-xs-12 col-lg-12 text-center">
              <h3>Vagas que me candidatei</h3>
            </div>
            <div class="col-xs-12 col-lg-12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Vaga</th>
                    <th>Empresa</th>
                    <td>Status</td>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $query = "SELECT * FROM candidatos cand
                      INNER JOIN usuarios users ON cand.id_usuario = users.id_usuario
                      INNER JOIN (SELECT nome as nomeEmprego, id_emprego FROM empregos) empre ON cand.id_emprego = empre.id_emprego
                      WHERE users.id_usuario = $idUsuario";
                      $query = mysql_query($query);
                      $numRows = mysql_num_rows($query);
                      if ($numRows > 0) {

                        while ($result = mysql_fetch_array($query)) {
                          $nomeEmprego = $result['nomeEmprego'];
                          $id_emprego = $result['id_emprego'];
                          $status = $result['status'];
                          $query2 = "SELECT * FROM empregos empreg INNER JOIN (SELECT razaosoc as nomeEmpresa, id_empresa FROM empresas) empres ON empreg.id_empresa = empres.id_empresa WHERE empreg.id_emprego = '$id_emprego'";
                          $query2 = mysql_query($query2);
                          while($result2 = mysql_fetch_array($query2)) {
                            $nomeEmpresa = $result2['nomeEmpresa'];
                          }
                          if ($status == 'rejeitado' || $status == NULL) {
                            $status = 'Pendente';
                          }
                          echo
                          '
                            <tr>
                              <td>'.$nomeEmprego.'</td>
                              <td>'.$nomeEmpresa.'</td>
                              <td>'.$status.'</td>
                            </tr>
                          ';
                        }
                      }else{
                        echo '<div class="erro"> Nenhuma candidatura foi encontrada</div>';
                      }
                  ?>

                </tbody>
              </table>
            </div>
        </div>
      </div>
    <?php include_once 'footer.php'; ?>
  </div>
</body>
</html>