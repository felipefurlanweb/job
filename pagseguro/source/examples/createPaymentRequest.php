<?php

if (!isset($_SESSION)) {
    session_start();
}

    include_once '../../../banco.php'; 

/*
 * ***********************************************************************
 Copyright [2011] [PagSeguro Internet Ltda.]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 * ***********************************************************************
 */

require_once "../PagSeguroLibrary/PagSeguroLibrary.php";

/**
 * Class with a main method to illustrate the usage of the domain class PagSeguroPaymentRequest
 */
class CreatePaymentRequest
{

    public static function main()
    {

        /*

        Email: c86207974537411568095@sandbox.pagseguro.com.br 
        Senha: T9el4eC3ntyvFyw1 
        69418563436 cpf fake
        Número: 4111 1111 1111 1111 
        Bandeira: VISA Válido até: 12/2030 CVV: 123


        */


        // Instantiate a new payment request
        $paymentRequest = new PagSeguroPaymentRequest();

        $tipoCadastro = $_GET['tipoCadastro'];

        if ($tipoCadastro == "candidato") {

            $id = $_SESSION['job']['usuario']['id'];
            $planoId = $_SESSION['job']['usuario']['plano'];

            if ($planoId == '1') { $plano = "30 dias curriculum publicado - Candidato"; $valorPlano = 5.90;}
            if ($planoId == '2') { $plano = "60 dias curriculum publicado - Candidato"; $valorPlano = 7.90;}
            if ($planoId == '3') { $plano = "90 dias curriculum publicado - Candidato"; $valorPlano = 10.90;}

            $paymentRequest->addItem($planoId, $plano, '1', $valorPlano);

            $query = "SELECT * FROM usuarios WHERE id_usuario = $id";
            $query = mysql_query($query);
            while ($result = mysql_fetch_array($query)) {
                $nome = $result['nome'];
                $cpf = $result['cpf'];
                $email = $result['email'];
                $endereco = $result['endereco'];
                $telefone = $result['telefone'];
                $estado = $result['estado'];
                $cidade = $result['cidade'];
                $ddd = substr($telefone, 0,2);
                $telefone = substr($telefone, 2);
            }

            // Set shipping information for this payment request
            // $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
            // $paymentRequest->setShippingType($sedexCode);
            // $paymentRequest->setShippingAddress();


            // Set your customer information.
            // $paymentRequest->setSender(
            //     $nome,
            //     $email
            // );

        }else{

            $razaosoc = $_SESSION['job']['painel']['razaosoc'];
            $cnpj = $_SESSION['job']['empresa']['cnpj'];
            $email = $_SESSION['job']['painel']['email'];
            $senha = $_SESSION['job']['painel']['senha'];
            $planoId = $_SESSION['job']['painel']['plano'];

            if ($planoId == '1') { $plano = "Plano Mensal - Facejobs"; $valorPlano = 18.90; }
            if ($planoId == '2') { $plano = "Plano Anual - Facejobs"; $valorPlano = 15.90; }

            $paymentRequest->addItem($planoId, $plano, '1', $valorPlano);

            $query = "SELECT * FROM empresas WHERE cnpj = '$cnpj' ";
            $query = mysql_query($query);
            while ($result = mysql_fetch_array($query)) {
                $nomeFantasia = $result['nome_fantasia'];
                $email = $result['email'];
                $endereco = $result['endereco'];
                $estado = $result['estado'];
                $cidade = $result['cidade'];
            }

            // Set shipping information for this payment request
            $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
            $paymentRequest->setShippingType($sedexCode);
            $paymentRequest->setShippingAddress(
                '',
                $endereco,
                '',
                '',
                '',
                $cidade,
                $estado,
                'BRA'
            );

            // Set your customer information.
            // $paymentRequest->setSender(
            //     $email
            // );

        }
        
        $paymentRequest->setCurrency("BRL");

        $paymentRequest->setReference("REF123");

        // $paymentRequest->setRedirectUrl("http://facejobs.com.br/retornoPagseguro.php");
        
        // DESCONTOS 
        // $paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 1.00, 'DISCOUNT_PERCENT');  
        // $paymentRequest->addPaymentMethodConfig('EFT', 2.90, 'DISCOUNT_PERCENT');  
        // $paymentRequest->addPaymentMethodConfig('BOLETO', 10.00, 'DISCOUNT_PERCENT');  
        // $paymentRequest->addPaymentMethodConfig('DEPOSIT', 3.45, 'DISCOUNT_PERCENT');  
        // $paymentRequest->addPaymentMethodConfig('BALANCE', 0.01, 'DISCOUNT_PERCENT');  
        
        try {

            // seller authentication
            // $credentials = new PagSeguroAccountCredentials("felipe_sistemas_@hotmail.com",
            //     "6AA9A7D0AE98461F89A3176B13E3957D");
            $credentials = new PagSeguroAccountCredentials("felipe_sistemas_@hotmail.com",
            // sandbox
            "6AA9A7D0AE98461F89A3176B13E3957D");
            // producao
            // "B9AE4CF3CBEC4F22A50E47D2C5088764");

            // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
            $url = $paymentRequest->register($credentials);

            self::printPaymentUrl($url);
            

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public static function printPaymentUrl($url)
    {

            $tipoCadastro = $_GET['tipoCadastro'];

            if ($tipoCadastro == "candidato") {
                $idUsuario = $_SESSION['job']['usuario']['id'];
                $cpfcnpj = $_SESSION['job']['usuario']['cpf'];
                $planoId = $_SESSION['job']['usuario']['plano'];

                if ($planoId == '1') { $plano = "30 dias"; $autor = "candidato"; }
                if ($planoId == '2') { $plano = "60 dias"; $autor = "candidato"; }
                if ($planoId == '3') { $plano = "90 dias"; $autor = "candidato"; }

                $query = "INSERT INTO usuarios_planos (idUsuario, plano, status) VALUES ('$idUsuario', '$plano', 'Aguardando Pagamento')";
                $query = mysql_query($query);
                echo "<script> window.location.href = '$url'; </script>";

            }else{
                
                $cpfcnpj = $_SESSION['job']['painel']['cpfcnpj'];

                $planoId = $_SESSION['job']['painel']['plano'];

                if ($planoId == '1') { $plano = "30 dias"; $autor = "empresa"; }
                if ($planoId == '2') { $plano = "Anual"; $autor = "empresa"; }

                $query = "INSERT INTO planos_cadastrados (cpf_cnpj, autor, plano, status, data) VALUES ('$cpfcnpj', '$autor', '$plano', 'Aguardando Pagamento', NOW())";
                $query = mysql_query($query);
                $query = "SELECT * FROM planos_cadastrados ORDER BY id_plano_cadastrado DESC LIMIT 1";
                $query = mysql_query($query);
                while ($result = mysql_fetch_array($query)) {
                    $idPagseguro = $result['id_plano_cadastrado'];
                }
                // $_SESSION['job']['painel']['idPagseguro'] = $idPagseguro;
                echo "<script> window.location.href = '$url'; </script>";

            }
    }
}

CreatePaymentRequest::main();
