<html>
<head>
  <?php include_once 'head.php'; ?>
    <?php 
      $idUsuario = $_GET['idUsuario'];
    ?>
</head>
<body>
  <div class="container-fluid">
    <?php include_once 'menu.php'; ?>
    <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <form action="admin/engine/formularios.php" method="post">
                <input type="hidden" name="id" value="alterarSenha">
                <input type="hidden" name="idUsuario" value="<?php echo $idUsuario; ?>">
                <label>Senha Antiga:</label>
                <input type="password" name="senhaAntiga" class="form-control">
                <label>Nova Senha:</label>
                <input type="password" name="senhaNova" class="form-control">
                <input type="submit" value="Salvar" class="btn btn-success marginCimaBaixo">
              </form>
            </div>
          </div>
        </div>
    </div>
    <?php include_once 'footer.php'; ?>
  </div>
</body>
</html>