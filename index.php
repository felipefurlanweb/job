<?php include_once 'head.php'; ?>

		<section>
			<div class="container">
				<div class="col-md-12 semPadding">
					<?php 
					
					if (@$_SESSION['job']['usuario']['id'] != "") {

						$idUser = $_SESSION['job']['usuario']['id'];
						$query = "SELECT * FROM cvs WHERE id_usuario = $idUser";

						$query = mysql_query($query);

						$numRows = mysql_num_rows($query);

						while ($result = mysql_fetch_array($query)) {

							$idCurriculum = $result['id_curriculum'];

						}

						if ($numRows == 0) {

							echo

							'

							<div class="marginBottom">
								<a href="cadastrarCv.php" class="btn btn-success">
									CADASTRAR CURRICULUM
								</a>
							</div>

							';

						}
					}

					?>
				</div>
			</div>
		</section>

		<section>
			<div class="container">

				<?php include_once 'buscaRapida.php'; ?>

				<div class="col-xs-12 col-md-8" id="slideshow">

					<div id="slideshowCarousel" class="owl-carousel">
						<?php 
							$query = "SELECT * FROM slideshow WHERE ativo = 1";
							$query = mysql_query($query);
							while ($result = mysql_fetch_array($query)) {
								$imagem = $result['imagem'];
								echo '<div class="imgSlideshow" style="background: url(admin/'.$imagem.') 50% 50% no-repeat;"></div>';
							}
						?>
					</div>

				</div>			



			</div>
		</section>
		

			<?php 

			//SE LOGIN FOR EMPRESA
			if (@$_SESSION['job']['empresa']['id'] != "") {

				?>
				<section>
					<div class="container">
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-primary">

								<div class="panel-heading">
									<span>CANDIDATOS</span>
								</div>

								<div class="panel-body">
								<?php
									$query = "SELECT * FROM cvs cv 
									INNER JOIN usuarios u ON cv.id_usuario = u.id
									WHERE cv.ativo = 1
									ORDER BY u.nome ASC LIMIT 10";
									$query = mysql_query($query);
									$numRows = mysql_num_rows($query);
									if ($numRows > 0) {
										while ($result = mysql_fetch_array($query)) {
											$id_curriculum = $result['id_curriculum'];
											$nome = $result['nome'];
											$email = $result['email'];
											$cargoInteresse = $result['cargo_interesse1'];
											$imagemUsuario = $result['imagem'];
											$escolaridade = $result['escolaridade'];
											if($escolaridade == 'doutoradoCompleto' ) $escolaridade = 'Doutorado Completo';
											if($escolaridade == 'doutoradoIncompleto' ) $escolaridade = 'Doutorado Incompleto';
											if($escolaridade == 'mestradoCompleto' ) $escolaridade = 'Mestrado Completo';
											if($escolaridade == 'mestradoIncompleto' ) $escolaridade = 'Mestrado Incompleto';
											if($escolaridade == 'posGraduacaoCompleta' ) $escolaridade = 'Pós-graduação Completa';
											if($escolaridade == 'posGraduacaoIncompleta' ) $escolaridade = 'Pós-graduação Incompleta';
											if($escolaridade == 'superiorCompleto' ) $escolaridade = 'Superior Completo';
											if($escolaridade == 'superiorIncompleto' ) $escolaridade = 'Superior Incompleto';
											if($escolaridade == 'ensinoMedioCompleto' ) $escolaridade = 'Ensino Médio Completo';
											if($escolaridade == 'ensinoMedioIncompleto' ) $escolaridade = 'Ensino Médio Incompleto';	
											if($escolaridade == 'ensinoFundamentalCompleto' ) $escolaridade = 'Ensino Fundamental Completo';
											if($escolaridade == 'ensinoFundamentalIncompleto' ) $escolaridade = 'Ensino Fundamental Incompleto';
											if($escolaridade == 'naoAlfabetizado' ) $escolaridade = 'Não Alfabetizado';
											$cidade = $result['cidade'];
											$estado = $result['estado'];
											$objetivo = $result['objetivo'];
											echo
											'
											<div class="col-xs-12 col-md-2">
												<div>
													<p><b>'.$nome.'</b></p>
													<hr>
													<p><b>Cargo de Interesse</b></p>
													<p>'.$cargoInteresse.'</p>
													<p><b>Escolaridade</b></p>
													<p>'.$escolaridade.'</p>
													<p><b>Cidade</b></p>
													<p>'.$cidade.' - '.$estado.'</p>
													<a href="verCv.php?idCv='.$id_curriculum.'" class="btn btn-primary">Ver Curriculum</a>
												</div>
											</div>
											';						
										}
									}

								?>	
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php
			}

			//SE LOGIN FOR CANDIDATO
			if (@$_SESSION['job']['usuario']['id'] != "") {

				?>
				<section>
					<div class="container">
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-primary">
								<div class="panel panel-heading">
									<span>VAGAS</span>
								</div>
								<div class="panel panel-body">
								<?php

									$query = "SELECT * FROM vagas v LEFT JOIN (SELECT nome_fantasia as nomeEmpresa, id as idEmpresa, ativo, imagem FROM empresas ) em ON v.idEmpresa = em.idEmpresa ORDER BY v.nome ASC LIMIT 9";
									$query = mysql_query($query);
									$numRows = "";
									$numRows = mysql_num_rows($query);
									if ($numRows > 0) {
										while ($result = mysql_fetch_array($query)) {

											$id = $result["id"];
											$nome = $result["nome"];
											$nomeEmpresa = $result["nomeEmpresa"];
											$tipo_vaga = $result["tipo_vaga"];
											$horario = $result["horario"];
											$data = $result["data"];

											if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
											if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
											if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
											if($horario == "comercial") { $horarioString = "Comercial"; } 
											if($horario == "2turno") { $horarioString = "2ª turno"; } 
											$data = FormataData_Brasil($data,"tsp");
											echo
											'
												<div class="col-xs-12 col-md-3 vaga">
													<div class="form-group">
														<span>
															<b>'.$nome.'</b>
														</span>
														<span>
															<small>
																'.$data.'
															</small>
														</span>
													</div>
													<div class="form-group">
														<span>'.$nomeEmpresa.'</span>
													</div>
													<div>
														<a class="btn btn-primary" href="vaga.php?id='.$id.'">
															Mais informações
														</a>
													</div>



												</div>
											';							

										}
									}
								?>
									</div>
								</div>
						</div>
					</div>
				</section>

				<?php 
			}

			// SEM CADASTRO
			$semLogar = count(@$_SESSION['job']); 
			if ($semLogar == 0) {
				?>
				<section>
					<div class="container">
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-primary">
								<div class="panel panel-heading">
									<span>VAGAS</span>
								</div>
								<div class="panel panel-body">
								<?php

									$query = "SELECT * FROM vagas v LEFT JOIN (SELECT nome_fantasia as nomeEmpresa, id as idEmpresa, ativo, imagem FROM empresas ) em ON v.idEmpresa = em.idEmpresa ORDER BY v.nome ASC LIMIT 9";
									$query = mysql_query($query);
									$numRows = "";
									$numRows = mysql_num_rows($query);
									if ($numRows > 0) {
										while ($result = mysql_fetch_array($query)) {

											$id = $result["id"];
											$nome = $result["nome"];
											$nomeEmpresa = $result["nomeEmpresa"];
											$tipo_vaga = $result["tipo_vaga"];
											$horario = $result["horario"];
											$data = $result["data"];

											if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
											if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
											if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
											if($horario == "comercial") { $horarioString = "Comercial"; } 
											if($horario == "2turno") { $horarioString = "2ª turno"; } 
											$data = FormataData_Brasil($data,"tsp");
											echo
											'
												<div class="col-xs-12 col-md-3 vaga">
													<div class="form-group">
														<span>
															<b>'.$nome.'</b>
														</span>
														<span>
															<small>
																'.$data.'
															</small>
														</span>
													</div>
													<div class="form-group">
														<span>'.$nomeEmpresa.'</span>
													</div>
													<div>
														<a class="btn btn-primary" href="vaga.php?id='.$id.'">
															Mais informações
														</a>
													</div>



												</div>
											';							

										}
									}
								?>
									</div>
								</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="col-xs-12 col-md-12">
							
								<div class="panel panel-primary">
									<div class="panel panel-heading">
										<span>CURSOS</span>
									</div>
									<div class="panel panel-body">
										<?php
											$query = "SELECT * FROM cursos ORDER BY data DESC LIMIT 10";
											$query = mysql_query($query);
											$numRows = mysql_num_rows($query);
											if ($numRows > 0) {
												while ($result = mysql_fetch_array($query)) {
													$id = $result["id"];
													$nome = $result["nome"];
													$estado = $result["estado"];
													$cidade = $result["cidade"];
													echo
													'
													<div class="col-xs-12 col-md-4">
														<div>
															<b>'.$nome.'</b>
														</div>
														<div>
															<span>Local: '.$cidade.' - '.$estado.'</span>
														</div>
														<div class="marginTop">
															<a class="btn btn-primary" href="curso.php?id='.$id.'">
																Leia mais
															</a>
														</div>
													</div>
													';						
												}
											}
										?>	
									</div>
								</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="col-xs-12 col-md-12">
							
								<div class="panel panel-primary">
									<div class="panel panel-heading">
										<span>CANDIDATOS</span>
									</div>
									<div class="panel panel-body">
										<?php
											$query = "SELECT * FROM cvs cv 
											INNER JOIN usuarios u ON cv.id_usuario = u.id
											WHERE cv.ativo = 1
											ORDER BY u.nome ASC LIMIT 10";
											$query = mysql_query($query);
											$numRows = mysql_num_rows($query);
											if ($numRows > 0) {
												while ($result = mysql_fetch_array($query)) {
													$id_curriculum = $result['id_curriculum'];
													$nome = $result['nome'];
													$email = $result['email'];
													$cargoInteresse = $result['cargo_interesse1'];
													$imagemUsuario = $result['imagem'];
													$escolaridade = $result['escolaridade'];
													if($escolaridade == 'doutoradoCompleto' ) $escolaridade = 'Doutorado Completo';
													if($escolaridade == 'doutoradoIncompleto' ) $escolaridade = 'Doutorado Incompleto';
													if($escolaridade == 'mestradoCompleto' ) $escolaridade = 'Mestrado Completo';
													if($escolaridade == 'mestradoIncompleto' ) $escolaridade = 'Mestrado Incompleto';
													if($escolaridade == 'posGraduacaoCompleta' ) $escolaridade = 'Pós-graduação Completa';
													if($escolaridade == 'posGraduacaoIncompleta' ) $escolaridade = 'Pós-graduação Incompleta';
													if($escolaridade == 'superiorCompleto' ) $escolaridade = 'Superior Completo';
													if($escolaridade == 'superiorIncompleto' ) $escolaridade = 'Superior Incompleto';
													if($escolaridade == 'ensinoMedioCompleto' ) $escolaridade = 'Ensino Médio Completo';
													if($escolaridade == 'ensinoMedioIncompleto' ) $escolaridade = 'Ensino Médio Incompleto';	
													if($escolaridade == 'ensinoFundamentalCompleto' ) $escolaridade = 'Ensino Fundamental Completo';
													if($escolaridade == 'ensinoFundamentalIncompleto' ) $escolaridade = 'Ensino Fundamental Incompleto';
													if($escolaridade == 'naoAlfabetizado' ) $escolaridade = 'Não Alfabetizado';
													$cidade = $result['cidade'];
													$estado = $result['estado'];
													$objetivo = $result['objetivo'];
													echo
													'
													<div class="col-xs-12 col-md-2">
														<div>
															<p><b>'.$nome.'</b></p>
															<hr>
															<p><b>Cargo de Interesse</b></p>
															<p>'.$cargoInteresse.'</p>
															<p><b>Escolaridade</b></p>
															<p>'.$escolaridade.'</p>
															<p><b>Cidade</b></p>
															<p>'.$cidade.' - '.$estado.'</p>
															<a href="verCv.php?idCv='.$id_curriculum.'" class="btn btn-primary">Ver Curriculum</a>
														</div>
													</div>
													';						
												}
											}
										?>	
									</div>
								</div>
						</div>
					</div>
				</section>
				<?php
			}
			?>

	<script type="text/javascript">

	$(document).ready(function() {

		$("#slideshowCarousel").owlCarousel({
			autoPlay: 10000,
			items : 1
		});

	});

	</script>

<?php include_once 'footer.php'; ?>


