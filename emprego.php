<?php include_once 'head.php';

	$idEmprego = $_GET['idEmprego'];
	$idEmpregoDefault = $_GET['idEmprego'];
	include_once 'menu.php'; 


	$query = "SELECT * FROM empregos em
	INNER JOIN empresas e ON em.id_empresa = e.id_empresa
	WHERE em.id_emprego = '$idEmpregoDefault'";

	$query = mysql_query($query);

	while ($result = mysql_fetch_array($query)) {

		$nomeEmpresa = $result['nome_fantasia'];
		$id_empresa = $result['id_empresa'];

	}

	$query = "SELECT * FROM empregos WHERE id_emprego = '$idEmpregoDefault'";

	$query = mysql_query($query);

	while ($result = mysql_fetch_array($query)) {

		$nome = $result['nome'];

		$empresa = $result['id_empresa'];

		$principaisAtividades = $result['principais_atividades'];

		$estado = $result['estado'];

		$cidade = $result['cidade'];

		$requisitos = $result['requisitos'];

		$salario = $result['salario'];


		$tipo_vaga = $result['tipo_vaga'];
		$horario = $result['horario'];
		if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
		if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
		if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }
		if($horario == "comercial") { $horarioString = "Comercial"; } 
		if($horario == "2turno") { $horarioString = "2ª turno"; } 

	}

?>
<script type="text/javascript">

window.onload = function() {

	new dgCidadesEstados({

		cidade: document.getElementById('cidade'),

		estado: document.getElementById('estado'),

		estadoVal: '<?php echo $estado; ?>',

		cidadeVal: '<?php echo $cidade; ?>'

	});

}

</script>

<div class="container">
	<div class="row">


		<div class="col-xs-12 col-md-12">


			<?php

			if (@$_SESSION['job']['usuario']['cpf'] != "") {

				$idUsuario = $_SESSION['job']['usuario']['id'];

				$query = "SELECT * FROM candidatos WHERE id_emprego = '$idEmprego' AND id_usuario = '$idUsuario'";

				$query = mysql_query($query);

				$numRows = mysql_num_rows($query);

				if ($numRows > 0) {

					echo

					'

					<div>
						<div class="alert alert-info">VOCÊ JÁ SE CANDIDATOU PARA ESTE EMPREGO</div>
					</div>
					';

				}
				$sql = "SELECT * FROM usuarios_planos WHERE idUsuario = $idUsuario AND status = 'pago'";
				$sql = mysql_query($sql);
				$numRows = mysql_num_rows($sql);
				if ($numRows > 0) {
					echo

					'

					<div>

						<form action="admin/engine/formularios.php" method="post">

							<input type="hidden" name="id" value="candidatarEmprego">

							<input type="hidden" name="idEmprego" value="'.$idEmprego.'">

							<input type="hidden" name="idUsuario" value="'.$idUsuario.'">

							<input type="submit" class="form-control btn btn-success" value="CANDIDATAR-SE">

						</form>

					</div>

					';
				}else{
					$sql = "SELECT * FROM usuarios_planos WHERE idUsuario = $idUsuario";
					$sql = mysql_query($sql);
					$res = mysql_fetch_assoc($sql);
					if ($res["status"] == "Aguardando Pagamento") {
						?>
						<div class="alert alert-warning text-center">
							Seu pagamento está pendente, após confirmado, você poderá se candidatar-se.
						</div>
					<?php
					}else{
					?>
					<div class="alert alert-info text-center">
						Você deve ser assinante para se candidatar-se.
						<h3>
							<a href="planoCandidato.php?id=<?php echo $idUsuario; ?>">ASSINE JÁ!</a>
						</h3>
					</div>
					<?php
					}
				}
			}else{
				$urlVolta = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				echo
				'
					<div>
						<a href="login.php?urlVolta='.$urlVolta.'" class="btn btn-success">Faça login para se candidatar</a>
					</div> 
				';
			}


			?>

		</div>
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-primary">

				<div class="panel-heading">

					<span>INFORMAÇÕES</span>

				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Vaga</label>
						<input class="form-control" readonly type="text" value="<?php echo $nome; ?>">
					</div>
					<div class="form-group">
						<label>Empresa</label>
						<div class="marginBottom">
							<input class="form-control" readonly type="text" value="<?php echo $nomeEmpresa; ?>">
						</div>
						<a href="empresa.php?idEmpresa=<?php echo $id_empresa; ?>" class="btn btn-primary">Conheça a Empresa</a>
					</div>
					<div class="form-group">
						<label>Requisitos</label>
						<textarea class="form-control" readonly><?php echo $requisitos; ?></textarea>
					</div>
					<div class="form-group">
						<label>Principais Atividades</label>
						<textarea class="form-control" readonly><?php echo $principaisAtividades; ?></textarea>
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Cidade</label>
						<input class="form-control" readonly type="text" value="<?php echo $cidade; ?> - <?php echo $estado ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Tipo de vaga</label>
						<input class="form-control" readonly type="text" value="<?php echo $tipo_vagaString; ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Horário</label>
						<input class="form-control" readonly type="text" value="<?php echo $horarioString; ?>">
					</div>
					<div class="col-xs-12 col-md-6 form-group infoVaga">
						<label>Salário</label>
						<input class="form-control" readonly type="text" value="R$ <?php echo $salario; ?>">
					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<?php include_once 'footer.php'; ?>



</body>

</html>