<html>
<head>
  <?php include_once 'head.php'; ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#cnpj").mask("99.999.999/9999-99");
      $("#cpf").mask("999.999.999-99");
    });
  </script>
</head>
<body>
  
    <?php 
            include_once 'menu.php'; 
            echo mostraMensagem();
            if (isset($_GET['urlVolta'])) {
              $urlVolta = $_GET['urlVolta'];
              $_SESSION['job']['urlVolta'] = $urlVolta;
            }
        ?>
        <div class="row">
          <div class="container">
            <div class="col-xs-12 col-md-6">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Empresa
                </div>
                <div class="panel-body">
                  <form action="admin/engine/formularios.php" method="post">
                    <input type="hidden" name="id" value="empresaLogin">
                      <div class="form-group">
                        <label>CNPJ: </label>
                        <input class="form-control" type="text" name='cnpj' id="cnpj" required>
                      </div>
                      <div class="form-group">
                        <label>Senha: </label>
                        <input class="form-control" type="password" name="senha" />
                      </div>
                      <div class="form-group">
                        <a href="esqueceuSenha.php">Esqueceu sua senha?</a>
                      </div>
                      <div class="form-group">
                        <a href="esqueceuCpfCnpj.php">Esqueceu seu CNPJ?</a>
                      </div>
                      <div class="form-group">
                          <button class="btn btn-success" type="submit">Entrar</button>
                      </div>
                  </form>   
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Candidato
                </div>
                <div class="panel-body">
                  <form action="admin/engine/formularios.php" method="post">
                    <input type="hidden" name="id" value="loginUser">
                      <div class="form-group text-center">
                        <fb:login-button scope="public_profile,email" class="btnLogin" onlogin="checkLoginStateLogin(1);">
                        </fb:login-button>
                      </div>
                      <div class="form-group">
                        <label>CPF: </label>
                        <input class="form-control" type="text" name='cpf'id="cpf" required>
                      </div>
                      <div class="form-group">
                        <label>Senha: </label>
                        <input class="form-control" type="password" name="senha" />
                      </div>
                      <div class="form-group">
                        <a href="esqueceuSenha.php">Esqueceu sua senha?</a>
                      </div>
                      <div class="form-group">
                        <a href="esqueceuCpfCnpj.php">Esqueceu seu CPF?</a>
                      </div>
                      <div class="form-group">
                          <button class="btn btn-success" type="submit">Entrar</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>           
        </div>

    <?php include_once 'footer.php'; ?>
  </div>
</body>
</html>