<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php 
            include_once 'menu.php'; 
            echo mostraMensagem();
        ?>
        <div class="row">
                <div class="row" id="boxCadastrar">
                    <form action="admin/engine/formularios.php" method="post">
                      <input type="hidden" name="id" value="sugestoes">
                      <table>
                        <caption>Envie sugestões para melhorias do site</caption>
                        <tr>
                          <td id="right-form" ><label>Mensagem: </label></td>
                          <td id="left-form" ><textarea class="form-control" name="sugestao"></textarea></td>
                        </tr>
                        <tr>
                          <td id="right-form" ></td>
                          <td id="left-form" >
                            <button class="btn btn-success" type="submit">Enviar</button></td>
                        </tr>
                      </table>
                    </form>              
                </div>
        </div>
		<?php include_once 'footer.php'; ?>
	</div>
</body>
</html>