<?php 
if (!isset($_SESSION['job'])) {
	session_start();
}
include_once 'admin/engine/funcoes.php'; 
?>
<html>
<head>
	<title>@!</title>

	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="" type="image/x-icon">

    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    
    <meta name="robots" content="index, follow">
    <meta name="googlebots" content="index, follow">  
    <meta name="title" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="owl-carousel/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="owl-carousel/owl.theme.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/style.css"> -->
	<link rel="stylesheet" type="text/css" href="css/style_.css">
	<link href='http://fonts.googleapis.com/css?family=Cantarell' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/cidadesEstados.js"></script>
	<script type="text/javascript" src="owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="js/jquery.mask.js"></script>
	<script type="text/javascript" src="js/jquery.maskMoney.js"></script>


	<?php 

		include_once 'banco.php';

	?>
	<section>
		<div class="container">
			<div class="col-xs-12">
				<?php echo mostraMensagem(); ?>
			</div>
		</div>
	</section>

	<?php include_once 'menu.php'; ?>

	<script type="text/javascript">
		window.onload = function() {
			new dgCidadesEstados({
				estado: document.getElementById('estado'),
				cidade: document.getElementById('cidade')
			});
		}
	</script>
  <script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response, login) {

      if(login){
        FB.api('/me', {fields: 'picture.type(large), name, email' },
          function(response) {
            var email = response.email;
            $.ajax({
              url: 'admin/engine/formularios.php',
              type: 'POST',
              dataType: 'html',
              data: {id: 'loginUser', email: email},
              success: function(data){
                console.log(data);
                data = $.trim(data);
                if(data == "1"){
                  window.location.href = "index.php";
                }else{
                  alert("Dados Inválidos");
                }
              },
              error:function(data){
                alert("Algo deu errado, tente novamente");
              }
            });
            
          }
        );
      }else{
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          testAPI();
        } else {
          // The person is not logged into your app or we are unable to tell.
          console.log("nao logado com o facebook");

        }
      }

    }
    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.

    function loginFacebook(){
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
      checkLoginState();
    }

    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
    }

    function checkLoginStateLogin(login) {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response, login);
      });
    }

    window.fbAsyncInit = function() {
    FB.init({
      appId      : '897948350346283',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.



    };

    // Load the SDK asynchronously
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
      console.log('Welcome!  Fetching your information.... ');
      FB.api('/me', {fields: 'picture.type(large), name, email' },
        function(response) {
          $("#nomeFacebook").val(response.name);
          $("#emailFacebook").val(response.email);
          var imagem = response.picture.data.url;
          $("#imagemFacebook").attr({
            src: imagem
          });
          $("#imagemFacebookInput").val(imagem);
          $(".btnLogin").hide();
        }
      );
    }

    function desconectar(){
      FB.logout(function(response) {
         $(".btnLogin").show();
         $(".sairFace").hide();
         $("#status").hide();
      });
    }
  </script>