<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php 
            include_once 'menu.php'; 
            echo mostraMensagem();
            $idEmprego = $_GET['idEmprego'];
            $query = "SELECT * FROM empregos WHERE id_emprego = '$idEmprego'";
            $query = mysql_query($query);
            while ($result = mysql_fetch_array($query)) {
                $nome = $result['nome'];
                $empresa = $result['id_empresa'];
                $principaisAtividades = $result['principais_atividades'];
                $estado = $result['estado'];
                $cidade = $result['cidade'];
                $requisitos = $result['requisitos'];
                $salario = $result['salario'];
            }
        ?>
        <script type="text/javascript">
        window.onload = function() {
            new dgCidadesEstados({
              cidade: document.getElementById('cidade'),
              estado: document.getElementById('estado'),
              estadoVal: '<?php echo $estado; ?>',
              cidadeVal: '<?php echo $cidade; ?>'
            });
        }
        </script>
        <div class="row">
                <div class="row" id="boxCadastrar">
                <form action="admin/engine/formularios.php" method="POST" enctype="multipart/form-data">
                    <input name="id" value="empregoEdit" hidden>
                    <input name="idEmprego" value="<?php echo $idEmprego ?>" hidden>
                    <?php

                    $empresa = $_SESSION['job']['empresa']['id_empresa'];

                    ?>
                    <input type="hidden" name="empresa" value="<?php echo $empresa; ?>">

                        <div class="row">
                            <label>Nome</label>
                            <input type="text" name="nome" class="form-control" value="<?php echo $nome; ?>">
                            <label>Requisitos</label>
                            <textarea name="requisitos" class="form-control"><?php echo $requisitos; ?></textarea>
                            <label>Principais Atividades</label>
                            <textarea name="principaisAtividades" class="form-control"><?php echo $principaisAtividades; ?></textarea>
                            <label>Estado</label>
                            <select id="estado" name="estado" class="form-control"></select>
                            <label>Cidade</label>
                            <select id="cidade" name="cidade" class="form-control"></select>
                            <label>Salário</label>
                            <input type="text" class="form-control valorReal" name="salario" value="<?php echo $salario; ?>">
                        </div>
                        <br/>
                        <div class="row text-right">
                            <input type="submit" class="btn btn-success" value="Salvar">
                        </div>

                </form>          
                </div>
        </div>
		<?php include_once 'footer.php'; ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
      });
    </script>
	</div>
</body>
</html>