<?php include_once 'head.php';

include_once 'menu.php'; 

$idUsuario = $_GET['id'];
$query = "SELECT * FROM usuarios WHERE id_usuario = '$idUsuario'";
$query = mysql_query($query);
while ($result = mysql_fetch_array($query)) {
  $nome = $result['nome'];
  $cpf = $result['cpf'];
  $email = $result['email'];
}
?>
<div class="row">
  <div class="container">
    <div class="text-center">
      <h3>ESCOLHA UM PLANO PARA PUBLICAR SEU CURRICULUM</h3>
    </div>
    <div>
      <form action="admin/engine/formularios.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="planoCandidato">
        <input type="hidden" name="idUsuario" value="<?php echo $idUsuario; ?>">  

        <div class="col-xs-12 col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              PLANO 1
            </div>
            <div class="panel-body">
              <p>Seu curriculum publicado durante 30 dias.</p>
              <h2>R$5,90 / 30 dias</h2>
              <input type="radio" name="planoCandidato" value="1" required>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              PLANO 2
            </div>
            <div class="panel-body">
              <p>Seu curriculum publicado durante 60 dias.</p>
              <h2>R$7,90 / 60 dias</h2>
              <input type="radio" name="planoCandidato" value="2" required>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              PLANO 3
            </div>
            <div class="panel-body">
              <p>Seu curriculum publicado durante 90 dias.</p>
              <h2>R$10,90 / 30 dias</h2>
              <input type="radio" name="planoCandidato" value="3" required>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button class="btn btn-success" type="submit">Continuar</button>
        </div>
        </form>  
      </div>           
    </div>
  </div>
  <?php include_once 'footer.php'; ?>
</div>
</body>
</html>