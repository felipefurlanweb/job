<?php include_once 'head.php'; ?>
<?php 
	include_once 'menu.php'; 
	echo mostraMensagem();
?>

		<div class="container">
			<div class="row">

				<div class="col-xs-12 col-md-6">
					<form action="admin/engine/formularios.php" method="post">
						<input type="hidden" name="id" value="emailContato">
							<div class="form-group">
								<label>Nome</label>
								<input type="text" class="form-control" name="nome" required>
							</div>
							<div class="form-group">
								<label>Telefone</label>
								<input type="text" class="form-control" name="telefone" required>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control" name="email" required>
							</div>
							<div class="form-group">
								<label>Mensagem</label>
								<textarea class="form-control" name="mensagem" required></textarea>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" value="ENVIAR">
							</div>
					</form>
				</div>

				<div class="col-xs-12 col-md-6">

					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14753.655399493498!2d-47.55210795!3d-22.413444049999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdf87d3a31faf8a99!2sShopping+Center+Rio+Claro!5e0!3m2!1spt-BR!2sbr!4v1484230256258" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

				</div>

			</div>
		</div>

		<?php include_once 'footer.php'; ?>

	</div>

</body>

</html>