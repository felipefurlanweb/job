<?php include_once 'head.php'; ?>
      <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
      <script src="admin/js/jasny.js"></script>
      <link rel="stylesheet" type="text/css" href="admin/css/jasny.css">
      <script type="text/javascript">
        $(document).ready(function() {
          $("#datanasc").mask("99/99/9999");
          $(".cpf").mask("999.999.999-99");
          $(".cnpj").mask("99.999.999/9999-99");
        });
      </script>
  </head>
  <body>
    
      <?php include_once 'menu.php'; ?>
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
              <div class="panel-heading">
                Cadastro
              </div>
              <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"><a href="#tabEmpresa" aria-controls="tabEmpresa" role="tab" data-toggle="tab">Tenho Empresa</a></li>
                  <li role="presentation"><a href="#tabCandidato" aria-controls="tabCandidato" role="tab" data-toggle="tab">Sou Candidato</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane" id="tabEmpresa">
                    <form action="admin/engine/formularios.php" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="id" value="empresaAdd">
                      <div class="col-xs-12 col-md-12">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                            <div>
                              <span class="btn btn-default btn-file">
                                  <span class="fileinput-new">Selecione uma imagem</span>
                              <span class="fileinput-exists">Alterar</span>
                              <input type="file" name="imagem">
                              </span>
                              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                            </div>
                          </div>
                      </div>
                      <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                          <label>Razão Social: </label>
                          <input class="form-control" type="text" name="razaosoc" required/>
                        </div>
                        <div class="form-group">
                          <label>Nome Fantasia: </label>
                          <input class="form-control" type="text" name="nomefantasia" required/>
                        </div>                          
                        <div class="form-group">
                          <label>CNPJ: </label>
                          <input class="form-control cnpj" type="text" name='cnpj' required>
                        </div>                          
                        <div class="form-group">
                          <label>E-mail: </label>
                          <input class="form-control" type="email" name="email" required/>
                        </div>                          
                        <div class="form-group">
                          <label>Senha: </label>
                          <input class="form-control" type="password" name="senha" required/>
                        </div>
                        <div class="form-group">
                          <label>Telefone: </label>
                          <input class="form-control" type="text" name="telefone" maxlength="15" id="telefone">
                        </div>
                        <div class="form-group">
                          <label>Endereço: </label>
                          <input class="form-control" type="text" name="endereco" />
                        </div>
                          
                        <script type="text/javascript">
                          $(document).ready(function() {
                            new dgCidadesEstados({
                              estado: document.getElementById('estado2'),
                              cidade: document.getElementById('cidade2')
                            });
                          });
                        </script>
                          
                            <div class="form-group">
                              <label>Estado: </label>
                              <select class="form-control" id="estado2" name="estado">
                                <option value="">Selecione um estado ...</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Cidade: </label>
                              <select class="form-control" id="cidade2" name="cidade">
                                <option value="">Selecione uma cidade ...</option>
                              </select>
                            </div>
                          
                          
                            <div class="form-group">
                              <label>Ramo de Atividade: </label>
                              <input class="form-control" type="text" name="ramoAtividade" />
                            </div>
                          
                          
                            <div class="form-group">
                              <label>Conte um pouco sobre sua empresa: </label>
                              <textarea class="form-control" name="sobre"></textarea>
                            </div>
                          
                          
                            <div class="form-group">
                              <label>Termos de uso:</label>
                              <textarea READONLY class="form-control">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                              </textarea>
                              <br/>
                              <div class="col-md-12 text-right semPadding">
                                <label>Li e aceito os termos de uso</label>
                                <input type="checkbox" required>
                              </div>
                            </div>
                            <div class="form-group text-right">
                              <button class="btn btn-success" type="submit">Cadastrar</button>
                            </div>
                      </div>
                    </form>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="tabCandidato">
                    <form action="admin/engine/formularios.php" method="post" enctype='multipart/form-data'>
                      <input type="hidden" name="id" value="cadastrarUser">
                      <div class="col-xs-12 col-md-12 text-center loginFacebook">
                        <fb:login-button scope="public_profile,email" class="btnLogin" onlogin="checkLoginState();">
                        </fb:login-button>
                      </div>
                      <div class="col-xs-12 col-md-12">
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                          <input type="hidden" id="imagemFacebookInput" name="imagemFacebookInput">
                          <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                            <img src="" id="imagemFacebook">
                          </div>
                          <div>
                            <span class="btn btn-default btn-file">
                                  <span class="fileinput-new">Selecione uma imagem</span>
                            <span class="fileinput-exists">Alterar</span>
                            <input type="file" name="imagemUser">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                          </div>
                        </div>
                        
                          <div class="form-group">
                            
                              <label>Nome: </label>
                            
                            
                              <input class="form-control" type="text" name="nome" id="nomeFacebook"/>
                            
                          </div>
                          <div class="form-group">
                            
                              <label>CPF: </label>
                            
                            
                              <input class="form-control cpf" type="text" name='cpfcnpj' required>
                            
                          </div>
                          <div class="form-group">
                            
                              <label>E-mail: </label>
                            
                            
                              <input class="form-control" type="email" name="email" id="emailFacebook" required/>
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Senha: </label>
                            
                            
                              <input class="form-control" type="password" name="senha" required/>
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Telefone: </label>
                            
                            
                              <input class="form-control" type="text" name="telefone" id="telefone2" maxlength="15">
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Endereço: </label>
                            
                            
                              <input class="form-control" type="text" name="endereco" />
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Data Nasc.: </label>
                            
                            
                              <input class="form-control" type="text" name="datanasc" id="datanasc" />
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Rede social: </label>
                            
                            
                              <input class="form-control" type="text" name="rede_social" />
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Estado: </label>
                            
                            
                              <select class="form-control" id="estado" name="estado">
                                <option value="">Selecione um estado ...</option>
                              </select>
                            
                          </div>
                          <div class="form-group">
                            
                              <label>Cidade: </label>
                            
                            
                              <select class="form-control" id="cidade" name="cidade">
                                <option value="">Selecione uma cidade ...</option>
                              </select>
                            
                          </div>
                          <script type="text/javascript">
                            window.onload = function() {
                              new dgCidadesEstados({
                                estado: document.getElementById('estado'),
                                cidade: document.getElementById('cidade')
                              });
                            }
                          </script>
                          <div class="form-group">
                            
                              <label>Termos de uso:</label>
                            
                            
                              <textarea READONLY class="form-control">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                              </textarea>
                              <br/>

                          </div>
                          <div class="form-group text-right">
                              <div>
                                <label>Li e aceito os termos de uso</label>
                                <input type="checkbox" required>
                              </div>
                            
                              <button class="btn btn-success" type="submit">Cadastrar</button>
                          </div>
                        
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
        <?php include_once 'footer.php'; ?>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $(".valorReal").maskMoney({
          prefix: 'R$ ',
          allowNegative: true,
          thousands: '.',
          decimal: ',',
          affixesStay: false
        });
        $(".formEmpresa").hide();
        $(".formCandidato").hide();
        $('input[type=radio][name=tipoCadastro]').change(function() {
          if (this.value == 'candidato') {
            $(".formEmpresa").hide('slow/400/fast');
            $(".formCandidato").show('slow/400/fast');
          } else if (this.value == 'empresa') {
            $(".formCandidato").hide('slow/400/fast');
            $(".formEmpresa").show('slow/400/fast');
          }
        });
      });
    </script>
  </body>
</html>
