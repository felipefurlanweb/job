<html>
<head>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div class="container-fluid">
		<?php 
            include_once 'menu.php'; 
            echo mostraMensagem();
        ?>
        <div class="row">
                <div class="row" id="boxCadastrar">
                <form action="admin/engine/formularios.php" method="POST" enctype="multipart/form-data">
                    <input name="id" value="empregoAdd" hidden>
                    <?php

                    $empresa = $_SESSION['job']['empresa']['id_empresa'];

                    ?>
                    <input name="empresa" value="<?php echo $empresa; ?>" hidden>

                        <div class="row">
                            <label>Nome</label>
                            <input type="text" name="nome" class="form-control">
                            <label>Requisitos</label>
                            <textarea name="requisitos" class="form-control"></textarea>
                            <label>Principais Atividades</label>
                            <textarea name="principaisAtividades" class="form-control"></textarea>
                            <label>Estado</label>
                            <select id="estado" name="estado" class="form-control"></select>
                            <label>Cidade</label>
                            <select id="cidade" name="cidade" class="form-control">
                                <option>Selecione um estado ...</option>
                            </select>
                            <label>Salário</label>
                            <input type="text" class="form-control valorReal" name="salario">
                        </div>
                        <br/>
                        <div class="row text-right">
                            <input type="submit" class="btn btn-success" value="Cadastrar">
                        </div>

                </form>             
                </div>
        </div>
		<?php include_once 'footer.php'; ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
      });
    </script>
	</div>
</body>
</html>