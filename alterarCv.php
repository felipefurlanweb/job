<html>

<head>

	<?php include_once 'head.php'; ?>

  <style type="text/css">

    input[type="file"] {

        display: initial;

    }

  </style>

</head>

<body>

	<div class="container-fluid">

		<?php 

            include_once 'menu.php'; 

            echo mostraMensagem();

            $idCurriculum = $_POST['idCurriculum'];

            $query = "SELECT * FROM cvs WHERE id_curriculum = '$idCurriculum'";

            $query = mysql_query($query);

            while ($result = mysql_fetch_array($query)) {

              $empregado = $result['empregado'];
              
              $escolaridade = $result['escolaridade'];

              $objetivo = $result['objetivo'];

              $experiencia = $result['experiencia'];

              $salario = $result['salario'];

              $anexoCv = $result['anexoCv'];

              $ultimoEmprego = $result['ultimoEmprego'];

            }

        ?>

        <div class="row">

                <div class="row boxCadastrarCv" id="boxCadastrar">

                    <form action="admin/engine/formularios.php" method="post" enctype="multipart/form-data">

                      <input type="hidden" name="id" value="alterarCv">

                      <input type="hidden" name="idCurriculum" value="<?php echo $idCurriculum; ?>">

                      <input type="hidden" name="anexoCv" value="<?php echo $anexoCv; ?>">

                      <div class="col-xs-12 col-lg-12 text-center">

                        <?php 



                          if ($anexoCv != "") {

                            echo '<label>Arquivo: </label>'.$anexoCv;

                            echo '<br/>';

                            echo '<a href="/'.$anexoCv.'">Fazer download do currículo</a>';

                            echo '<br/>';

                            echo '<br/>';

                            echo 'Para alterar o arquivo do seu currículo, envie outro arquivo';

                            echo '<br/>';

                          ?>

                          <div class="col-md-6 col-md-offset-3 text-center">

                            <input type="file" name="cvAnexo">

                            <br/>

                            <br/>

                          </div>



                          <?php

                            echo '<br/>';

                            echo '<br/>';

                          }else{

                            echo

                            '

                              <div class="col-md-6 col-md-offset-3 text-center">

                                Envie o arquivo do seu currículo

                              </div>

                              <div class="col-md-6 col-md-offset-3 text-center">

                                <input type="file" name="cvAnexo">

                                <br/>

                                <br/>

                              </div>

                            ';

                          }



                        ?>

                      </div>

                      <table>

                        <caption>Informe seus dados principais nos campos abaixo.</caption>

                        <tr>

                          <td id="right-form" ><label>Está empregado atualmente? </label></td>

                          <td id="left-form">

                            <select class="form-control" name="empregado">

                              <?php 

                              if ($empregado == '1'){ $empregado = 'Sim'; $empregadoValue = '1'; }

                              if ($empregado == '0'){ $empregado = 'Não'; $empregadoValue = '0'; }

                              ?>

                              <option value="<?php echo $empregadoValue; ?>"><?php echo $empregado; ?></option>

                              <option>---------</option>

                              <option value="1">Sim</option>

                              <option value="0">Não</option>

                            </select>

                          </td>

                        </tr>
                        <tr>
                          <td id="right-form" ><label>Escolaridade</label></td>
                          <td id="left-form">
                            <select class="form-control" name="escolaridade">
                              <?php 
                                $escolaridadeValue = "";
                                if($escolaridade == 'doutoradoCompleto'){ $escolaridadeValue = 'doutoradoCompleto'; $escolaridade = 'Doutorado Completo'; }
                                if($escolaridade == 'doutoradoIncompleto'){ $escolaridadeValue = 'doutoradoIncompleto'; $escolaridade = 'Doutorado Incompleto'; }
                                if($escolaridade == 'mestradoCompleto'){ $escolaridadeValue = 'mestradoCompleto'; $escolaridade = 'Mestrado Completo'; }
                                if($escolaridade == 'mestradoIncompleto'){ $escolaridadeValue = 'mestradoIncompleto'; $escolaridade = 'Mestrado Incompleto'; }
                                if($escolaridade == 'posGraduacaoCompleta'){ $escolaridadeValue = 'posGraduacaoCompleta'; $escolaridade = 'Pós-graduação Completa'; }
                                if($escolaridade == 'posGraduacaoIncompleta'){ $escolaridadeValue = 'posGraduacaoIncompleta'; $escolaridade = 'Pós-graduação Incompleta'; }
                                if($escolaridade == 'superiorCompleto'){ $escolaridadeValue = 'superiorCompleto'; $escolaridade = 'Superior Completo'; }
                                if($escolaridade == 'superiorIncompleto'){ $escolaridadeValue = 'superiorIncompleto'; $escolaridade = 'Superior Incompleto'; }
                                if($escolaridade == 'ensinoMedioCompleto'){ $escolaridadeValue = 'ensinoMedioCompleto'; $escolaridade = 'Ensino Médio Completo'; }
                                if($escolaridade == 'ensinoMedioIncompleto'){ $escolaridadeValue = 'ensinoMedioIncompleto'; $escolaridade = 'Ensino Médio Incompleto'; }
                                if($escolaridade == 'ensinoFundamentalCompleto'){ $escolaridadeValue = 'ensinoFundamentalCompleto'; $escolaridade = 'Ensino Fundamental Completo'; }
                                if($escolaridade == 'ensinoFundamentalIncompleto'){ $escolaridadeValue = 'ensinoFundamentalIncompleto'; $escolaridade = 'Ensino Fundamental Incompleto'; }
                                if($escolaridade == 'naoAlfabetizado'){ $escolaridadeValue = 'naoAlfabetizado'; $escolaridade = 'Não Alfabetizado'; }
                              ?>
                              <option value="<?php echo $escolaridadeValue; ?>"><?php echo $escolaridade; ?></option>
                              <option>---------</option>
                              <option value="doutoradoCompleto">Doutorado Completo</option>
                              <option value="doutoradoIncompleto">Doutorado Incompleto</option>
                              <option value="mestradoCompleto">Mestrado Completo</option>
                              <option value="mestradoIncompleto">Mestrado Incompleto</option>
                              <option value="posGraduacaoCompleta">Pós-graduação Completa</option>
                              <option value="posGraduacaoIncompleta">Pós-graduação Incompleta</option>
                              <option value="superiorCompleto">Superior Completo</option>
                              <option value="superiorIncompleto">Superior Incompleto</option>
                              <option value="ensinoMedioCompleto">Ensino Médio Completo</option>
                              <option value="ensinoMedioIncompleto">Ensino Médio Incompleto</option>
                              <option value="ensinoFundamentalCompleto">Ensino Fundamental Completo</option>
                              <option value="ensinoFundamentalIncompleto">Ensino Fundamental Incompleto</option>
                              <option value="naoAlfabetizado">Não Alfabetizado</option>
                            </select>
                          </td>
                        </tr>
                        <tr>

                          <td id="right-form" ><label>Objetivo Profissional</label></td>

                          <td id="left-form" ><textarea class="form-control" name="objetivoProfissional" ><?php echo $objetivo ?></textarea></td>

                        </tr>

                        <tr>

                          <td id="right-form" ><label>Experiência Profissional</label></td>

                          <td id="left-form" ><textarea class="form-control" name="experienciaProfissional"><?php echo $experiencia ?></textarea></td>

                        </tr>

                        <tr>

                          <td id="right-form" ><label>Último emprego</label></td>

                          <td id="left-form" ><input type="text" class="form-control" name="ultimoEmprego" value="<?php echo $ultimoEmprego; ?>"></td>

                        </tr>

                        <tr>

                          <td id="right-form" ><label>Salário</label></td>

                          <td id="left-form" ><input type="text" class="form-control valorReal" name="salario" value="<?php echo $salario; ?>"></td>

                        </tr>

                        <tr>

                          <td id="right-form"></td>

                          <td id="left-form">

                            <button class="btn btn-success" type="submit">Salvar</button></td>

                        </tr>

                      </table>

                    </form>         

                </div>

        </div>

    <script type="text/javascript">

        $(document).ready(function() {

            $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        }); 

    </script>

		<?php include_once 'footer.php'; ?>

	</div>

</body>

</html>