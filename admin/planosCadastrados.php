<?php include_once 'headAdmin.php'; ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#tableCandidatos').DataTable();
            $('#tableEmpresas').DataTable();
        });
    </script>

    <div id="page-wrapper">
        <div class="row">
                <?php echo mostraMensagem(); ?>  
                <h2>Planos Cadastrados</h2>
                <hr>
                <div class="col-lg-12">
                    <div class="panel panel-default">                        
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="glyphicon glyphicon-user"></i> 
                                Candidatos
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="tableCandidatos">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>CPF</th>
                                            <th>Plano</th>
                                            <th>Status</th>
                                            <th>Data</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                        $query = "SELECT * FROM usuarios u 
                                                INNER JOIN usuarios_planos up ON u.id = up.idUsuario 
                                                ORDER BY up.data DESC";
                                        $query = mysql_query($query);
                                        while ($result = mysql_fetch_array($query)) {
                                            $nome = $result['nome'];
                                            $cpf = $result['cpf'];
                                            $plano = $result['plano'];
                                            $status = $result['status'];
                                            $data = $result['data'];
                                            $data = FormataData_Brasil($data, "tsp");

                                            if ($status == "Aguardando Pagamento") { $backgroundTr = "#5bc0de"; }
                                            if ($status == "pago") { $backgroundTr = "#5cb85c"; }
                                            if ($status == "Cancelado") { $backgroundTr = "#d9534f"; }

                                            echo
                                            '
                                            <tr style="background: '.$backgroundTr.'">
                                                <td>'.$nome.'</td>
                                                <td>'.$cpf.'</td>
                                                <td>'.$plano.'</td>
                                                <td>'.$status.'</td>
                                                <td>'.$data.'</td>
                                            </tr>
                                            ';
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">                        
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="glyphicon glyphicon-user"></i> 
                                Empresas
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="tableEmpresas">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>CNPJ</th>
                                            <th>Plano</th>
                                            <th>Status</th>
                                            <th>Data</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                        $query = "SELECT * FROM empresas u 
                                                INNER JOIN empresas_planos up ON u.id = up.idEmpresa
                                                ORDER BY up.data DESC";
                                        $query = mysql_query($query);
                                        while ($result = mysql_fetch_array($query)) {
                                            $nome = $result['nome'];
                                            $cnpj = $result['cnpj'];
                                            $plano = $result['plano'];
                                            $status = $result['status'];
                                            $data = $result['data'];
                                            $data = FormataData_Brasil($data, "tsp");

                                            if ($status == "Aguardando Pagamento") { $backgroundTr = "#5bc0de"; }
                                            if ($status == "pago") { $backgroundTr = "#5cb85c"; }
                                            if ($status == "Cancelado") { $backgroundTr = "#d9534f"; }

                                            echo
                                            '
                                            <tr style="background: '.$backgroundTr.'">
                                                <td>'.$nome.'</td>
                                                <td>'.$cnpj.'</td>
                                                <td>'.$plano.'</td>
                                                <td>'.$status.'</td>
                                                <td>'.$data.'</td>
                                            </tr>
                                            ';
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
    </div>

<?php include_once 'footerAdmin.php'; ?>