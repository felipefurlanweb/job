<?php include_once 'headAdmin.php'; ?>
<?php
    $idEmpresa = $_SESSION['job']['empresa']['id'];
?>
<script type="text/javascript">
$(document).ready(function() {
    $('table').DataTable({
        "language": 
        {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
        
    });
});
</script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Candidatos</h1>
                </div>
                <div class="col-xs-12">
                    <?php echo mostraMensagem(); ?>
                </div>
                <div class="col-xs-12">
                                    <?php 
                                            $query = "SELECT * FROM candidatos cand
                                            INNER JOIN (SELECT id, nome as nomeUsuario, email as emailUsuario FROM usuarios) us ON cand.id_usuario = us.id
                                            INNER JOIN (SELECT id, idEmpresa, nome as nomeEmprego FROM vagas) v ON cand.id_vaga = v.id
                                            INNER JOIN (SELECT id, nome_fantasia as nomeEmpresa FROM empresas) e ON v.idEmpresa = e.id
                                            WHERE e.id = $idEmpresa
                                            ";
                                            $query = mysql_query($query);
                                     ?>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>Vaga</td>
                                                    <td>Candidato</td>
                                                    <!--                                                     
                                                    <td>Anotação</td>
                                                    <td>Aceitar</td>
                                                    <td>Recusar</td>
                                                    <td>Revisar</td> 
                                                    -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    while ($result = mysql_fetch_array($query)) {
                                                        $nomeEmprego = $result['nomeEmprego'];
                                                        $nomeUsuario = $result['nomeUsuario'];
                                                        $emailUsuario = $result['emailUsuario'];
                                                        $idUsuario = $result['id_usuario'];
                                                        $id_vaga = $result['id_vaga'];
                                                        $revisar = $result['revisar'];
                                                        $status = $result['status'];
                                                        // $queryMsg = "SELECT * FROM candidatos cand
                                                        //     INNER JOIN (SELECT id_usuario, nome as nomeUsuario, email as emailUsuario FROM usuarios) us ON cand.id_usuario = us.id_usuario
                                                        //     INNER JOIN (SELECT id_emprego, id_empresa, nome as nomeEmprego FROM empregos) empreg ON cand.id_emprego = empreg.id_emprego
                                                        //     INNER JOIN mensagem_emprego_candidato msg ON msg.id_candidato = cand.id_usuario
                                                        //     WHERE empreg.id_empresa = '$idEmpresa'
                                                        //     AND msg.id_candidato = '$idUsuario'
                                                        //     ORDER BY msg.data DESC LIMIT 1";
                                                        // $queryMsg = mysql_query($queryMsg);
                                                        // $numRows = mysql_num_rows($queryMsg);
                                                        // if ($numRows > 0) {
                                                        //     while ($resultMsg = mysql_fetch_array($queryMsg)) {
                                                        //         $mensagem = $resultMsg['mensagem'];
                                                        //     }
                                                        // }else{
                                                        //     $mensagem = "Sem mensagem";
                                                        // }


                                                        // if ($revisar != 0) {
                                                        //     $corTrCandidato = 'background:rgba(255, 249, 0, 0.38)';
                                                        // }else{
                                                        //     if ($status == 'rejeitado') { 
                                                        //         $corTrCandidato = 'background:#d9534f;color: white;';
                                                        //     }
                                                        //     if ($status == 'aceitado') { 
                                                        //         $corTrCandidato = 'background:#47a447;color: white;';
                                                        //     }
                                                        //     if($status == NULL){
                                                        //         $corTrCandidato = ""; 
                                                        //     }
                                                        // }

                                                        echo'
                                                            <tr>
                                                                <td>'.$nomeEmprego.'</td>
                                                                <td><a href="candidato.php?idEmprego='.$id_vaga.'&idCandidato='.$idUsuario.'&nomeEmprego='.$nomeEmprego.'">'.$nomeUsuario.'</td>
                                                            </tr>
                                                        ';
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                </div>
            </div>
        </div>
<!-- <td>'.$mensagem.'</td>
                                                                <td>
                                                                    <form action="engine/formularios.php" method="post">
                                                                        <input type="hidden" name="id" value="opcaoCandidato">
                                                                        <input type="hidden" name="opcao" value="aceitar">
                                                                        <input type="hidden" name="id_emprego" value="'.$id_emprego.'">
                                                                        <input type="hidden" name="idUsuario" value="'.$idUsuario.'">
                                                                        <input type="hidden" name="emailUsuario" value="'.$emailUsuario.'">

                                                                        <input type="hidden" name="nomeUsuario" value="'.$nomeUsuario.'">
                                                                        <input type="hidden" name="nomeEmprego" value="'.$nomeEmprego.'">
                                                                        
                                                                        <button type="submit" class="btn btn-success glyphicon glyphicon-ok"></button>
                                                                    </form>
                                                                </td>
                                                                <td>
                                                                    <form action="engine/formularios.php" method="post">
                                                                        <input type="hidden" name="id" value="opcaoCandidato">
                                                                        <input type="hidden" name="opcao" value="recusar">
                                                                        <input type="hidden" name="id_emprego" value="'.$id_emprego.'">
                                                                        <input type="hidden" name="idUsuario" value="'.$idUsuario.'">
                                                                        <input type="hidden" name="emailUsuario" value="'.$emailUsuario.'">
                                                                        <button type="submit" class="btn btn-danger glyphicon glyphicon-remove"></button>
                                                                    </form>
                                                                </td>
                                                                <td>
                                                                    <form action="engine/formularios.php" method="post">
                                                                        <input type="hidden" name="id" value="opcaoCandidato">
                                                                        <input type="hidden" name="id_emprego" value="'.$id_emprego.'">
                                                                        <input type="hidden" name="idUsuario" value="'.$idUsuario.'">
                                                                        <input type="hidden" name="emailUsuario" value="'.$emailUsuario.'">
                                                                        <input type="hidden" name="opcao" value="revisar">
                                                                        <button type="submit" class="btn btn-warning glyphicon glyphicon-exclamation-sign"></button>
                                                                    </form>
                                                                </td> -->