<html>
<head>
    <?php 

        include_once 'headAdmin.php';
        include_once 'verificaLogin.php';

    ?>
</head>
<body>
    <div id="wrapper">
        <?php include_once 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo mostraMensagem(); ?>
                    <h1 class="page-header">Cargos de Interesses</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="cargosInteressesAdd.php"><button type="button" class="btn btn-primary">Adicionar</button></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Deletar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = mysql_query("SELECT * FROM cargos_interesses ORDER BY nome ASC");
                                        while ($result = mysql_fetch_array($sql)) {
                                            $nome = $result['nome'];
                                            $id_cargo_interesse = $result['id_cargo_interesse'];
                                            echo
                                            '
                                                <tr>
                                                    <td>'.$nome.'</td>
                                                    <td class="center">
                                                        <form action="engine/formularios.php" method="post">
                                                            <input type="hidden" name="id" value="cargoInteresseRemove">
                                                            <input type="hidden" name="id_cargo_interesse" value="'.$id_cargo_interesse.'">
                                                            <button type="submit" class="btn btn-danger btn-circle">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </form>
                                                    </td>                                                    
                                                </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    
</body>
</html>
