<?php include_once 'headAdmin.php'; ?>

<?php

    $status = "";
    if (!$isAdmin) {

        if ($isEmpresa) {
            $query = "SELECT * FROM empresas_planos WHERE idEmpresa = $idAuth ORDER BY data DESC LIMIT 1";
            $query = mysql_query($query);
            $res = mysql_fetch_assoc($query);
            $status = $res['status'];
        }else if($isUsuario){
            $query = "SELECT * FROM usuarios_planos WHERE idUsuario = $idAuth ORDER BY data DESC LIMIT 1";
            $query = mysql_query($query);
            $res = mysql_fetch_assoc($query);
            $status = $res['status'];
        }
    }

?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo mostraMensagem(); ?>
                    <h1 class="page-header">Vagas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php 
                                if (!$isAdmin) {
                                    echo
                                    '
                                        <a href="vagasAdd.php" class="btn btn-primary">
                                            Adicionar
                                        </a>
                                    ';
                                }
                            ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nome Candidato</th>
                                            <?php 
                                            if (!$isAdmin){
                                                if ($status == "pago") {
                                                    ?>
                                                    <th>Ativar</th>
                                                    <?php 
                                                }
                                            }
                                            ?>
                                            <?php 
                                            if (!$isAdmin) {
                                                ?>
                                                <th>Editar</th>
                                                <th>Deletar</th>
                                                <?php 
                                            }else{
                                                echo
                                                '
                                                <td>Empresa</td>
                                                ';
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = "";
                                        $query = "";
                                        if ($isAdmin) {
                                            $query = "SELECT * FROM vagas v
                                            INNER JOIN empresas e ON v.idEmpresa = e.id
                                            ORDER BY v.data DESC";
                                        }else{
                                            if ($isEmpresa) {
                                                //sql empresas
                                                $query = "SELECT * FROM vagas v
                                                INNER JOIN empresas e ON v.idEmpresa = e.id
                                                WHERE e.id = $idAuth
                                                ORDER BY v.data DESC";
                                            }
                                        }
                                     $query = mysql_query($query);
                                     while ($result = mysql_fetch_array($query)) {

                                        $nome = $result["nome"];
                                        $id = $result["id"];
                                        $razaosoc = $result["razaosoc"];

                                        echo
                                        '
                                        <tr>
                                        <td>'.$nome.'</td>';
                                        if ($isAdmin) {
                                            echo '<td>'.$razaosoc.'</td>';
                                        }
                                        if (!$isAdmin) {
                                            if ($status == "pago") {
                                                if ($ativo) {
                                                    echo '
                                                    <td>
                                                    <form action="engine/formularios.php" method="post">
                                                    <input type="text" name="id" value="empregoActive" hidden>
                                                    <input type="text" name="idEmprego" value="'.$id.'" hidden>
                                                    <input type="text" name="ativo" value="'.$ativo.'" hidden>
                                                    <input type="submit" value="" style="background: url(img/ativo.png) 50% 50% no-repeat;height:15px;width:15px;background-size:cover;border:none;" title="Desativando este emprego, o emprego ficará indisponível no site.">
                                                    </form>
                                                    </td>';
                                                }else{
                                                    echo '
                                                    <td>
                                                    <form action="engine/formularios.php" method="post">
                                                    <input type="text" name="id" value="empregoActive" hidden>
                                                    <input type="text" name="idEmprego" value="'.$id.'" hidden>
                                                    <input type="text" name="ativo" value="'.$ativo.'" hidden>
                                                    <input type="submit" value="" style="background: url(img/desativo.png) 50% 50% no-repeat;height:15px;width:15px;background-size:cover;border:none;" title="Ativando este emprego, o emprego ficará disponível no site.">
                                                    </form>
                                                    </td>';           
                                                }
                                            }
                                        }
                                        if (!$isAdmin) {
                                            echo 
                                            '
                                            <td class="center">
                                                <form action="empregosEdit.php" method="post">
                                                    <input type="text" name="idEmprego" value="'.$id.'" hidden>
                                                    <button type="submit" class="btn btn-info btn-circle">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </button>
                                                </form>
                                            </td>
                                            <td class="center">
                                                <form action="engine/formularios.php" method="post">
                                                    <input type="text" name="id" value="empregoRemove" hidden>
                                                    <input type="text" name="idEmprego" value="'.$id.'" hidden>
                                                    <button type="submit" class="btn btn-danger btn-circle">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </form>
                                            </td>                                                    
                                            ';
                                        }
                                        echo
                                        '
                                        </tr>
                                        ';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

<?php include_once 'footerAdmin.php'; ?>
