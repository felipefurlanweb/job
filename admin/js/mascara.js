    function mascara(str) {
        // Caso passe de 14 caracteres serÃ¡ formatado como CNPJ 
        if (str.value.length > 14)                       
            str.value = cnpj(str.value);
        // Caso contrÃ¡rio como CPF
        else                           
            str.value = cpf(str.value);
    }
 
    // Funcao de formatacao CPF
    function cpf(valor) {
        // Remove qualquer caracter digitado que nÃ£o seja numero
        valor = valor.replace(/\D/g, "");                   
 
        // Adiciona um ponto entre o terceiro e o quarto digito
        valor = valor.replace(/(\d{3})(\d)/, "$1.$2");
 
        // Adiciona um ponto entre o terceiro e o quarto dÃ­gitos 
        // desta vez para o segundo bloco      
        valor = valor.replace(/(\d{3})(\d)/, "$1.$2");
 
        // Adiciona um hifen entre o terceiro e o quarto dÃ­gitos
        valor = valor.replace(/(\d{3})(\d)$/, "$1-$2");     
        return valor;
    }
 
    // Funcao de formatacao CNPJ
    function cnpj(valor) {
        // Remove qualquer caracter digitado que nÃ£o seja numero
        valor = valor.replace(/\D/g, "");                           
 
        // Adiciona um ponto entre o segundo e o terceiro dÃ­gitos
        valor = valor.replace(/^(\d{2})(\d)/, "$1.$2");
 
        // Adiciona um ponto entre o quinto e o sexto dÃ­gitos
        valor = valor.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
 
        // Adiciona uma barra entre o oitavaloro e o nono dÃ­gitos
        valor = valor.replace(/\.(\d{3})(\d)/, ".$1/$2");
 
        // Adiciona um hÃ­fen depois do bloco de quatro dÃ­gitos
        valor = valor.replace(/(\d{4})(\d)/, "$1-$2");              
        return valor;
    }