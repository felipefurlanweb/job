<?php   
if (!isset($_SESSION)) {
session_start();
}
include_once 'engine/funcoes.php';
include_once '../banco.php';

$idAuth  = 0;
$isAdmin = 0;
$isEmpresa = 0;
$isUsuario = 0;
if (@isset($_SESSION['job']['empresa'])) {
    $idAuth = $_SESSION['job']['empresa']['id'];
    $isEmpresa = 1;
}else if (@isset($_SESSION['job']['usuario'])) {
    $idAuth = $_SESSION['job']['usuario']['id'];
    $isUsuario = 1;
}else{
    $isAdmin = 1;
}

?>

<html>
    <title>Painel Administrador</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="sbAdmin/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="sbAdmin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="sbAdmin/vendor/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jasny.css">
    <link rel="stylesheet" type="text/css" href="summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script src="sbAdmin/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="js/cidadesEstados.js"></script>
    <script type="text/javascript" src="../js/jquery.maskMoney.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="js/jasny.js"></script>
    <script src="summernote/dist/summernote.js"></script>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Job</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <?php 
                                if ($isAdmin) {
                                    $nomeLogado = "Admin";
                                }else{
                                    $queryLogado = "SELECT * FROM empresas WHERE id = ".$_SESSION['job']['empresa']['id'];
                                    $queryLogado = mysql_query($queryLogado);
                                    $resLogado = mysql_fetch_assoc($queryLogado);
                                    $nomeLogado = $resLogado["nome_fantasia"];
                                }
                            ?>
                            <?php echo $nomeLogado; ?> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="sair.php"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <?php 
                            if (@$_SESSION['job']['empresa']['id'] != "") {
                                echo '
                                <li><a href="index.php">Home</a></li>
                                <li><a href="candidatos.php">Candidatos</a></li>
                                <li><a href="vagas.php">Vagas</a></li>
                                <li><a href="empresasEdit.php?idEmpresa='.$_SESSION['job']['empresa']['id'].'">Minha Empresa</a></li>
                                ';
                            }
                            if ($isAdmin) {
                                echo
                                '
                                <li><a href="slideshow.php">Slideshow</a></li>
                                <li><a href="empresas.php">Empresas</a></li>
                                <li><a href="vagas.php">Vagas</a></li>
                                <li><a href="cursos.php">Cursos</a></li>
                                <li><a href="somos.php">Quem Somos</a></li>
                                <li><a href="suporte.php">Suporte</a></li>
                                <li><a href="sair.php">Sair</a></li>
                                ';
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
            </div>
        </nav>


