<html>
<head>
    <?php 

        include_once 'headAdmin.php';
        include_once 'verificaLogin.php';

    ?>
    <script type="text/javascript">
    window.onload = function() {
      new dgCidadesEstados({
        estado: document.getElementById('estado'),
        cidade: document.getElementById('cidade')
      });
    }
    </script>
</head>
<body>
    <div id="wrapper">
        <?php include 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Adicionar Empresa</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                    <input name="id" value="empresaAdd" hidden>
                    <div class="col-lg-6">
                        <div class="row">
                            <label>Nome</label>
                            <input type="text" name="nome" class="form-control">
                        </div>
                        <br/>
                        <div class="row">
                            <input type="submit" class="btn btn-success" value="Salvar">
                        </div>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
        }); 
    </script>
    
</body>
</html>
