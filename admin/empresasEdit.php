<?php 
  include_once 'headAdmin.php';
  $idEmpresa = $_GET['idEmpresa'];
  $query = "SELECT * FROM empresas WHERE id = '$idEmpresa'";
  $query = mysql_query($query);
  while ($result = mysql_fetch_array($query)) {
    $imagem = $result['imagem'];
    $razaosoc = $result['razaosoc'];
    $nome_fantasia = $result['nome_fantasia'];
    $cpf_cnpj = $result['cnpj'];
    $email = $result['email'];
    $telefone = $result['telefone'];
    $endereco = $result['endereco'];
    $estado = $result['estado'];
    $cidade = $result['cidade'];
    $sobre = $result['sobre'];
    $ramoAtividade = $result['ramoAtividade'];
  }
  ?>
  <script type="text/javascript">
      $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
  window.onload = function() {
    new dgCidadesEstados({
      cidade: document.getElementById('cidade'),
      estado: document.getElementById('estado'),
      estadoVal: '<?php echo $estado; ?>',
      cidadeVal: '<?php echo $cidade; ?>'
    });
  }
  </script>

    <div id="page-wrapper">
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <h1 class="page-header">Empresa</h1>
          <?php echo mostraMensagem(); ?>
        </div>
          <form action="engine/formularios.php" method="post" id="formCadastroEmpresa" enctype="multipart/form-data">
            <input type="hidden" name="id" value="empresaEdit">
            <input type="hidden" name="idEmpresa" value="<?php echo $idEmpresa; ?>">
              <div>
                  <div class="col-xs-12 form-group">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $imagem ?>">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        
                      </div>
                      <div>
                        <span class="btn btn-default btn-file">
                          <span class="fileinput-new">Selecione uma imagem</span>
                          <span class="fileinput-exists">Alterar</span>
                          <input type="file" name="imagem"></span>
                          <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6 form-group">
                    <label>Razão Social: </label>
                    <input value="<?php echo $razaosoc; ?>" class="form-control" type="text" name="razaosoc" />
                  </div>
                  <div class="col-xs-6 form-group">
                    <label>Nome Fantasia: </label>
                    <input value="<?php echo $nome_fantasia; ?>" class="form-control" type="text" name="nomefantasia" />
                  </div>
                  <div class="col-xs-4 form-group">
                    <label>E-mail: </label>
                    <input value="<?php echo $email; ?>" class="form-control" type="email" name="email" />
                  </div>
                  <div class="col-xs-4 form-group">
                    <label>Telefone: </label>
                    <input value="<?php echo $telefone; ?>" class="form-control" type="text" name="telefone" 
                    placeholder="(xx) xxxx-xxxx" />
                  </div>
                  <div class="col-xs-4 form-group">
                    <label>Endereço: </label>
                    <input value="<?php echo $endereco; ?>" class="form-control" type="text" name="endereco"/>
                  </div>
                  <div class="col-xs-6 form-group">
                    <label>Estado: </label>
                    <select class="form-control" id="estado" name="estado"></select>
                  </div>
                  <div class="col-xs-6 form-group">
                    <label>Cidade: </label>
                    <select class="form-control" id="cidade" name="cidade">
                      <option>Selecione um estado ...</option>
                    </select>
                  </div>
                  <div class="col-xs-12 form-group">
                    <label>Ramo de Atividade: </label>
                    <input value="<?php echo $ramoAtividade; ?>" class="form-control" 
                    type="text" name="ramoAtividade"/>
                  </div>
                  <div class="col-xs-12 form-group">
                    <label>Conte um pouco sobre sua empresa: </label>
                    <textarea class="form-control" name="sobre"><?php echo $sobre; ?></textarea>
                  </div>
                  <div class="col-xs-12 form-group">
                    <button class="btn btn-success" type="submit">Salvar</button>
                  </div>

              </div>
              
            </form> 
      </div>

    <script type="text/javascript">
    $(document).ready(function() {
      $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    }); 
    </script>
