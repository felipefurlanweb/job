<?php include_once 'headAdmin.php'; ?>

<script type="text/javascript">
    window.onload = function() {
      new dgCidadesEstados({
        estado: document.getElementById('estado'),
        cidade: document.getElementById('cidade')
      });
    }
    $(document).ready(function() {
        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
</script>

        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">Adicionar Curso</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <div class="col-xs-12 col-md-12">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="cursoAdd" hidden>

                        <div class="row">

                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Estado</label>
                                        <select id="estado" name="estado" class="form-control" required></select>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Cidade</label>
                                        <select id="cidade" name="cidade" class="form-control" required>
                                            <option>Selecione uma cidade ...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <label>Presença</label>
                                        <select name="presenca" class="form-control">
                                            <option value="">Selecione uma opção ...</option>
                                            <option value="Presencial">Presencial</option>
                                            <option value="A Distância">A Distância</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-2">
                                        <label>Horas</label>
                                        <input type="number" name="horas" class="form-control" maxlength="2">
                                    </div>
                                    <div class="col-xs-12 col-md-3">
                                        <label>Valor</label>
                                        <input type="text" name="valor" class="form-control valorReal" value="0,00">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Resumo</label>
                                        <textarea name="resumo" class="form-control" required></textarea>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Outras Informações</label>
                                        <textarea name="outrasinfo" class="form-control"></textarea>
                                    </div>
                                    <div class="col-xs-12 col-md-12 marginTop">
                                        <label>Programa Completo</label>
                                        <textarea name="programacompleto" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group marginTop">
                                <input type="submit" class="btn btn-success" value="Salvar">
                            </div>
                        </div>

                </form>      

                </div>

                <!-- /.col-lg-12 -->

            </div>

        </div>

