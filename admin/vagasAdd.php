<?php include_once 'headAdmin.php'; ?>

<script type="text/javascript">
    window.onload = function() {
      new dgCidadesEstados({
        estado: document.getElementById('estado'),
        cidade: document.getElementById('cidade')
      });
    }
    $(document).ready(function() {
        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
</script>

        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">Adicionar Vaga</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <div class="col-xs-12 col-md-12">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="empregoAdd" hidden>

                    <?php

                    $idEmpresa = @$_SESSION['job']['empresa']['id'];

                    ?>

                    <input name="idEmpresa" value="<?php echo $idEmpresa; ?>" hidden>



                        <div class="row">

                            <div class="form-group">
                                <label>Vaga</label>
                                <select name="nome" class="form-control" required>
                                    <option value="">Selecione uma vaga ...</option>
                                    <?php 
                                        $query = "SELECT * FROM nome_empregos ORDER BY emprego ASC";
                                        $query = mysql_query($query);
                                        while ($result = mysql_fetch_array($query)) {
                                            $emprego = $result['emprego'];
                                            $emprego = utf8_encode($emprego);
                                            echo '<option value="'.$emprego.'">'.$emprego.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Requisitos</label>
                                        <textarea name="requisitos" class="form-control" required></textarea>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Principais Atividades</label>
                                        <textarea name="principaisAtividades" class="form-control" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Estado</label>
                                        <select id="estado" name="estado" class="form-control" required></select>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Cidade</label>
                                        <select id="cidade" name="cidade" class="form-control" required>
                                            <option>Selecione uma cidade ...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <label>Tipo de vaga</label>
                                        <select name="tipo_vaga" class="form-control" required>
                                            <option value="">Selecione uma opção ...</option>
                                            <option value="efetivo">Efetivo</option>
                                            <option value="temporario">Temporário</option>
                                            <option value="prestadorServico">Prestador de serviço</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <label>Horario</label>
                                        <select name="horario" class="form-control" required>
                                            <option value="">Selecione uma opção ...</option>
                                            <option value="comercial">Comercial</option>
                                            <option value="2turno">2ª turno</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <label>Salário</label>
                                        <input type="text" class="form-control valorReal" name="salario" required>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">

                            </div>
                            <div class="form-group">
                                <label>Benefícios</label>
                                <textarea name="beneficios" class="form-control"></textarea>
                            </div>
                        </div>

                        <br/>

                        <div class="row text-right">

                            <input type="submit" class="btn btn-success" value="Salvar">

                        </div>

                </form>      

                </div>

                <!-- /.col-lg-12 -->

            </div>

        </div>

