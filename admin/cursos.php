<?php include_once 'headAdmin.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo mostraMensagem(); ?>
                    <h1 class="page-header">Cursos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="cursoAdd.php" class="btn btn-primary">
                                Adicionar
                            </a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Editar</th>
                                            <th>Deletar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $query = "SELECT * FROM cursos ORDER BY nome ASC";
                                        $query = mysql_query($query);
                                        while ($result = mysql_fetch_array($query)) {
                                            $nome = $result["nome"];
                                            $id = $result["id"];
                                            echo
                                            '
                                            <tr>
                                                <td>'.$nome.'</td>
                                                <td class="center">
                                                    <form action="cursoEdit.php" method="post">
                                                        <input type="text" name="idCurso" value="'.$id.'" hidden>
                                                        <button type="submit" class="btn btn-info btn-circle">
                                                            <span class="glyphicon glyphicon-pencil"></span>
                                                        </button>
                                                    </form>
                                                </td>
                                                <td class="center">
                                                    <form action="engine/formularios.php" method="post">
                                                        <input type="text" name="id" value="cursoRemove" hidden>
                                                        <input type="text" name="idCurso" value="'.$id.'" hidden>
                                                        <button type="submit" class="btn btn-danger btn-circle">
                                                            <span class="glyphicon glyphicon-remove"></span>
                                                        </button>
                                                    </form>
                                                </td>                                                    
                                            </tr>
                                            ';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

<?php include_once 'footerAdmin.php'; ?>
