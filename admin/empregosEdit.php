<html>

<head>

    <?php 



        include_once 'headAdmin.php';

        include_once 'verificaLogin.php';

        $idEmprego = $_POST['idEmprego'];

        $query = "SELECT * FROM empregos WHERE id_emprego = '$idEmprego'";

        $query = mysql_query($query);

        while ($result = mysql_fetch_array($query)) {

            $nome = $result['nome'];

            $empresa = $result['id_empresa'];
            $beneficios = $result['beneficios'];

            $principaisAtividades = $result['principais_atividades'];

            $estado = $result['estado'];

            $cidade = $result['cidade'];

            $requisitos = $result['requisitos'];

            $salario = $result['salario'];
            $tipo_vaga = $result['tipo_vaga'];
            $horario = $result['horario'];

            if($tipo_vaga == "efetivo") { $tipo_vagaString = "Efetivo" ; } 
            if($tipo_vaga == "temporario") { $tipo_vagaString = "Temporário" ; } 
            if($tipo_vaga == "prestadorServico") { $tipo_vagaString = "Prestador de serviço" ; }

            if($horario == "administrativo") { $horarioString = "Administrativo"; } 
            if($horario == "2turno") { $horarioString = "2ª turno"; } 
            if($horario == "comercial") { $horarioString = "Comercial"; } 


        }

    ?>

    <script type="text/javascript">

    window.onload = function() {

        new dgCidadesEstados({

          cidade: document.getElementById('cidade'),

          estado: document.getElementById('estado'),

          estadoVal: '<?php echo $estado; ?>',

          cidadeVal: '<?php echo $cidade; ?>'

        });

    }

    </script>



</head>

<body>

    <div id="wrapper">

        <?php include 'menu-lateral.php'; ?>

        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">Editar Vaga</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

            <div class="col-lg-6">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="empregoEdit" hidden>

                    <input name="idEmprego" value="<?php echo $idEmprego ?>" hidden>

                    <?php



                    $empresa = $_SESSION['job']['empresa']['id_empresa'];

                    ?>

                    <input type="hidden" name="empresa" value="<?php echo $empresa; ?>">



                        <div class="row">

                            <label>Vaga</label>
                            <select name="nome" class="form-control" required>
                                <option value="<?php echo $nome; ?>"><?php echo $nome; ?></option>
                                <?php 
                                    $query = "SELECT * FROM nome_empregos WHERE emprego <> '$nome' ORDER BY emprego ASC";
                                    $query = mysql_query($query);
                                    while ($result = mysql_fetch_array($query)) {
                                        $emprego = $result['job'];
                                        $emprego = utf8_encode($emprego);
                                        echo '<option value="'.$emprego.'">'.$emprego.'</option>';
                                    }
                                ?>
                            </select>
                            <label>Requisitos</label>

                            <textarea name="requisitos" class="form-control"><?php echo $requisitos; ?></textarea>

                            <label>Principais Atividades</label>

                            <textarea name="principaisAtividades" class="form-control"><?php echo $principaisAtividades; ?></textarea>

                            <label>Estado</label>

                            <select id="estado" name="estado" class="form-control"></select>

                            <label>Cidade</label>

                            <select id="cidade" name="cidade" class="form-control"></select>

                            <label>Tipo de vaga</label>
                            <select name="tipo_vaga" class="form-control" required>
                                <option value="<?php echo $tipo_vaga ?>"><?php echo $tipo_vagaString ?></option>
                                <option>-----------------------</option>
                                <option value="efetivo">Efetivo</option>
                                <option value="temporario">Temporário</option>
                                <option value="prestadorServico">Prestador de serviço</option>
                            </select>
                            <label>Horario</label>
                            <select name="horario" class="form-control" required>
                                <option value="<?php echo $horario ?>"><?php echo $horarioString ?></option>
                                <option>-----------------------</option>
                                <option value="administrativo">Administrativo</option>
                                <option value="2turno">2ª turno</option>
                            </select>
                            <label>Benefícios</label>

                            <textarea name="beneficios" class="form-control"><?php echo $beneficios; ?></textarea>
                            <label>Salário</label>
                            <input type="text" class="form-control valorReal" name="salario" value="<?php echo $salario; ?>">

                        </div>

                        <br/>

                        <div class="row text-right">

                            <input type="submit" class="btn btn-success" value="Salvar">

                        </div>



                </form> 

                </div>

                <!-- /.col-lg-12 -->

            </div>

        </div>

        <!-- /#page-wrapper -->

    </div>

    <script type="text/javascript">

        $(document).ready(function() {

            $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        }); 

    </script>

    

</body>

</html>

