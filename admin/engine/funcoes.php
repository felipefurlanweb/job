<?php 



function toISO8859($texto){

	return Encoding::toISO8859($texto);

}

function FormataData_Brasil($timestamp, $formato="dl")
{
	// Recebe uma data e hora no formato MySQl (aaaa-mm-dd hh:mm:ss) e retorna o que foi solicitado no $formato:
	// $formato:
	// "dc" - retorna dd/mm/aa
	// "dl" - retorna dd/mm/aaaa
	// "hc" - retorna hh:mm
	// "hl" - retorna hh:mm:ss
	// "ts" - retorna dd/mm/aaaa hh:mm:ss
	
	$timestamp = explode(" ", $timestamp);
	$data = $timestamp[0];
	$hora = $timestamp[1];

	$data_separada = explode("-", $data);
	$hora_separada = explode(":", $hora);
	
	switch($formato)
	{
		case "dc":
			return $data_separada[2]."/".$data_separada[1]."/".substr($data_separada[0], -2, 2);
			break;
			
		case "dl":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0];
			break;

		case "hc":
			return $hora_separada[0].":".$hora_separada[1];
			break;
		
		case "hl":
			return $hora_separada[0].":".$hora_separada[1].":".$hora_separada[2];
			break;

		case "ts":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0]." ".$hora_separada[0].$hora_separada[1].$hora_separada[2];
			break;
		
		case "tsp":
			return $data_separada[2]."/".$data_separada[1]."/".$data_separada[0]." às ".$hora_separada[0].":".$hora_separada[1];
			break;			
					
		default:
			return $timestamp;			
	}
}

function VoltaMensagem($msg, $tipo){

	setaMensagem($msg, $tipo);

	voltaPaginaAnterior();

}



function setaMensagem($msg, $tipo){

	if (!isset($_SESSION['mensagens'])) {

		$_SESSION['mensagens'] = array();

	}

	$_SESSION['mensagens'][$msg] = $tipo;

}



function Mensagens(){

	$msgs = array();

	if (isset($_SESSION['mensagens'])) {

		$msgs = $_SESSION['mensagens'];

	}

	limpaMensagens();

	return $msgs;

}



function limpaMensagens(){

	if (@isset($_SESSION['mensagens'])) {

		unset($_SESSION['mensagens']);

	}

}





function mostraMensagem($style=""){

	$mensagens = "";

	if ($style != "") {

		$style = 'style="'.$style.'"';

	}

	$mens = array();

	$msgs = Mensagens();

	if (!empty($msgs)) {

		foreach ($msgs as $_mensagem => $tipo) {

			$mens[] = $_mensagem;

		}

		$mensagens = '<div class="'.$tipo.'" '.$style.'>'.implode("<br>\n", $mens).'</div>';

	}

	return $mensagens;

}



function goToPage($pagina){

	goToUrl($pagina);

}



function goToUrl($url){

	header("Location: ".$url);

	exit(0);

}



function voltaPaginaAnterior(){

	header("Location: ".$_SERVER['HTTP_REFERER']);

	exit(0);

}



function ValidaCPFCNPJ($cpfcnpj){

  $cpfcnpj = LimpaMascara($cpfcnpj);

	

  if(!is_numeric($cpfcnpj))

    return FALSE;

    

  if (strlen($cpfcnpj) == 14)

    return ValidaCNPJ($cpfcnpj);

  if (strlen($cpfcnpj) == 11)

    return ValidaCPF($cpfcnpj);

  else

    return FALSE;

}



function LimpaMascara($numero) {	

	$caracteres = array(".", ",", " ", "/", "-", "(", ")", "=", "#", "*", "\"", "'", "?", "[", "]", "{", "}", "|");	

	$numero = str_replace($caracteres, "", $numero);	

	return $numero;

}



function ValidaCNPJ($cnpj)

{

	// Script de Júlio César Martini (baphp@imasters.com.br)

	// (com alterações)

	

	$s = "";

	for ($x=1; $x<=strlen($cnpj); $x=$x+1)

	{

		$ch=substr($cnpj,$x-1,1);

		if (ord($ch)>=48 && ord($ch)<=57)    

			$s=$s.$ch;    

	}



	$cnpj=$s;

	if (strlen($cnpj)!=14)

		return FALSE;

	else

	if ($cnpj=="00000000000000")	

		return FALSE;		

	

	$Numero[1]=intval(substr($cnpj,1-1,1));

	$Numero[2]=intval(substr($cnpj,2-1,1));

	$Numero[3]=intval(substr($cnpj,3-1,1));

	$Numero[4]=intval(substr($cnpj,4-1,1));

	$Numero[5]=intval(substr($cnpj,5-1,1));

	$Numero[6]=intval(substr($cnpj,6-1,1));

	$Numero[7]=intval(substr($cnpj,7-1,1));

	$Numero[8]=intval(substr($cnpj,8-1,1));

	$Numero[9]=intval(substr($cnpj,9-1,1));

	$Numero[10]=intval(substr($cnpj,10-1,1));

	$Numero[11]=intval(substr($cnpj,11-1,1));

	$Numero[12]=intval(substr($cnpj,12-1,1));

	$Numero[13]=intval(substr($cnpj,13-1,1));

	$Numero[14]=intval(substr($cnpj,14-1,1));

	

	$soma=$Numero[1]*5+$Numero[2]*4+$Numero[3]*3+$Numero[4]*2+$Numero[5]*9+$Numero[6]*8+$Numero[7]*7+

	$Numero[8]*6+$Numero[9]*5+$Numero[10]*4+$Numero[11]*3+$Numero[12]*2;



	$soma=$soma-(11*(intval($soma/11)));

	

	if ($soma==0 || $soma==1)	

		$resultado1=0;		

	else

		$resultado1=11-$soma;

   

	if ($resultado1==$Numero[13])

	{

		$soma=$Numero[1]*6+$Numero[2]*5+$Numero[3]*4+$Numero[4]*3+$Numero[5]*2+$Numero[6]*9+

		$Numero[7]*8+$Numero[8]*7+$Numero[9]*6+$Numero[10]*5+$Numero[11]*4+$Numero[12]*3+$Numero[13]*2;

		$soma=$soma-(11*(intval($soma/11)));

		

		if ($soma==0 || $soma==1)

			$resultado2=0;

		else

			$resultado2=11-$soma;

		

		if ($resultado2==$Numero[14])

			return TRUE;   

   		else

			return FALSE;  

	}

	else

		return FALSE;

}



function ValidaCPF($cpf)

{ 	

	// Script de Júlio César Martini (baphp@imasters.com.br)

	// http://www.imasters.com.br/artigo/1403 (acessado em 15/05/2007)

	// (com alterações)

	

	// verifica se o valor passado é numérico	

	if(!is_numeric($cpf)) 

 		return FALSE;

 		

 	// verifica se foi passado com todos os caracteres repetidos	

	if(($cpf == '11111111111') || ($cpf == '22222222222') ||

 	   ($cpf == '33333333333') || ($cpf == '44444444444') ||

       ($cpf == '55555555555') || ($cpf == '66666666666') ||

       ($cpf == '77777777777') || ($cpf == '88888888888') ||

       ($cpf == '99999999999') || ($cpf == '00000000000')) 

 		return FALSE;    	  

 	

 	// lê o dígito verificador (DV)

	$dv_informado = substr($cpf, 9,2);



 	for($i=0; $i<=8; $i++) 

 		$digito[$i] = substr($cpf, $i,1); 

	

	// calcula o valor do 10° dígito de verificação

	$posicao = 10;

	$soma = 0;



	for($i=0; $i<=8; $i++) 

	{

		$soma = $soma + $digito[$i] * $posicao;

		$posicao = $posicao - 1;

	}



	$digito[9] = $soma % 11;



	if($digito[9] < 2)    

		$digito[9] = 0;   

	else

		$digito[9] = 11 - $digito[9];   

   

	// calcula o valor do 11° dígito de verificação

	$posicao = 11;

	$soma = 0;



	for ($i=0; $i<=9; $i++)

	{

		$soma = $soma + $digito[$i] * $posicao;

		$posicao = $posicao - 1;

	}



	$digito[10] = $soma % 11;



	if ($digito[10] < 2)

		$digito[10] = 0;   

	else

		$digito[10] = 11 - $digito[10];   

	

	// verifica se o DV calculado é igual ao informado

	$dv = $digito[9] * 10 + $digito[10];

	if ($dv != $dv_informado)

		return FALSE;  

	else

		return TRUE;  

}



?>