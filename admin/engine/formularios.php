    <?php 
    
    if (!isset($_SESSION)) {
        @session_start();
    }
        include_once 'funcoes.php';
        
        include_once '../../banco.php';

        include_once '../redimensiona.php';

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);



    $id = $_POST['id'];
    switch ($id) {


        case 'cursoAdd':
            cursoAdd();
            break;

        case 'cursoEdit':
            cursoEdit();
            break;

        case 'cursoRemove':
            cursoRemove();
            break;
            

        // Slideshow
        case 'slideShowAdd':
            slideShowAdd();
            break;
        case 'slideshowEdit':
            slideshowEdit();
            break;   
        case 'slideshowRemove':
            slideshowRemove();
            break;   
        case 'slideshowActive':
            slideshowActive();
            break;      

        // Empregos
        case 'empregoAdd':
            empregoAdd();
            break;  
        case 'empregoAddUser':
            empregoAddUser();
            break;  
        case 'empregoEdit':
            empregoEdit();
            break;  
        case 'empregoRemove':
            empregoRemove();
            break; 
        case 'empregoActive':
            empregoActive();
            break; 
 

        // Empresas
        case 'empresaAdd':
            empresaAdd();
            break;  
        case 'empresaEdit':
            empresaEdit();
            break;  
        case 'empresaRemove':
            empresaRemove();
            break;  
        case 'empresaActive':
            empresaActive();
            break;  

        // Usuarios
        case 'cadastrarUser':
            cadastrarUser();
            break;
        case 'editUser':
            editUser();
            break;
        case 'planoCandidato':
            planoCandidato();
            break;
        case 'planoEmpresa':
            planoEmpresa();
            break;

        case 'loginUser':
            loginUser();
            break;
        case 'loginUserEmprego':
            loginUserEmprego();
            break;
            
        case 'validarUser':
            validarUser();
            break;
        case 'cadastrarCv':
            cadastrarCv();
            break;
        case 'alterarCv':
            alterarCv();
            break;


        // Planos

        case 'planoEdit';
            planoEdit();
            break;


        // Pagina Quem Somos
        case 'quemSomos':
            quemSomos();
            break;

        // Pagina Emprego
        case 'candidatarEmprego':
            candidatarEmprego();
            break;

        // MAILS

        case 'emailContato':
            emailContato();
            break;

        case 'emailCvEmpresa':
            emailCvEmpresa();
            break;


        // ADMIN
        case 'loginAdmin':
            loginAdmin();
            break;
        case 'aceitarCandidato':
            aceitarCandidato();
            break;
        case 'recusarContrato':
            recusarContrato();
            break;

        case 'sugestoes':
            sugestoes();
            break;

        case 'esqueceuSenha':
            esqueceuSenha();
            break;
        case 'esqueceuCpfCnpj':
            esqueceuCpfCnpj();
            break;


        case 'mensagemCandidatoEmprego':
            mensagemCandidatoEmprego();
            break;

        case 'alterarSenha':
            alterarSenha();
            break;

        case 'opcaoCandidato':
            opcaoCandidato();
            break;

        case 'cargoDesejadoAdd':
            cargoDesejadoAdd();
            break;
        case 'cargoDesejadoRemove':
            cargoDesejadoRemove();
            break;

        case 'cargoInteressesAdd':
            cargoInteressesAdd();
            break;
        case 'cargoInteressesRemove':
            cargoInteressesRemove();
            break;

        case 'suporteAdd':
            suporteAdd();
            break;
        case 'suporteRemove':
            suporteRemove();
            break;

        case 'suporteEdit':
            suporteEdit();
            break;

        case 'empresaLogin':
            empresaLogin();
            break;

    }

    // FUNÇÕES

    function cursoAdd(){
        $nome = $_POST["nome"];
        $estado = $_POST["estado"];
        $cidade = $_POST["cidade"];
        $presenca = $_POST["presenca"];
        $horas = $_POST["horas"];
        $valor = $_POST["valor"];
        $resumo = $_POST["resumo"];
        $outrasinfo = $_POST["outrasinfo"];
        $programacompleto = $_POST["programacompleto"];
        $query = "INSERT INTO cursos (
            nome,
            estado,
            cidade,
            presenca,
            horas,
            valor,
            resumo,
            outrasinfo,
            programacompleto
            ) VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
            )";
        $query = sprintf($query,
            addslashes($nome),
            addslashes($estado),
            addslashes($cidade),
            addslashes($presenca),
            addslashes($horas),
            addslashes($valor),
            addslashes($resumo),
            addslashes($outrasinfo),
            addslashes($programacompleto));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Curso cadastrado com sucesso", "alert alert-success");
            goToPage("../cursos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cursos.php");
        }

    }

    function cursoEdit(){
        $idCurso = $_POST["idCurso"];
        $nome = $_POST["nome"];
        $estado = $_POST["estado"];
        $cidade = $_POST["cidade"];
        $presenca = $_POST["presenca"];
        $horas = $_POST["horas"];
        $valor = $_POST["valor"];
        $resumo = $_POST["resumo"];
        $outrasinfo = $_POST["outrasinfo"];
        $programacompleto = $_POST["programacompleto"];
        $query = "UPDATE cursos SET
            nome = '%s',
            estado = '%s',
            cidade = '%s',
            presenca = '%s',
            horas = '%s',
            valor = '%s',
            resumo = '%s',
            outrasinfo = '%s',
            programacompleto = '%s' 
            WHERE id = $idCurso";
        $query = sprintf($query,
            addslashes($nome),
            addslashes($estado),
            addslashes($cidade),
            addslashes($presenca),
            addslashes($horas),
            addslashes($valor),
            addslashes($resumo),
            addslashes($outrasinfo),
            addslashes($programacompleto));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Curso alterado com sucesso", "alert alert-success");
            goToPage("../cursos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cursos.php");
        }

    }

    function cursoRemove(){
        $idCurso = $_POST["idCurso"];
        $query = "DELETE FROM cursos WHERE id = $idCurso";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Curso deletado com sucesso", "alert alert-success");
            goToPage("../cursos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cursos.php");
        }

    }

    function slideShowAdd(){
        $foto = $_FILES['imagem'];    
        $redim = new Redimensiona();
        $src = $redim->Redimensionar($foto, 1920);
        $imagem = str_replace("../","", $src);
        if (!empty($src)) {
            $query = "INSERT INTO slideshow (imagem) VALUES ('$imagem')";
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Slideshow cadastrado com sucesso", "alert alert-success");
                goToPage("../slideshow.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../slideshow.php");
            }
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../slideshow.php");
        }
    }

    function slideshowEdit(){
        $idSlideshow = $_POST['idSlideshow'];
        $foto = $_FILES['imagem'];    
        $redim = new Redimensiona();
        $src = $redim->Redimensionar($foto, 1920);
        $imagem = str_replace("../","", $src);
        if (!empty($src)) {
            $query = mysql_query("SELECT * FROM slideshow WHERE id_slideshow = '$idSlideshow' ");
            while ($result = mysql_fetch_array($query)) {
                $imagemAntiga = $result['imagem'];
            }
            $deletou = unlink('../'.$imagemAntiga);
            $query = "UPDATE slideshows SET imagem = '$imagem' WHERE id_slideshow = '$idSlideshow' ";
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Slideshow alterado com sucesso", "alert alert-success");
                goToPage("../slideshow.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../slideshow.php");
            }
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../slideshow.php");
        }
    }
    function slideshowRemove(){
        $idSlideshow = $_POST['idSlideshow'];
        $query = mysql_query("SELECT * FROM slideshow WHERE id_slideshow = $idSlideshow ");
        while ($result = mysql_fetch_array($query)) {
            $imagem = $result['imagem'];
        }
        $deletou = unlink('../'.$imagem);
        if ($deletou) {
            $query = mysql_query("DELETE FROM slideshow WHERE id_slideshow = $idSlideshow ");
            setaMensagem("Slideshow deletado com sucesso", "alert alert-success");
            goToPage("../slideshow.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../slideshow.php");
        }
    }
    function slideshowActive(){
        $id = $_POST['idSlideshow'];
        $ativo = $_POST['ativo'];
        if ($ativo) {
            $sql = mysql_query("UPDATE slideshow SET ativo = 0 WHERE id_slideshow = '$id'");
            setaMensagem("Slideshow desativado com sucesso", "alert alert-success");
            goToPage("../slideshow.php");
        }else{
            $sql = mysql_query("UPDATE slideshow SET ativo = 1 WHERE id_slideshow = '$id'");
            setaMensagem("Slideshow ativado com sucesso", "alert alert-success");
            goToPage("../slideshow.php");
        }
    }

    function empregoAdd(){
        $nome = $_POST['nome'];
        $idEmpresa = $_POST['idEmpresa'];
        $principaisAtividades = $_POST['principaisAtividades'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $requisitos = $_POST['requisitos'];
        $salario = $_POST['salario'];
        $tipo_vaga = $_POST['tipo_vaga'];
        $horario = $_POST['horario'];
        $beneficios = $_POST['beneficios'];
        
        $query = "INSERT INTO vagas (nome,idEmpresa,requisitos,principais_atividades,estado,cidade,salario,data, tipo_vaga, horario, beneficios) VAlUES ";
        $query .= "('%s', '%s', '%s', '%s', '%s', '%s', '%s', NOW(), '%s', '%s', '%s')";
        $query = sprintf($query, addslashes($nome), addslashes($idEmpresa), addslashes($requisitos), addslashes($principaisAtividades), addslashes($estado), addslashes($cidade), addslashes($salario), addslashes($tipo_vaga), addslashes($horario), addslashes($beneficios));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Vaga cadastrada com sucesso", "alert alert-success");
            goToPage("../vagas.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../vagas.php");
        }

    }

    function empregoAddUser(){
        $emailUser = $_SESSION['login']['email'];
        $nome = $_POST['nome'];
        $empresa = $_POST['empresa'];
        $principaisAtividades = $_POST['principaisAtividades'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $requisitos = $_POST['requisitos'];
        $salario = $_POST['salario'];

        $query = "INSERT INTO empregos (nome,id_empresa,requisitos,principais_atividades,estado,cidade,salario,data) VAlUES ";
        $query .= "('%s', '%s', '%s', '%s', '%s', '%s', '%s', NOW())";
        $query = sprintf($query, addslashes($nome), addslashes($empresa), addslashes($requisitos), addslashes($principaisAtividades), addslashes($estado), addslashes($cidade), addslashes($salario));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Emprego cadastrado com sucesso", "alert alert-success");
            goToPage("../empregos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../empregos.php");
        }

    }

    function empregoEdit(){
        $idEmprego = $_POST['idEmprego'];
        $nome = $_POST['nome'];
        $empresa = $_POST['empresa'];
        $principaisAtividades = $_POST['principaisAtividades'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $requisitos = $_POST['requisitos'];
        $salario = $_POST['salario'];
        $tipo_vaga = $_POST['tipo_vaga'];
        $horario = $_POST['horario'];
        $beneficios = $_POST['beneficios'];

        $query = "UPDATE empregos SET nome = '%s', 
        id_empresa = '%s', requisitos = '%s', principais_atividades = '%s', 
        estado = '%s', cidade = '%s', salario = '%s', tipo_vaga = '%s', horario = '%s',
        beneficios = '%s', data = NOW()  WHERE id_emprego = '$idEmprego' ";
        $query = sprintf($query, addslashes($nome), addslashes($empresa), addslashes($requisitos), addslashes($principaisAtividades), addslashes($estado), addslashes($cidade), addslashes($salario), addslashes($tipo_vaga), addslashes($horario), addslashes($beneficios));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Vaga alterada com sucesso", "alert alert-success");
            goToPage("../empregos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../empregos.php");
        }
    }

    function empregoRemove(){
        $idEmprego = $_POST['idEmprego'];
        $query = mysql_query("DELETE FROM empregos WHERE id_emprego = '$idEmprego' ");
        if ($query) {
            setaMensagem("Vaga deletada com sucesso", "alert alert-success");
            goToPage("../empregos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../empregos.php");
        }
    }

    function empregoActive(){
        $idEmprego = $_POST['idEmprego'];
        $ativo = $_POST['ativo'];
        if ($ativo) {
            $sql = mysql_query("UPDATE empregos SET ativo = 0 WHERE id_emprego = '$idEmprego'");
            setaMensagem("Emprego desativado com sucesso", "alert alert-success");
            goToPage("../empregos.php");
        }else{
            $sql = mysql_query("UPDATE empregos SET ativo = 1 WHERE id_emprego = '$idEmprego'");
            setaMensagem("Emprego ativado com sucesso", "alert alert-success");
            goToPage("../empregos.php");
        }
    }
//  ------------------------------------------------------------------------------
    function empresaAdd(){

        $cnpj = $_POST['cnpj'];
        $validaCpf = ValidaCPFCNPJ($cnpj);
        if (!$validaCpf) {
            setaMensagem("CNPJ inválido", "alert alert-danger");
            goToPage("../../cadastro.php");
        }
        $query = "SELECT id FROM empresas WHERE cnpj = '$cnpj'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            setaMensagem("Já existe um CNPJ com este cadastro.", "alert alert-danger");
            goToPage("../../cadastro.php");
        }
        $razaosoc = $_POST['razaosoc'];
        $nomefantasia = $_POST['nomefantasia'];
        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $telefone = $_POST['telefone'];
        $endereco = $_POST['endereco'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $sobre = $_POST['sobre'];
        $ramoAtividade = $_POST['ramoAtividade'];
        $senha = md5($senha);
        $foto = $_FILES['imagem'];    
        $nomeFoto = $_FILES['imagem']['name'];    
        if ($nomeFoto == "") {
            $imagem = "images/empresaDefault.jpg";
        }else{
            $redim = new Redimensiona();
            $src = $redim->Redimensionar($foto, 250);
            $imagem = str_replace("../","", $src);
        }
        $query = "INSERT INTO empresas (imagem, razaosoc, nome_fantasia, cnpj, email, senha, telefone, endereco, estado, cidade, sobre, ramoAtividade) VALUES ";
        $query .= "('%s','%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
        $query = sprintf($query, addslashes($imagem), addslashes($razaosoc), addslashes($nomefantasia), addslashes($cnpj), addslashes($email), addslashes($senha), addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), addslashes($sobre), addslashes($ramoAtividade));
        $query = mysql_query($query);
        if ($query) {

            $query = "SELECT * FROM empresas WHERE cnpj = '$cnpj'";
            $query = mysql_query($query);
            $res = mysql_fetch_assoc($query);
            $idEmpresa = $res['id'];
            $_SESSION['job']['empresa']['id'] = $idEmpresa;
            setaMensagem("Cadastrado realizado com sucesso", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../cadastro.php");
        }

    }

    function empresaEdit(){
        $idEmpresa = $_POST['idEmpresa'];
        $razaosoc = $_POST['razaosoc'];
        $nomefantasia = $_POST['nomefantasia'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        $endereco = $_POST['endereco'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $sobre = $_POST['sobre'];
        $ramoAtividade = $_POST['ramoAtividade'];
        $foto = $_FILES['imagem'];    
        $redim = new Redimensiona();
        $src = $redim->Redimensionar($foto, 250);
        $imagem = str_replace("../","", $src);
        if (empty($src)) {  
            $query = "UPDATE empresas SET razaosoc = '%s', nome_fantasia = '%s', email = '%s', telefone = '%s', endereco = '%s', estado = '%s', cidade = '%s', sobre = '%s', ramoAtividade = '%s' WHERE id = '$idEmpresa'";
            $query = sprintf($query, addslashes($razaosoc), addslashes($nomefantasia), addslashes($email), addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), addslashes($sobre), addslashes($ramoAtividade));
        }else{
            $query = "UPDATE empresas SET imagem = '%s', razaosoc = '%s', nome_fantasia = '%s', email = '%s', telefone = '%s', endereco = '%s', estado = '%s', cidade = '%s', sobre = '%s', ramoAtividade = '%s' WHERE id = '$idEmpresa'";
            $query = sprintf($query, addslashes($imagem), addslashes($razaosoc), addslashes($nomefantasia), addslashes($email), addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), addslashes($sobre), addslashes($ramoAtividade));
        }
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Empresa editada com sucesso", "alert alert-success");
            goToPage("../empresasEdit.php?idEmpresa=$idEmpresa");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../index.php");
        }
    }

    function empresaRemove(){
    //     $idEmpresa = $_POST['idEmpresa'];
    //     $query = mysql_query("DELETE FROM empresas WHERE id_empresa = '$idEmpresa' ");
    //     if ($query) {
    //         setaMensagem("Empresa deletada com sucesso", "alert alert-success");
    //         goToPage("../empresas.php");
    //     }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../empresas.php");
    //     }
    }

    function empresaActive(){
        $idEmpresa = $_POST['idEmpresa'];
        $ativo = $_POST['ativo'];
        if ($ativo) {
            $sql = mysql_query("UPDATE empresas SET ativo = 0 WHERE id = '$idEmpresa'");
            setaMensagem("Empresa desativada com sucesso", "alert alert-success");
            goToPage("../empresas.php");
        }else{
            $sql = mysql_query("UPDATE empresas SET ativo = 1 WHERE id = '$idEmpresa'");
            setaMensagem("Empresa ativada com sucesso", "alert alert-success");
            goToPage("../empresas.php");
        }
    }

    function cadastrarUser(){

        $nome = $_POST['nome'];
        $cpf = $_POST['cpfcnpj'];

        $validaCpf = ValidaCPFCNPJ($cpf);
        if (!$validaCpf) {
            setaMensagem("CPF ou CNPJ inválido", "alert alert-danger");
            goToPage("../../cadastro.php");
        }
        $query = "SELECT * FROM usuarios WHERE cpf = '$cpf'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            setaMensagem("Já existe um cadastro com este CPF.", "alert alert-danger");
            goToPage("../../cadastro.php");
        }

        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $senha = md5($senha);
        $telefone = $_POST['telefone'];
        $endereco = $_POST['endereco'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $datanasc = $_POST['datanasc'];
        $rede_social = $_POST['rede_social'];
        $foto = $_FILES['imagemUser'];    
        @$redim = new Redimensiona();
        @$src = $redim->Redimensionar($foto, 150);
        $imagem = str_replace("../","", $src);
        if (empty($src)) {  
            if (isset($_POST["imagemFacebookInput"])) {
                $imagem = $_POST["imagemFacebookInput"];
            }else{
                $imagem = "images/userDefault.jpg";
            }
        }
        $query = "INSERT INTO usuarios (nome, cpf, email, senha, 
        telefone, endereco, estado, cidade, rede_social, imagem, datanasc) VALUES ";
        $query .= "('%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s')";
        $query = sprintf($query, addslashes($nome), addslashes($cpf) ,addslashes($email), addslashes($senha),
        addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), 
        addslashes($rede_social), addslashes($imagem), addslashes($datanasc));    
        $query = mysql_query($query);

        if ($query) {
            $query = "SELECT * FROM usuarios WHERE cpf = '$cpf'";
            $query = mysql_query($query);
            $res = mysql_fetch_assoc($query);
            $idUsuario = $res['id'];
            $_SESSION['job']['usuario']['id'] = $idUsuario;
            setaMensagem("Cadastro realizado com sucesso", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../cadastro.php");
        }
    }

    function editUser(){
        
        $idUsuario = $_POST['idUsuario'];
        if (isset($_POST["cargos"])) {
            $sql = "DELETE FROM cargos_interesses WHERE idUsuario = $idUsuario";
            $sql = mysql_query($sql);
            foreach ($_POST["cargos"] as $key => $value) {
                $sql = "INSERT INTO cargos_interesses (idUsuario, nome) VALUES ($idUsuario, '$value')";
                $sql = mysql_query($sql);
            }
        }
        
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        $endereco = $_POST['endereco'];
        $estado = $_POST['estado'];
        $cidade = $_POST['cidade'];
        $imagemAntiga = $_POST['imagemAntiga'];
        $rede_social = $_POST['rede_social'];
        $foto = $_FILES['imagem'];    
        $redim = new Redimensiona();
        $src = $redim->Redimensionar($foto, 250);
        $imagem = str_replace("../","", $src);
        if (empty($src)) {  
            $query = "UPDATE usuarios SET email = '%s', telefone = '%s', endereco = '%s', estado = '%s', cidade = '%s', rede_social = '%s' WHERE id = '$idUsuario' ";
            $query = sprintf($query, addslashes($email), addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), addslashes($rede_social));
        }else{
            if ($imagemAntiga != "img/userDefault.jpg") {
                $deletaImagemAntiga = unlink('../../'.$imagemAntiga);
            }
            $query = "UPDATE usuarios SET email = '%s', telefone = '%s', endereco = '%s', estado = '%s', cidade = '%s', rede_social = '%s', imagem = '%s' WHERE id = '$idUsuario' ";
            $query = sprintf($query, addslashes($email), addslashes($telefone), addslashes($endereco), addslashes($estado), addslashes($cidade), addslashes($rede_social), addslashes($imagem));
        }
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastrado alterado com sucesso", "alert alert-success");
            goToPage("../../candidato.php?idUsuario=$idUsuario");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../candidato.php?idUsuario=$idUsuario");
        }
    }

    function loginUser(){
        if (isset($_POST["email"])) {
            $email = $_POST['email'];
            $query = "SELECT * FROM usuarios WHERE email = '$email'";
            $query = mysql_query($query);
            $numRows = mysql_num_rows($query);
            if ($numRows > 0) {
                $res = mysql_fetch_assoc($query);
                $idUsuario = $res['id'];
                $_SESSION['job']['usuario']['id'] = $idUsuario;
                echo '1';
                die();
            }else{
                echo '0';
                die();
            }
        }else{
            $cpf = $_POST['cpf'];
            $senha = $_POST['senha'];
            $senha = md5($senha);
            $query = "SELECT * FROM usuarios WHERE cpf = '$cpf' AND senha = '$senha'";
            $query = mysql_query($query);
            $numRows = mysql_num_rows($query);
            if ($numRows > 0) {
                $res = mysql_fetch_assoc($query);
                $idUsuario = $res['id'];
                $_SESSION['job']['usuario']['id'] = $idUsuario;
                goToPage("../../index.php");
            }else{
                setaMensagem("Dados inválidos", "alert alert-danger");
                goToPage("../../login.php");
            }   
        }

    }

    function empresaLogin(){
        $cnpj = $_POST['cnpj'];
        $senha = $_POST['senha'];
        $senha = md5($senha);
        $query = "SELECT * FROM empresas WHERE cnpj = '$cnpj' AND senha = '$senha'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            $res = mysql_fetch_assoc($query);
            $idEmpresa= $res['id'];
            $_SESSION['job']['empresa']['id'] = $idEmpresa;
            goToPage("../../index.php");
        }else{
            setaMensagem("Dados inválidos.", "alert alert-danger");
            goToPage("../../index.php");
        }         
    }

    function loginUserEmprego(){
        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $idEmprego = $_POST['idEmprego'];
        $senha = base64_encode($senha);
        $query = "SELECT * FROM usuarios WHERE email = '$email' AND senha = '$senha'";
        $query = mysql_query($query);
        if ($query) {
            while ($result = mysql_fetch_array($query)) {
                $nome = $result['nome'];
                $id = $result['id_usuario'];
                $senha = $result['senha'];
                $senha = $result['senha'];
            }
            $_SESSION['job']['usuario']['id'] = $id;
            $_SESSION['job']['usuario']['nome'] = $nome;
            $_SESSION['job']['usuario']['email'] = $email;
            $_SESSION['job']['usuario']['senha'] = $senha;
            goToPage("../../emprego.php?idEmprego=$idEmprego");
        }else{
            setaMensagem("Dados inválidos", "erro");
            goToPage("../../login.php");
        }
    }

    function validarUser(){
        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $senha = base64_encode($senha);
        $query = "SELECT * FROM usuarios WHERE email = '$email' AND senha = '$senha'";
        $query = mysql_query($query);
        if ($query) {
            while ($result = mysql_fetch_array($query)) {
                $id = $result['id_usuario'];
                $nome = $result['nome'];
                $email = $result['email'];
                $senha = $result['senha'];
            }
            $_SESSION['job']['usuario']['id'] = $id;
            $_SESSION['job']['usuario']['nome'] = $nome;
            $_SESSION['job']['usuario']['email'] = $email;
            $_SESSION['job']['usuario']['senha'] = $senha;
            goToPage("../../index.php");
        }else{
            setaMensagem("Dados inválidos", "erro");
            goToPage("../../login.php");
        }
    }

    function cadastrarCv(){
        $idUsuario = $_SESSION['job']['usuario']['id'];
        
        if ($_FILES['cvAnexo']['name'] == "") {

            $empregado = $_POST['empregado'];
            $objetivoProfissional = $_POST['objetivoProfissional'];
            $experienciaProfissional = $_POST['experienciaProfissional'];
            $ultimoEmprego = $_POST['ultimoEmprego'];
            $escolaridade = $_POST['escolaridade'];
            $salario = $_POST['salario'];
            if ($salario == "") {
                $salario = 'A combinar';
            }
            $query = "INSERT INTO cvs (id_usuario, empregado, escolaridade, objetivo, experiencia, salario, ultimoEmprego) ";
            $query .= "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
            $query = sprintf($query, addslashes($idUsuario), addslashes($empregado), addslashes($escolaridade), addslashes($objetivoProfissional), addslashes($experienciaProfissional), addslashes($salario), addslashes($ultimoEmprego));
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Curriculum cadastrado com sucesso", "alert alert-success");
                goToPage("../../index.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../../index.php");
            }
        }else{
            $curriculoAnexo = $_FILES['cvAnexo'];
            $uploaddir = 'cvs/';
            $uploadfile = $uploaddir . $_FILES['cvAnexo']['name'];
            if (move_uploaded_file($_FILES['cvAnexo']['tmp_name'], '../../'.$uploadfile)) {
                $query = "INSERT INTO cvs (id_usuario, anexoCv) ";
                $query .= "VALUES ('%s', '%s')";
                echo $query = sprintf($query, addslashes($idUsuario), addslashes($uploadfile));
                $query = mysql_query($query);
                if ($query) {
                    setaMensagem("Curriculum cadastrado com sucesso", "alert alert-success");
                    goToPage("../../index.php");
                }else{
                    setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                    goToPage("../../index.php");
                }
            } else {
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../../index.php");
            }
        }

    }

    function alterarCv(){
        $idCurriculum = $_POST['idCurriculum'];
        $empregado = $_POST['empregado'];
        $objetivoProfissional = $_POST['objetivoProfissional'];
        $experienciaProfissional = $_POST['experienciaProfissional'];
        $salario = $_POST['salario'];
        $escolaridade = $_POST['escolaridade'];
        $extensoesPermitidas = array('doc','docx', 'zip');
        $filename = strtolower($_POST['anexoCv']);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $allowed =  array('doc','docx' ,'zip');
        if(!in_array($ext,$allowed)){
            setaMensagem("Tipo de arquivo inválido", "erro");
            goToPage("../../index.php");
        }
        if ($_FILES['cvAnexo']['name'] != "") {
            $curriculoAnexo = $_FILES['cvAnexo'];
            $uploaddir = 'cvs/';
            $anexoCv = $uploaddir . $_FILES['cvAnexo']['name'];
            $anexoCvDelete = $_POST['anexoCv'];
            @unlink('../../'.$anexoCvDelete);
            $move = move_uploaded_file($_FILES['cvAnexo']['tmp_name'], '../../'.$anexoCv);
        }else{
            $anexoCv = $_POST['anexoCv'];
        }
        if ($salario == "") {
            $salario = 'A combinar';
        }
        $ultimoEmprego = $_POST['ultimoEmprego'];
        $query = "UPDATE cvs SET empregado = '%s', escolaridade = '%s', objetivo = '%s', experiencia = '%s', salario = '%s', anexoCv = '$anexoCv', ultimoEmprego = '%s' WHERE id_curriculum = '$idCurriculum'";
        $query = sprintf($query, addslashes($empregado), addslashes($escolaridade), addslashes($objetivoProfissional), addslashes($experienciaProfissional), addslashes($salario), addslashes($ultimoEmprego));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Curriculum alterado com sucesso", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../index.php");
        }
    }

    function quemSomos(){
        $foto = $_FILES['imagem'];    
        $texto = $_POST['texto'];
        $redim = new Redimensiona();
        $src = $redim->Redimensionar($foto, 1920);
        $imagem = str_replace("../","", $src);
        if (!empty($src)) {
            $query = "TRUNCATE TABLE quem_somos";
            $query = mysql_query($query);
            $query = "";
            $query = "INSERT INTO quem_somos (texto,imagem) VALUES ('%s','$imagem')";
            $query = sprintf($query, addslashes($texto));
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Quem somos alterado com sucesso", "alert alert-success");
                goToPage("../somos.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../somos.php");
            }
        }else{
            $query = "UPDATE quem_somos SET texto = '%s'";
            $query = sprintf($query, addslashes($texto));
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Quem somos alterado com sucesso", "alert alert-success");
                goToPage("../somos.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../somos.php");
            }
        }
    }

//  ------------------------------------------------------------------------------

//  ------------------------------------------------------------------------------
    function emailContato(){

        $modelo = '
        <table style="width: 500px;margin: 0 auto;padding: 5px;  font-family: sans-serif;
">
            <caption style="background: rgb(91, 160, 145); color: white;padding: 20px;margin: 20px 0 10px 0;">
                Email de contato
            </caption>
            <tr>
                <td><b>Nome</b></td>
                <td>[#NOME]</td>
            </tr>
            <tr>
                <td><b>E-mail</b></td>
                <td>[#EMAIL]</td>
            </tr>
            <tr>
                <td><b>Telefone</b></td>
                <td>[#TELEFONE]</td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td>[#MENSAGEM]</td>                                    
            </tr>
        </table>
        ';

        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        $mensagem = $_POST['mensagem'];

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: $nome <$email>\r\n";
                         
        $modelo = str_replace("[#NOME]", $nome, $modelo);
        $modelo = str_replace("[#EMAIL]", $email, $modelo);
        $modelo = str_replace("[#TELEFONE]", $telefone, $modelo);
        $modelo = str_replace("[#MENSAGEM]", $mensagem, $modelo);
        $email = mail('contato@ffdevweb.com.br','Email de contato', $modelo, $headers);

        if ($email) {
            setaMensagem("Email enviado com sucesso", "alert alert-success");
            goToPage("../../contato.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../contato.php");
        }
    }
//  ------------------------------------------------------------------------------

    function candidatarEmprego(){
        $idEmprego = $_POST['idEmprego'];
        $idUsuario = $_POST['idUsuario'];

        $query = "SELECT * FROM usuarios WHERE id = '$idUsuario'";
        $query = mysql_query($query);
        while ($result = mysql_fetch_array($query)) {
            $nomeCandidato = $result['nome'];
        }

        $query = "SELECT * FROM vagas WHERE id = '$idEmprego'";
        $query = mysql_query($query);
        while ($result = mysql_fetch_array($query)) {
            $nomeEmprego = $result['nome'];
            $idEmpresa = $result['id_empresa'];
        }

        $query = "SELECT * FROM empresas WHERE id = '$idEmpresa'";
        $query = mysql_query($query);
        while ($result = mysql_fetch_array($query)) {
            $emailEmpresa = $result['email'];;
        }

        $modelo = '
        <div style="font-family: sans-serif;text-align:center;width: 800px;margin: 0 auto;
        background: rgb(31, 80, 119); color: white;padding: 20px;">
            Email de contato
        </div>
        <div style="width: 800px;margin: 20px auto;text-align:center;">
            Olá, o candidato <b>[#NOMECANDIDATO]</b> se candidatou para a vaga <b>[#EMPREGO]</b><br/>
            <a href="http://ffdevweb.com.br/projetos/job/admin">Clique aqui</a> para maiores informações.
        </div>

        ';

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
        $headers .= "From: Job <contato@ffdevweb.com.br>\r\n";
                         
        $modelo = str_replace("[#NOMECANDIDATO]", $nomeCandidato, $modelo);
        $modelo = str_replace("[#EMPREGO]", $nomeEmprego, $modelo);

        $query = "INSERT INTO candidatos (id_usuario, id_vaga) VALUES ($idUsuario, $idEmprego)";
        $query = mysql_query($query);
        if ($query) {
            //$email = mail($emailEmpresa,'Email de contato', $modelo, $headers);
            setaMensagem("Sucesso", "alert alert-success");
            goToPage("../../empregos.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../emprego.php?idEmprego=$idEmprego");
        }
    }

//  ------------------------------------------------------------------------------

    function loginAdmin(){

        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $senhaString = $senha;
        $senha = md5($senha);
        $query = "SELECT * FROM empresas WHERE email = '$email' AND senha = '$senha'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            $_SESSION['job']['empresa']['id'] = $id_empresa;
            goToPage("../index.php");
        }else{
            $query = "SELECT * FROM usuarios WHERE email = '$email' 
            AND senha = '$senha'";
            $query = mysql_query($query);
            $numRows = mysql_num_rows($query);
            if ($numRows > 0) {
                $_SESSION['job']['usuario']['id'] = $id_empresa;
                goToPage("../index.php");
            }else{   
                if($email == "admin@admin.com.br" && $senhaString == "admin"){
                    $_SESSION['job']['admin'] = 1;
                    goToPage("../index.php");
                }else{
                    setaMensagem("Dados inválidos", "alert alert-danger");
                    goToPage("../login.php");
                }
            }
        }


    }

//  ------------------------------------------------------------------------------

    function aceitarCandidato(){

        $nomeCandidato = $_POST['nomeCandidato'];
        $nomeEmprego = $_POST['nomeEmprego'];
        $email = $_POST['email'];
        $mensagem = $_POST['mensagem'];
        
        $modelo = '
        <div style="font-family: sans-serif;text-align:center;width: 800px;margin: 0 auto;background: rgb(91, 160, 145); color: white;padding: 20px;">Email de contato</div>
            <div style="width: 800px;margin: 20px auto;text-align:center;">Olá <b>[#NOME]</b>, a empresa <b>[#EMPRESA]</b> aceitou sua candidatura para a vaga <b>[#EMPREGO]</b>, segue abaixo a mensagem:</div>
        <table style="width: 800px;margin: 0 auto;padding: 5px; font-family: sans-serif;">
            <tr>
                <td style="text-align:justify;">[#MENSAGEM]</td>                                    
            </tr>
        </table>
        ';

        $headers = "MIME-Version: 1.0 \r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1 \r\n";
        $headers .= "From: $nome <$email>\r\n";
                         
        $modelo = str_replace("[#NOME]", $nomeCandidato, $modelo);
        $modelo = str_replace("[#EMPRESA]", $_SESSION['job']['painel']['nome_fantasia'], $modelo);
        $modelo = str_replace("[#EMPREGO]", $nomeEmprego, $modelo);
        $modelo = str_replace("[#MENSAGEM]", ''.$mensagem.'', $modelo);
        $email = mail($email,'Email de contato', $modelo, $headers);

        if ($email) {
            setaMensagem("Email enviado com sucesso", "alert alert-success");
            goToPage("../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../index.php");
        }
    }

//  ------------------------------------------------------------------------------

    function recusarContrato(){

        $idCandidato = $_POST['idCandidato'];
        $idEmprego = $_POST['idEmprego'];
        $query = "UPDATE candidatos SET status = 'rejeitado' WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Candidato rejeitado com sucesso", "alert alert-success");
            goToPage("../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../index.php");
        }
    }

//  ------------------------------------------------------------------------------

    function emailCvEmpresa(){

        $modelo = '
        <table style="width: 500px;margin: 0 auto;padding: 5px;  font-family: sans-serif;
">
            <caption style="background: rgb(91, 160, 145); color: white;padding: 20px;margin: 20px 0 10px 0;">Email de contato</caption>
            <tr>
                <td><b>Empresa</b></td>
                <td>[#NOME]</td>
            </tr>
            <tr>
                <td><b>E-mail</b></td>
                <td>[#EMAIL]</td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td>[#MENSAGEM]</td>                                    
            </tr>
        </table>
        ';

        $nome = $_SESSION['job']['empresa']['nome_fantasia'];
        $email = $_SESSION['job']['empresa']['email'];
        $mensagem = $_POST['mensagem'];
        $emailCandidato = $_POST['email'];

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: $nome <$email>\r\n";
                         
        $modelo = str_replace("[#NOME]", $nome, $modelo);
        $modelo = str_replace("[#EMAIL]", $email, $modelo);
        $modelo = str_replace("[#MENSAGEM]", $mensagem, $modelo);
        $email = mail($emailCandidato,'Email de contato', $modelo, $headers);

        if ($email) {
            setaMensagem("Email enviado com sucesso", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../index.php");
        }
    }

//  ------------------------------------------------------------------------------

    function planoCandidato(){

        $plano = $_POST['planoCandidato'];
        $idUsuario = $_POST['idUsuario'];
        $query = "SELECT * FROM usuarios WHERE id_usuario = '$idUsuario'";
        $query = mysql_query($query);
        while ($result = mysql_fetch_array($query)) {
            $nome = $result['nome'];
            $cpf = $result['cpf'];
            $email = $result['email'];
        }
        $_SESSION['job']['usuario']['plano'] = $plano;
        goToPage("../../pagseguro/source/examples/createPaymentRequest.php?tipoCadastro=candidato");
    }

//  ------------------------------------------------------------------------------

    function planoEmpresa(){
        $plano = $_POST['planoCandidato'];
        $_SESSION['job']['empresa']['plano'] = $plano;
        $cpfcnpj = $_SESSION['job']['empresa']['cnpj'];
        $formaPagamento = $_POST['formaPagamento'];
        if ($formaPagamento == "cartao") {
            goToPage("../../pagseguro/source/examples/createPaymentRequest.php?tipoCadastro=empresa");
        }else{

                $planoId = $plano;

                if ($planoId == '1') { $plano = "30 dias"; $autor = "empresa"; }
                if ($planoId == '2') { $plano = "Anual"; $autor = "empresa"; }
                $query = "INSERT INTO planos_cadastrados (cpf_cnpj, autor, plano, status, data) VALUES ('$cpfcnpj', '$autor', '$plano', 'Aguardando Pagamento', NOW())";
                $query = mysql_query($query);
                if ($query) {
                    goToPage("../boleto/boleto_cef.php");
                }else{
                    setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                    goToPage("../index.php");
                }
        }
    }


//  ------------------------------------------------------------------------------


    function planoEdit(){
        
        $cpfcnpj = $_POST['cpf_cnpj'];
        $status = $_POST['status'];

        $query1 = "INSERT INTO historico_plano_alterado (cpf_cnpj,status,data) VALUES ('$cpfcnpj','$status',NOW())";
        $query1 = mysql_query($query1);

        $query = "";
        $query = "UPDATE planos_cadastrados SET status = '$status' WHERE cpf_cnpj = '$cpfcnpj'";
        $res = mysql_query($query);

        $query = "SELECT * FROM usuarios WHERE cpf = '$cpfcnpj'";
        $query = mysql_query($query);
        while ($res = mysql_fetch_array($query)) {
           $id_usuario = $res['id_usuario'];
        }

        if ($id_usuario != "") {
            $query = "";
            $query = "UPDATE cvs SET ativo = 1 WHERE id_usuario = '$id_usuario'";
            $query = mysql_query($query);
        }

        if ($res) {
            setaMensagem("Status alterado com sucesso", "alert alert-success");
            goToPage("../planosCadastrados.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../planosCadastrados.php");
        }
 }



//  ------------------------------------------------------------------------------

    function sugestoes(){

        $modelo = '
        <table style="width: 500px;margin: 0 auto;padding: 5px;  font-family: sans-serif;
">
            <caption style="background: rgb(91, 160, 145); color: white;padding: 20px;margin: 20px 0 10px 0;">Email de contato</caption>
            <tr>
                <td><b>Empresa</b></td>
                <td>[#NOME]</td>
            </tr>
            <tr>
                <td><b>E-mail</b></td>
                <td>[#EMAIL]</td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td>[#MENSAGEM]</td>                                    
            </tr>
        </table>
        ';

        $nome = $_SESSION['job']['empresa']['nome_fantasia'];
        $email = $_SESSION['job']['empresa']['email'];
        $mensagem = $_POST['sugestao'];

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: $nome <$email>\r\n";
                         
        $modelo = str_replace("[#NOME]", $nome, $modelo);
        $modelo = str_replace("[#EMAIL]", $email, $modelo);
        $modelo = str_replace("[#MENSAGEM]", $mensagem, $modelo);
        $email = mail('contato@ffdevweb.com.br','Email de sugestoes', $modelo, $headers);

        if ($email) {
            setaMensagem("Email enviado com sucesso", "alert alert-success");
            goToPage("../../sugestoes.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../sugestoes.php");
        }
    }



//  ------------------------------------------------------------------------------

    function esqueceuSenha(){

        $modelo = 
        '
            <table style="width: 500px;margin: 0 auto;padding: 5px;  font-family: sans-serif;">
                <caption style="background: rgb(91, 160, 145); color: white;padding: 20px;margin: 20px 0 10px 0;">E-mail de recuperação de senha</caption>
                <tr>
                    <td><b>CPF / CNPJ</b></td>
                    <td>[#cpfCnpj]</td>
                </tr>
                <tr>
                    <td><b>Senha</b></td>
                    <td>[#SENHA]</td>
                </tr>
            </table>
        ';

        $email = $_POST['email'];

        $query = "SELECT * FROM usuarios WHERE email = '$email'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows == 0) {
            $query = "SELECT * FROM empresas WHERE email = '$email'";
            $query = mysql_query($query);
        }

        while ($result = mysql_fetch_array($query)) {
            $cpfcnpj = $result['cpf_cnpj'];
            $senha = $result['senha'];
        }
        
        $senha = base64_decode($senha);

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: job <'contato@ffdevweb.com.br'>\r\n";

        $modelo = str_replace("[#cpfCnpj]", $cpfcnpj, $modelo);
        $modelo = str_replace("[#SENHA]", $senha, $modelo);

        $enviaEmail = mail($email,'E-mail de recuperação de senha do site job', $modelo, $headers);

        if ($enviaEmail) {
            setaMensagem("Senha enviada para o e-mail cadastrado.", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../index.php");
        }
    }


//  ------------------------------------------------------------------------------

    function esqueceuCpfCnpj(){

        $modelo = 
        '
            <table style="width: 500px;margin: 0 auto;padding: 5px;  font-family: sans-serif;">
                <caption style="background: rgb(91, 160, 145); color: white;padding: 20px;margin: 20px 0 10px 0;">E-mail de recuperação de cpf/cnpj do site job</caption>
                <tr>
                    <td><b>CPF / CNPJ</b></td>
                    <td>[#cpfCnpj]</td>
                </tr>
            </table>
        ';

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: job <'contato@ffdevweb.com.br'>\r\n";

        $email = $_POST['email'];

        $query = "SELECT * FROM usuarios WHERE email = '$email'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            # code...
        }else{
            $query = "SELECT * FROM empresas WHERE email = '$email'";
            $query = mysql_query($query);
        }

        while ($result = mysql_fetch_array($query)) {
            $cpfcnpj = $result['cpf_cnpj'];
            $senha = $result['senha'];
        }
        $senha = base64_decode($senha);
        $modelo = str_replace("[#cpfCnpj]", $cpfcnpj, $modelo);

        $email = mail($email,'E-mail de recuperação de cpf/cnpj do site job', $modelo, $headers);

        if ($email) {
            setaMensagem("Um e-mail com o CPF/CNPJ foi enviado para seu e-mail", "alert alert-success");
            goToPage("../../index.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../../index.php");
        }
    }

//  ------------------------------------------------------------------------------


    function mensagemCandidatoEmprego(){

        $idEmprego = $_POST['idEmprego'];
        $idCandidato = $_POST['idCandidato'];
        $cpfcnpj = $_POST['cpfcnpj'];
        $mensagem = $_POST['mensagem'];

        $query = "INSERT INTO mensagem_emprego_candidato (id_emprego, id_candidato, mensagem, cpf_cnpj_autor,data) ";
        $query .= "VALUES ('$idEmprego', '$idCandidato', '%s', '$cpfcnpj', NOW())";
        $query = sprintf($query, addslashes($mensagem));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Mensagem adicionada com sucesso", "alert alert-success");
            goToPage("../candidato.php?idEmprego=$idEmprego&idCandidato=$idCandidato");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../candidato.php?idEmprego=$idEmprego&idCandidato=$idCandidato");
        }


    }


//  ------------------------------------------------------------------------------

    function alterarSenha(){

        $idUsuario = $_POST['idUsuario'];
        $senhaAntiga = $_POST['senhaAntiga'];
        $senhaAntiga = md5($senhaAntiga);
        $senhaNova = $_POST['senhaNova'];
        $senhaNova = md5($senhaNova);
        $query = "SELECT * FROM usuarios WHERE id = $idUsuario AND senha = '$senhaAntiga'";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows > 0) {
            $query = "UPDATE usuarios SET senha = '%s' WHERE id = '$idUsuario'";
            $query = sprintf($query, addslashes($senhaNova));
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Senha alterada com sucesso", "alert alert-success");
                goToPage("../../candidato.php?idUsuario=$idUsuario");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../../candidato.php?idUsuario=$idUsuario");
            }
        }else{
            setaMensagem("Senha inválida", "erro");
            goToPage("../../candidato.php?idUsuario=$idUsuario");
        }

    }

//  ------------------------------------------------------------------------------

    function opcaoCandidato(){

        $id_emprego = $_POST['id_emprego'];
        $idUsuario = $_POST['idUsuario'];
        $emailUsuario = $_POST['emailUsuario'];
        $opcao = $_POST['opcao'];

        if ($opcao == 'aceitar') {
        
            $nomeCandidato = $_POST['nomeUsuario'];
            $nomeEmprego = $_POST['nomeEmprego'];
            $email = $emailUsuario;
            $mensagem = '';
            
            $modelo = '
            <div style="font-family: sans-serif;text-align:center;width: 800px;margin: 0 auto;background: rgb(91, 160, 145); color: white;padding: 20px;">Email de contato do site job</div>
                <div style="width: 800px;margin: 20px auto;text-align:center;">Olá <b>[#NOME]</b>, a empresa <b>[#EMPRESA]</b> aceitou sua candidatura para a vaga <b>[#EMPREGO]</b></div>
            ';

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
            $headers .= "From: $nome <$email>\r\n";
                             
            $modelo = str_replace("[#NOME]", $nomeCandidato, $modelo);
            $modelo = str_replace("[#EMPRESA]", $_SESSION['job']['painel']['nome_fantasia'], $modelo);
            $modelo = str_replace("[#EMPREGO]", $nomeEmprego, $modelo);
            $email = mail($email,'Email de contato do site job', $modelo, $headers);
            
            $idCandidato = $idUsuario;
            $idEmprego = $id_emprego;
            $query = "UPDATE candidatos SET status = 'aceitado' WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
            $query = mysql_query($query);
            
            if ($email) {
                setaMensagem("Email enviado com sucesso", "alert alert-success");
                goToPage("../candidatos.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../candidatos.php");
            }

        }

        if ($opcao == 'recusar') {
            $idCandidato = $idUsuario;
            $idEmprego = $id_emprego;
            $query = "UPDATE candidatos SET status = 'rejeitado' WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
            $query = mysql_query($query);
            if ($query) {
                setaMensagem("Candidato rejeitado com sucesso", "alert alert-success");
                goToPage("../candidatos.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../candidatos.php");
            }
        }

        if ($opcao == 'revisar') {
            $idCandidato = $idUsuario;
            $idEmprego = $id_emprego;
            $query = "SELECT * FROM candidatos WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
            $numRows = mysql_num_rows($query);

                $query = mysql_query($query);
                while ($result = mysql_fetch_array($query)) {
                    $revisar = $result['revisar'];
                }
                if ($revisar == 1) {
                    $query = "UPDATE candidatos SET revisar = 0 WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
                }else{
                    $query = "UPDATE candidatos SET revisar = 1 WHERE id_usuario = '$idCandidato' AND id_emprego = '$idEmprego'";
                }
            

            $query = mysql_query($query);
            if ($query) {
                setaMensagem("A revisão foi marcada com sucesso", "alert alert-success");
                goToPage("../candidatos.php");
            }else{
                setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
                goToPage("../candidatos.php");
            }
        }

    }

//  ------------------------------------------------------------------------------

    function cargoDesejadoAdd(){

        $nome = $_POST['nome'];
        $query = "INSERT INTO cargos_desejados (nome) VALUES ('%s')";
        $query = sprintf($query, addslashes($nome));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cargo desejado adicionado com sucesso", "alert alert-success");
            goToPage("../cargosDesejados.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cargosDesejados.php");
        }

    }

//  -------------------------------------------------------------------------------

    function cargoDesejadoRemove(){
        $id_cargo_desejado = $_POST['id_cargo_desejado'];
        $query = mysql_query("DELETE FROM cargos_desejados WHERE id_cargo_desejado = '$id_cargo_desejado' ");
        if ($query) {
            setaMensagem("Cargo desejado deletado com sucesso", "alert alert-success");
            goToPage("../cargosDesejados.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cargosDesejados.php");
        }
    }

//  -------------------------------------------------------------------------------


    function cargoInteressesAdd(){

        $nome = $_POST['nome'];
        $query = "INSERT INTO cargos_interesses (nome) VALUES ('%s')";
        $query = sprintf($query, addslashes($nome));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cargo interesse adicionado com sucesso", "alert alert-success");
            goToPage("../cargosInteresses.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cargosInteresses.php");
        }

    }

//  -------------------------------------------------------------------------------

    function cargoInteressesRemove(){
        $id_cargo_interesse = $_POST['id_cargo_interesse'];
        $query = mysql_query("DELETE FROM cargos_interesses WHERE id_cargo_interesse = '$id_cargo_interesse' ");
        if ($query) {
            setaMensagem("Cargo interesse deletado com sucesso", "alert alert-success");
            goToPage("../cargosInteresses.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../cargosInteresses.php");
        }
    }
    
//  -------------------------------------------------------------------------------

    function suporteAdd(){

        $pergunta = $_POST['pergunta'];
        $resposta = $_POST['resposta'];
        $query = "INSERT INTO suporte (pergunta, resposta, data) VALUES ('%s','%s', NOW())";
        $query = sprintf($query, addslashes($pergunta), addslashes($resposta));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Pergunta/Resposta adicinada com sucesso", "alert alert-success");
            goToPage("../suporte.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../suporte.php");
        }

    }

//  -------------------------------------------------------------------------------

    function suporteRemove(){
        $id_suporte = $_POST['id_suporte'];
        $query = mysql_query("DELETE FROM suporte WHERE id_suporte = '$id_suporte' ");
        if ($query) {
            setaMensagem("Pergunta/Resposta deletada com sucesso", "alert alert-success");
            goToPage("../suporte.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../suporte.php");
        }
    }

//  -------------------------------------------------------------------------------

    function suporteEdit(){
        $id_suporte = $_POST['id_suporte'];
        $pergunta = $_POST['pergunta'];
        $resposta = $_POST['resposta'];
        $query = "UPDATE suporte SET pergunta = '%s', resposta = '%s', data = NOW() WHERE id_suporte = '$id_suporte'";
        $query = sprintf($query, addslashes($pergunta), addslashes($resposta));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Pergunta/Resposta alterada com sucesso", "alert alert-success");
            goToPage("../suporte.php");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage("../suporte.php");
        }
    }

//  -------------------------------------------------------------------------------



//  ------------------------------------------------------------------------------
//  final codigo
//  ------------------------------------------------------------------------------