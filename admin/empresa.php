<html>
<head>
    <?php 
    include_once 'headAdmin.php';
    include_once 'verificaLogin.php';
    ?>
</head>
<body>
    <div id="wrapper">
        <?php include 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9" style="margin: 20px 0 0;">
                    <div class="panel panel-default">
                        <?php echo mostraMensagem(); ?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i>Ultimos Candidatos</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Emprego</th>
                                            <th>Candidato</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                            $query = "SELECT * FROM candidatos cand
                                                        INNER JOIN (SELECT id_usuario, nome as nomeUsuario FROM usuarios) us ON cand.id_usuario = us.id_usuario
                                                        INNER JOIN (SELECT id_emprego, id_empresa, nome as nomeEmprego FROM empregos) empreg ON cand.id_emprego = empreg.id_emprego
                                                        INNER JOIN (SELECT id_empresa, nome_fantasia as nomeEmpresa FROM empresas) empres ON empreg.id_empresa = empres.id_empresa";
                                            $query = mysql_query($query);
                                            while ($result = mysql_fetch_array($query)) {
                                                $nomeEmpresa = $result['nomeEmpresa'];
                                                $nomeEmprego = $result['nomeEmprego'];
                                                $nomeUsuario = $result['nomeUsuario'];
                                                $idUsuario = $result['id_usuario'];
                                                $id_emprego = $result['id_emprego'];
                                                echo
                                                '
                                                    <tr>
                                                        <td>'.$nomeEmprego.'</td>
                                                        <td><a href="candidato.php?idEmprego='.$id_emprego.'&idCandidato='.$idUsuario.'&nomeEmprego='.$nomeEmprego.'">'.$nomeUsuario.'</td>
                                                    </tr>
                                                ';
                                            }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>
