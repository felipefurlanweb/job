<html>
<head>
    <?php 
        include_once 'headAdmin.php';
        include_once 'verificaLogin.php';
    ?>
</head>
<body>
    <div id="wrapper">
        <?php include_once 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12 marginCimaBaixo">
                    <?php 
                        echo mostraMensagem(); 
                        $cpfcnpj = $_SESSION['job']['empresa']['cnpj'];
                        $query = "";
                        $query = "SELECT to_days(pc.data)-to_days(NOW()) as dias, pc.data as dataCadastrada, pc.cpf_cnpj, pc.autor, pc.plano, pc.status FROM planos_cadastrados pc WHERE pc.cpf_cnpj = '$cpfcnpj' AND pc.autor = 'empresa' ORDER BY dataCadastrada DESC LIMIT 1";
                        $query = mysql_query($query);
                        $numRows = mysql_num_rows($query);

                        if ($numRows > 0) {
                            while ($result = mysql_fetch_array($query)) {
                                $plano = $result['plano'];
                                $status = $result['status'];
                                $dias = $result['dias'];
                                $dataCadastrada = $result['dataCadastrada'];
                                $dias = str_replace("-", "", $dias);
                                $time = strtotime($dataCadastrada);
                                $anoCadastrada = date('Y', $time);
                                $mesCadastrada = date('m', $time);
                                $diaCadastrada = date('d', $time);
                                $dataCadastro = $diaCadastrada.'/'.$mesCadastrada.'/'.$anoCadastrada;
                                $dataHoje = date("d/m/Y");
                                $mesHoje = date("m");

                            }
                            $statusPlano = '<div class="alert alert-info">Atualmente, seu plano é '.$plano.' - <b>Status:</b> '.$status.'</div>';
                            if ($plano == 'Anual') {
                              $qtsDias = intval($dias - 365);
                            }else{
                              $qtsDias = intval($dias - 30);
                            }
                            $qtsDias = str_replace("-","", $qtsDias);
                        }
                              $tablePlanosEmpresa = 
                              '
                                <form action="engine/formularios.php" method="post" enctype="multipart/form-data">
                                  <input type="hidden" name="id" value="planoEmpresa">
                                  <input type="hidden" name="cpfcnpj" value="'.$cpfcnpj.'">                        

                                  <div class="col-xs-12 col-lg-12 semPadding">
                                    <div class="col-xs-12 col-lg-6">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <span>PLANO 1</span>
                                        </div>
                                        <div class="panel-body">
                                          <h4>Plano mensal,<br/> As vagas cadastradas podem ser ativadas.</h4>
                                          <div class="precoPlano marginCimaBaixo">
                                            <label>
                                              <input type="radio" name="planoCandidato" value="1" required>
                                              R$18,90 p/ mês
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-6">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <span>PLANO 2</span>
                                        </div>
                                        <div class="panel-body">
                                          <h4>Plano anual,<br/> As vagas cadastradas podem ser ativadas.</h4>
                                          <div class="precoPlano marginCimaBaixo">
                                            <label>
                                              <input type="radio" name="planoCandidato" value="2" required>
                                              R$15,90 p/ ano
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-lg-12 boxFormaPagamento">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <span>Forma de pagamento</span>
                                        </div>
                                        <div class="panel-body">
                                          <div class="col-xs-6 col-lg-6">
                                            <label>
                                              Cartão de crédito
                                              <img src="images/logo-pagseguro.png">
                                              <input type="radio" name="formaPagamento" value="cartao" required>
                                            </label>
                                          </div>
                                          <div class="col-xs-6 col-lg-6">
                                          </div>
                                        </div>
                                        <div class="panel-footer text-right">
                                          <button class="btn btn-success" type="submit">Continuar</button>
                                        </div>
                                      </div>
                                  </div>
                                </form> 
                                <br/>
                              ';
                            if ($numRows == 0) {
                              echo $tablePlanosEmpresa;
                            }else{
                              if ($plano == 'Anual') {
                                
                                if ($qtsDias <= 0) {
                                  echo '<div class="alert alert-danger">Seu plano acabou, atualize para continuar publicando!</div>';                                
                                  echo
                                  '
                                    <form action="engine/formularios.php" method="post" enctype="multipart/form-data">
                                      <input type="hidden" name="id" value="planoEmpresa">
                                      <input type="hidden" name="cpfcnpj" value="'.$cpfcnpj.'">                        
                                        <div class="table-responsive">
                                      <table>
                                        <tr>
                                          <td>
                                            <div class="boxPlano">
                                              <div class="nomePlano">
                                                <span>PLANO 1</span>
                                              </div>
                                              <div class="infoPlano">
                                                <span>Plano mensal, seus empregos publicados mensalmente enquanto seu plano estiver ativo.</span>
                                                <div class="precoPlano">
                                                  R$<h2><h1>18</h1>,90</h2> p/ mês
                                                </div>
                                                <input type="radio" name="planoCandidato" value="1">
                                              </div>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="boxPlano boxPlanoBorda">
                                              <div class="nomePlano">
                                                <span>PLANO 2</span>
                                              </div>
                                              <div class="infoPlano">
                                                <span>Plano anual, seus empregos publicados durante o ano em que seu plano estiver ativo.</span>
                                                <div class="precoPlano">
                                                  R$<h2><h1>15</h1>,90</h2> p/ ano
                                                </div>
                                                <input type="radio" name="planoCandidato" value="2">
                                              </div>
                                            </div>
                                          </td>
                                        </tr>
                                      </table>
                                        </div> 
                                      <div class="col-xs-12 col-lg-12 semPadding boxFormaPagamento">
                                        <span>Qual forma de pagamento ?</span>
                                        <label>Cartão de crédito</label>
                                        <img src="images/logo-pagseguro.png">
                                        <input type="radio" name="formaPagamento" value="cartao" required>
                                      </div>
                                      <div class="col-xs-12 col-lg-12 semPadding">
                                        <button class="btn btn-success" type="submit">Continuar</button>
                                      </div>
                                    </form> 
                                  ';
                                }else{
                                  echo ''.$statusPlano.'';
                                }
                              }else{
                                if ($mesHoje > $mesCadastrada) {
                                  echo '<div class="alert alert-danger">Seu plano acabou, atualize para continuar publicando!</div>';                                
                                  echo
                                  '
                                    <form action="engine/formularios.php" method="post" enctype="multipart/form-data">
                                      <input type="hidden" name="id" value="planoEmpresa">
                                      <input type="hidden" name="cpfcnpj" value="'.$cpfcnpj.'">                        
                                        <div class="table-responsive">
                                      <table>
                                        <tr>
                                          <td>
                                            <div class="boxPlano">
                                              <div class="nomePlano">
                                                <span>PLANO 1</span>
                                              </div>
                                              <div class="infoPlano">
                                                <span>Plano mensal, seus empregos publicados mensalmente enquanto seu plano estiver ativo.</span>
                                                <div class="precoPlano">
                                                  R$<h2><h1>18</h1>,90</h2> p/ mês
                                                </div>
                                                <input type="radio" name="planoCandidato" value="1">
                                              </div>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="boxPlano boxPlanoBorda">
                                              <div class="nomePlano">
                                                <span>PLANO 2</span>
                                              </div>
                                              <div class="infoPlano">
                                                <span>Plano anual, seus empregos publicados durante o ano em que seu plano estiver ativo.</span>
                                                <div class="precoPlano">
                                                  R$<h2><h1>15</h1>,90</h2> p/ ano
                                                </div>
                                                <input type="radio" name="planoCandidato" value="2">
                                              </div>
                                            </div>
                                          </td>
                                        </tr>
                                      </table>
                                        </div> 
                                      <div class="col-xs-12 col-lg-12 semPadding boxFormaPagamento">
                                        <span>Qual forma de pagamento ?</span>
                                        <label>Cartão de crédito</label>
                                        <img src="images/logo-pagseguro.png">
                                        <input type="radio" name="formaPagamento" value="cartao" required>
                                      </div>
                                      <div class="col-xs-12 col-lg-12 semPadding">
                                        <button class="btn btn-success" type="submit">Continuar</button>
                                      </div>
                                    </form> 
                                  ';
                                }else{
                                  echo ''.$statusPlano.'';
                                }
                              }
                            }
                            
                    ?>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    
</body>
</html>
