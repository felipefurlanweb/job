<?php include_once 'headAdmin.php'; ?>

<script type="text/javascript">
    window.onload = function() {
      new dgCidadesEstados({
        estado: document.getElementById('estado'),
        cidade: document.getElementById('cidade')
      });
    }
    $(document).ready(function() {
        $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
</script>
<?php 

    $id = $_POST["idCurso"];
    $query = "SELECT * FROM cursos WHERE id = $id";
    $query = mysql_query($query);
    $result = mysql_fetch_assoc($query);
    $nome = $result["nome"];
    $estado = $result["estado"];
    $cidade = $result["cidade"];
    $presenca = $result["presenca"];
    $horas = $result["horas"];
    $valor = $result["valor"];
    $resumo = $result["resumo"];
    $outrasinfo = $result["outrasinfo"];
    $programacompleto = $result["programacompleto"];

?>
        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">Editar Curso</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <div class="col-xs-12 col-md-12">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="cursoEdit" hidden>
                    <input name="idCurso" value="<?php echo $id; ?>" hidden>

                        <div class="row">

                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" value="<?php echo $nome; ?>">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Estado</label>
                                        <select id="estado" name="estado" class="form-control" value="<?php echo $estado; ?>"></select>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Cidade</label>
                                        <select id="cidade" name="cidade" class="form-control" value="<?php echo $cidade; ?>">
                                            <option>Selecione uma cidade ...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <label>Presença</label>
                                        <select name="presenca" class="form-control" value="<?php echo $presenca; ?>">
                                            <option value="">Selecione uma opção ...</option>
                                            <option value="Presencial">Presencial</option>
                                            <option value="A Distância">A Distância</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-2">
                                        <label>Horas</label>
                                        <input type="number" name="horas" class="form-control" value="<?php echo $horas; ?>" maxlength="2">
                                    </div>
                                    <div class="col-xs-12 col-md-3">
                                        <label>Valor</label>
                                        <input type="text" name="valor" class="form-control valorReal" value="<?php echo $valor; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>Resumo</label>
                                        <textarea name="resumo" class="form-control"><?php echo $resumo; ?></textarea>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <label>Outras Informações</label>
                                        <textarea name="outrasinfo" class="form-control"><?php echo $outrasinfo; ?></textarea>
                                    </div>
                                    <div class="col-xs-12 col-md-12 marginTop">
                                        <label>Programa Completo</label>
                                        <textarea name="programacompleto" class="form-control"><?php echo $programacompleto; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group marginTop">
                                <input type="submit" class="btn btn-success" value="Salvar">
                            </div>
                        </div>

                </form>      

                </div>

                <!-- /.col-lg-12 -->

            </div>

        </div>

