<html>
<head>
    <?php 

        include_once 'headAdmin.php';
        include_once 'verificaLogin.php';
        $id = $_POST['idSlideshow'];
        $sql = mysql_query("SELECT * FROM slideshows WHERE id_slideshow = $id ");
        while($result = mysql_fetch_array($sql)) {
            $imagem = $result['imagem'];
        }
    ?>
</head>
<body>
    <div id="wrapper">
        <?php include 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Editar Slideshow</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                    <input name="id" value="slideshowEdit" hidden>
                    <input name="idSlideshow" value="<?php echo $id; ?>" hidden>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?php echo $imagem; ?>" alt="">
                              </div>
                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                              <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Selecione uma imagem</span>
                                    <span class="fileinput-exists">Alterar</span>
                                    <input type="file" name="imagem">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                              </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <input type="submit" class="btn btn-success" value="Salvar">
                        </div>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    
</body>
</html>
