<html>
<head>
    <?php 

    include_once 'headAdmin.php';
    include_once 'verificaLogin.php';
    include_once 'verificaPlano.php';

    $id_suporte = $_POST['id_suporte'];
    $query = "SELECT * FROM suporte WHERE id_suporte = '$id_suporte'";
    $query = mysql_query($query);
    while ($result = mysql_fetch_array($query)) {
        $pergunta = $result['pergunta'];
        $resposta = $result['resposta'];
    }

    ?>
</head>
<body>
    <div id="wrapper">
        <?php include 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Adicionar Pergunta / Resposta para Suporte</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                        <input name="id" value="suporteEdit" hidden>
                        <input name="id_suporte" value="<?php echo $id_suporte; ?>" hidden>

                        <div class="col-lg-12 semPadding">
                            <label>Pergunta</label>
                            <input type="text" name="pergunta" class="form-control" value="<?php echo $pergunta; ?>">
                        </div>
                        <div class="col-lg-12 semPadding">
                            <label>Resposta</label>
                            <textarea name="resposta" class="form-control"><?php echo $resposta; ?></textarea>
                        </div>
                        <div class="col-lg-12 semPadding">
                            <input type="submit" class="btn btn-success" value="Salvar">
                        </div>

                    </form>      
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
    
    </body>
    </html>
