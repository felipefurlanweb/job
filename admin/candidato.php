<?php include_once 'headAdmin.php'; ?>
    <?php

    $idUsuario = $_GET['idCandidato'];
    $idCandidato = $_GET['idCandidato'];
    $nomeEmprego = $_GET['nomeEmprego'];
    $idEmprego = $_GET['idEmprego'];
    $query = "SELECT * FROM cvs WHERE id_usuario = '$idUsuario'";
    $query = mysql_query($query);
    while ($result = mysql_fetch_array($query)) {
      $id_usuario = $result['id_usuario'];
      $empregado = $result['empregado'];
      $escolaridade = $result['escolaridade'];
      $objetivo = $result['objetivo'];
      $experiencia = $result['experiencia'];
      $salario = $result['salario'];
      $anexoCv = $result['anexoCv'];
      $ultimoEmprego = $result['ultimoEmprego'];
    }
    $query = "SELECT * FROM usuarios WHERE id = '$id_usuario'";
    $query = mysql_query($query);
    while ($result = mysql_fetch_array($query)) {
      $nome = $result['nome'];
      $endereco = $result['endereco'];
      $estado = $result['estado'];
      $cidade = $result['cidade'];
      $cargo_interesse1 = $result['cargo_interesse1'];
      $cargo_interesse2 = $result['cargo_interesse2'];
      $cargo_interesse3 = $result['cargo_interesse3'];
      $rede_social = $result['rede_social'];
      $imagem = $result['imagem'];
      $datanasc = $result['datanasc'];
    }

    ?>

    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Candidato</h1>
        </div>
        <div class="col-lg-12">
          <?php echo mostraMensagem(); ?>
        </div>
        <div class="col-lg-6">
          <div class="row">
          <div class="col-xs-12 form-group">
          <?php 

            if ($anexoCv != "") {

              echo '<label>Arquivo: </label>'.$anexoCv;

              echo '<br/>';

              echo '<a href="/'.$anexoCv.'">Fazer download do currículo</a>';

            }

          ?>
          </div>
          <div class="col-xs-12 form-group">
            <label>Nome Completo</label>
            <span><?php echo $nome; ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Endereço</label>
            <span><?php echo $endereco; ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Data Nascimento</label>
            <span><?php echo $datanasc; ?></span>
          </div>

          <div class="col-xs-12 form-group">
            <label>Cargos de Interesse</label><br/>
            <?php 
              $query = "SELECT * FROM cargos_interesses WHERE idUsuario = $idUsuario";
              $query = mysql_query($query);
              while ($res = mysql_fetch_array($query)) {
                $nome = $res["nome"];
                echo
                '
                <span>'.$nome.'</span><br/>
                ';
              }
            ?>
            <span></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Cidade</label>
            <span><?php echo $cidade.' - '.$estado; ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Empregado</label>
            <?php 
            if ($empregado == '1'){ $empregado = 'Sim'; $empregadoValue = '1'; }
            if ($empregado == '0'){ $empregado = 'Não'; $empregadoValue = '0'; }
            ?>
            <span><?php echo $empregado; ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Escolaridade</label>
            <?php 
            if($escolaridade == 'doutoradoCompleto'){ $escolaridadeValue = 'doutoradoCompleto'; $escolaridade = 'Doutorado Completo'; }
            if($escolaridade == 'doutoradoIncompleto'){ $escolaridadeValue = 'doutoradoIncompleto'; $escolaridade = 'Doutorado Incompleto'; }
            if($escolaridade == 'mestradoCompleto'){ $escolaridadeValue = 'mestradoCompleto'; $escolaridade = 'Mestrado Completo'; }
            if($escolaridade == 'mestradoIncompleto'){ $escolaridadeValue = 'mestradoIncompleto'; $escolaridade = 'Mestrado Incompleto'; }
            if($escolaridade == 'posGraduacaoCompleta'){ $escolaridadeValue = 'posGraduacaoCompleta'; $escolaridade = 'Pós-graduação Completa'; }
            if($escolaridade == 'posGraduacaoIncompleta'){ $escolaridadeValue = 'posGraduacaoIncompleta'; $escolaridade = 'Pós-graduação Incompleta'; }
            if($escolaridade == 'superiorCompleto'){ $escolaridadeValue = 'superiorCompleto'; $escolaridade = 'Superior Completo'; }
            if($escolaridade == 'superiorIncompleto'){ $escolaridadeValue = 'superiorIncompleto'; $escolaridade = 'Superior Incompleto'; }
            if($escolaridade == 'ensinoMedioCompleto'){ $escolaridadeValue = 'ensinoMedioCompleto'; $escolaridade = 'Ensino Médio Completo'; }
            if($escolaridade == 'ensinoMedioIncompleto'){ $escolaridadeValue = 'ensinoMedioIncompleto'; $escolaridade = 'Ensino Médio Incompleto'; }
            if($escolaridade == 'ensinoFundamentalCompleto'){ $escolaridadeValue = 'ensinoFundamentalCompleto'; $escolaridade = 'Ensino Fundamental Completo'; }
            if($escolaridade == 'ensinoFundamentalIncompleto'){ $escolaridadeValue = 'ensinoFundamentalIncompleto'; $escolaridade = 'Ensino Fundamental Incompleto'; }
            if($escolaridade == 'naoAlfabetizado'){ $escolaridadeValue = 'naoAlfabetizado'; $escolaridade = 'Não Alfabetizado'; }
            ?>
            <span><?php echo $escolaridade; ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Objetivo Profissional</label>
            <span><?php echo $objetivo ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Experiência Profissional</label>
            <span><?php echo $experiencia ?></span>
          </div>
          <div class="col-xs-12 form-group">
            <label>Pretensão Salarial</label>
            <span>R$ <?php echo $salario; ?></span>
          </div>
          <div class="col-xs-12 form-group">
          </div>
          <div class="col-xs-12 form-group">
          </div>
          <div class="col-xs-12 form-group">
          </div>
          <div class="col-xs-12 form-group">
          </div>

          <?php /* ?>
          <div class="acoesCandidatos">
            <button id="btnAceitarCandidato">Entrar em contato</button>
            <form action="engine/formularios.php" id="formRecusarContrato" method="post">
              <input type="hidden" name="id" value="recusarContrato">
              <input type="hidden" name="idCandidato" value="<?php echo $idCandidato; ?>">
              <input type="hidden" name="idEmprego" value="<?php echo $idEmprego; ?>">
              <input type="submit" value="Recusar">
            </form>
            <form action="engine/formularios.php" id="formAceitarContrato" method="post">

              <input type="hidden" name="id" value="aceitarCandidato">
              <input type="hidden" name="nomeCandidato" value="<?php echo $nome; ?>">
              <input type="hidden" name="nomeEmprego" value="<?php echo $nomeEmprego; ?>">
              <input type="hidden" name="email" value="<?php echo $email; ?>">
              <label>Envia uma mensagem para o candidato:</label>
              <hr>
              <textarea class="form-control" name="mensagem"></textarea>
              <input type="submit" value="Enviar">
              <span id="cancelarAceitarCandidato">Cancelar</span>
            </form>
          </div>
          <?php */ ?> 
        </div>
        </div>
        <div class="col-lg-6" id="boxMensagemCandidato">
          <label>Mensagem (anotação):</label>
            <form action="engine/formularios.php" method="post">
              <?php 
                
                $idCandidato = $_GET['idCandidato'];
                $idEmprego = $_GET['idEmprego'];
                // var_dump($_SESSION);
                $cpfcnpj = $_SESSION['job']['painel']['cpfcnpj'];
              ?>
              <input type="hidden" name="id" value="mensagemCandidatoEmprego">
              <input type="hidden" name="idEmprego" value="<?php echo $idEmprego; ?>">
              <input type="hidden" name="idCandidato" value="<?php echo $idCandidato; ?>">
              <input type="hidden" name="cpfcnpj" value="<?php echo $cpfcnpj; ?>">

          <textarea name="mensagem" class="form-control" required></textarea>
          <br>
          <input type="submit" class="btn btn-success" value="Enviar">
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $(document).ready(function() {
    $("#formAceitarContrato").hide();
    $("#btnAceitarCandidato").click(function(event) {
      $("#formAceitarContrato").show('slow/400/fast');
      $("#btnAceitarCandidato").hide('slow/400/fast');
      $("#formRecusarContrato").hide('slow/400/fast');
    });
    $("#cancelarAceitarCandidato").click(function(event) {
      $("#formAceitarContrato").hide('slow/400/fast');
      $("#btnAceitarCandidato").show('slow/400/fast');
      $("#formRecusarContrato").show('slow/400/fast');
    });
  });
  </script>
</body>
</html>
