<html>
<head>
    <?php 

    include_once 'headAdmin.php';
    include_once 'verificaLogin.php';
    include_once 'verificaPlano.php';

    ?>
</head>
<body>
    <div id="wrapper">
        <?php include 'menu-lateral.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Adicionar Cargo Desejado</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                        <input name="id" value="cargoDesejadoAdd" hidden>

                        <div class="col-lg-12 semPadding">

                            <label>Cargo Desejado</label>
                            <input type="text" name="nome" class="form-control" required>
                        </div>
                        <div class="col-lg-12 semPadding">
                            <input type="submit" class="btn btn-success" value="Salvar">
                        </div>

                    </form>      
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
    
    </body>
    </html>
