<?php   
if (!isset($_SESSION)) {
session_start();
}
include_once 'engine/funcoes.php';
include_once '../banco.php';
?>
<html>
    <title>Painel Administrador</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="sbAdmin/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="sbAdmin/vendor/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="sbAdmin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jasny.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="sbAdmin/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="js/mascara.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            Login
                        </h3>
                    </div>
                    <div class="panel-body">
                        <?php echo mostraMensagem(); ?>
                        <form role="form" action="engine/formularios.php" method="POST">
                            <input type="hidden" name="id" value="loginAdmin">
                            <fieldset>
                                <div class="form-group">
                                    <input type="email" class="form-control" 
                                    name="email" 
                                    placeholder="E-mail">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" 
                                    name="senha" 
                                    type="password">
                                </div>
                                <div class="form-group">
                                    <input type="submit"
                                    name="submit"
                                    class="btn btn-lg btn-success btn-block" 
                                    value="Entrar">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>
