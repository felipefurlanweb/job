<?php include_once 'headAdmin.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo mostraMensagem(); ?>
                    <h1 class="page-header">Suporte</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="suporteAdd.php"><button type="button" class="btn btn-primary">Adicionar</button></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Pergunta</th>
                                            <th>Data</th>
                                            <th>Editar</th>
                                            <th>Remover</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = mysql_query("SELECT * FROM suporte ORDER BY data DESC");
                                        while ($result = mysql_fetch_array($sql)) {
                                            $id_suporte = $result['id_suporte'];
                                            $pergunta = $result['pergunta'];
                                            $data = $result['data'];
                                            echo
                                            '
                                                <tr>
                                                    <td>'.$pergunta.'</td>
                                                    <td>'.$data.'</td>
                                                    <td class="center">
                                                        <form action="suporteEdit.php" method="post">
                                                            <input type="hidden" name="id_suporte" value="'.$id_suporte.'">
                                                            <button type="submit" class="btn btn-info btn-circle">
                                                                <span class="glyphicon glyphicon-pencil"></span>
                                                            </button>
                                                        </form>
                                                    </td> 
                                                    <td class="center">
                                                        <form action="engine/formularios.php" method="post">
                                                            <input type="hidden" name="id" value="suporteRemove">
                                                            <input type="hidden" name="id_suporte" value="'.$id_suporte.'">
                                                            <button type="submit" class="btn btn-danger btn-circle">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </form>
                                                    </td>                                                    
                                                </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>

<?php include_once 'footerAdmin.php'; ?>
