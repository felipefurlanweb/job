<?php include_once 'headAdmin.php'; ?>

        <div id="page-wrapper">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header">Slideshow</h1>
                </div>
                <div class="col-xs-12">
                    <?php echo mostraMensagem(); ?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="slideshowAdd.php"><button type="button" class="btn btn-primary">Adicionar Slideshow</button></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Imagem</th>
                                            <th>Ativar / Desativar</th>
                                            <th>Editar</th>
                                            <th>Deletar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = mysql_query("SELECT * FROM slideshow");
                                        while ($result = mysql_fetch_array($sql)) {
                                            $ativo = $result['ativo'];
                                            $imagem = $result['imagem'];
                                            $id = $result['id_slideshow'];
                                            echo
                                            '
                                                <tr>
                                                    <td class="imagemTabela">
                                                        <img src="'.$imagem.'">
                                                    </td>';
                                                    if ($ativo) {
                                                        echo '
                                                            <td>
                                                                <form action="engine/formularios.php" method="post">
                                                                    <input type="text" name="id" value="slideshowActive" hidden>
                                                                    <input type="text" name="idSlideshow" value="'.$id.'" hidden>
                                                                    <input type="text" name="ativo" value="'.$ativo.'" hidden>
                                                                    <input type="submit" value="" style="background: url(img/ativo.png) 50% 50% no-repeat;height:15px;width:15px;background-size:cover;border:none;">
                                                                </form>
                                                            </td>';
                                                    }else{
                                                        echo '
                                                            <td>
                                                                <form action="engine/formularios.php" method="post">
                                                                    <input type="text" name="id" value="slideshowActive" hidden>
                                                                    <input type="text" name="idSlideshow" value="'.$id.'" hidden>
                                                                    <input type="text" name="ativo" value="'.$ativo.'" hidden>
                                                                    <input type="submit" value="" style="background: url(img/desativo.png) 50% 50% no-repeat;height:15px;width:15px;background-size:cover;border:none;">
                                                                </form>
                                                            </td>';           
                                                    }
                                            echo 
                                            '
                                                    <td class="center">
                                                        <form action="slideshowEdit.php" method="post">
                                                            <input type="text" name="idSlideshow" value="'.$id.'" hidden>
                                                            <button type="submit" class="btn btn-info btn-circle">
                                                                <span class="glyphicon glyphicon-pencil"></span>
                                                            </button>
                                                        </form>
                                                    </td>
                                                    <td class="center">
                                                        <form action="engine/formularios.php" method="post">
                                                            <input type="text" name="id" value="slideshowRemove" hidden>
                                                            <input type="text" name="idSlideshow" value="'.$id.'" hidden>
                                                            <button type="submit" class="btn btn-danger btn-circle">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </form>
                                                    </td>                                                    
                                                </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>

<?php include_once 'footerAdmin.php'; ?>