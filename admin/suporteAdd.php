<?php include_once 'headAdmin.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
</script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Adicionar Pergunta / Resposta para Suporte</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">
                        <input name="id" value="suporteAdd" hidden>

                        <div class="row">
                            <div class="col-lg-12">
                                <label>Pergunta</label>
                                <input type="text" name="pergunta" class="form-control" required>
                            </div>
                            <div class="col-lg-12 marginTop">
                                <label>Resposta</label>
                                <textarea name="resposta" class="form-control" required></textarea>
                            </div>
                            <div class="col-lg-12 marginTop">
                                <input type="submit" class="btn btn-success" value="Salvar">
                            </div>
                        </div>

                    </form>      
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>

<?php include_once 'footerAdmin.php'; ?>