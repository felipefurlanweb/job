<?php include_once 'headAdmin.php'; ?>
<?php
    $sql = mysql_query("SELECT * FROM quem_somos");
    while($result = mysql_fetch_array($sql)) {
        $texto = $result['texto'];
        $imagem = $result['imagem'];
    }
?>
<script type="text/javascript">
    $(document).ready(function() {
      $('textarea').summernote({
        height: 150
      });
    });
</script>

        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">
                    <?php echo mostraMensagem(); ?>
                    <h1 class="page-header">Quem Somos</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="quemSomos" hidden>

                    <div class="col-xs-12 col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="<?php echo $imagem; ?>" alt="">
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                          <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Selecione uma imagem</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="imagem">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-12">
                        <label>Texto</label>
                        <textarea name="texto" class="form-control"><?php echo $texto; ?></textarea>
                    </div>


                    <div class="col-xs-12 col-md-12 marginTop">
                        <input type="submit" class="btn btn-success" value="Salvar">
                    </div>

                </form>

            </div>

        </div>

<?php include_once 'footerAdmin.php'; ?>