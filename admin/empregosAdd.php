<html>

<head>

    <?php 



        include_once 'headAdmin.php';

        include_once 'verificaLogin.php';

        include_once 'verificaPlano.php';



    ?>

    <script type="text/javascript">

    window.onload = function() {

      new dgCidadesEstados({

        estado: document.getElementById('estado'),

        cidade: document.getElementById('cidade')

      });

    }

    </script>

</head>

<body>

    <?php 

    if ($status == "Pendente") {

        setaMensagem("Seu plano ainda está pendente, aguarde para adicionar empregos!", "erro");

        goToPage("index.php");

    }

    ?>

    <div id="wrapper">

        <?php include 'menu-lateral.php'; ?>

        <div id="page-wrapper">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">Adicionar Vaga</h1>

                </div>

                <!-- /.col-lg-12 -->

            </div>

            <!-- /.row -->

            <div class="row">

                <div class="col-lg-6">

                <form action="engine/formularios.php" method="POST" enctype="multipart/form-data">

                    <input name="id" value="empregoAdd" hidden>

                    <?php



                    $empresa = $_SESSION['job']['empresa']['id_empresa'];



                    ?>

                    <input name="empresa" value="<?php echo $empresa; ?>" hidden>



                        <div class="row">


                            <label>Vaga</label>
                            <select name="nome" class="form-control" required>
                                <option value="">Selecione uma vaga ...</option>
                                <?php 
                                    $query = "SELECT * FROM nome_empregos ORDER BY emprego ASC";
                                    $query = mysql_query($query);
                                    while ($result = mysql_fetch_array($query)) {
                                        $emprego = $result['job'];
                                        $emprego = utf8_encode($emprego);
                                        echo '<option value="'.$emprego.'">'.$emprego.'</option>';
                                    }
                                ?>
                            </select>

                            <label>Requisitos</label>

                            <textarea name="requisitos" class="form-control" required></textarea>

                            <label>Principais Atividades</label>

                            <textarea name="principaisAtividades" class="form-control" required></textarea>

                            <label>Estado</label>

                            <select id="estado" name="estado" class="form-control" required></select>

                            <label>Cidade</label>

                            <select id="cidade" name="cidade" class="form-control" required>

                                <option>Selecione um estado ...</option>

                            </select>

                            <label>Tipo de vaga</label>

                            <select name="tipo_vaga" class="form-control" required>

                                <option>Selecione uma opção ...</option>

                                <option>-----------------------</option>

                                <option value="efetivo">Efetivo</option>

                                <option value="temporario">Temporário</option>

                                <option value="prestadorServico">Prestador de serviço</option>

                            </select>

                            <label>Horario</label>

                            <select name="horario" class="form-control" required>

                                <option>Selecione uma opção ...</option>

                                <option>-----------------------</option>

                                <option value="comercial">Comercial</option>

                                <option value="2turno">2ª turno</option>

                            </select>

                            <label>Benefícios</label>

                            <textarea name="beneficios" class="form-control"></textarea>

                            <label>Salário</label>

                            <input type="text" class="form-control valorReal" name="salario" required>

                        </div>

                        <br/>

                        <div class="row text-right">

                            <input type="submit" class="btn btn-success" value="Salvar">

                        </div>



                </form>      

                </div>

                <!-- /.col-lg-12 -->

            </div>

        </div>

        <!-- /#page-wrapper -->

    </div>

    <script type="text/javascript">

        $(document).ready(function() {

            $(".valorReal").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        }); 

    </script>

    

</body>

</html>

